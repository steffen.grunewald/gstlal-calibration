#!/usr/bin/env python3
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import numpy as np
import os
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import matplotlib.pyplot as plt

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlal import test_common


from gstlal import FIRtools as fir
from ticks_and_grid import ticks_and_grid


# Constants
test_duration = 1000		# seconds
f0 = 3.0			# Hz
df = 8.0			# Hz
sr = 2048			# Hz
fft_length = sr * 4		# samples
fft_overlap = fft_length / 2	# samples
num_ffts = (sr * test_duration - fft_overlap) // (fft_length - fft_overlap) - 20
fir_length = fft_length // 64
filt = np.array([0.00000023412, -0.00023546, -0.002361, 0.00694, -0.006843, -0.000361, 0.000002347351])
fir_latency = 2


#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def laltf_shiftfreq_test_01(pipeline, name):
	#
	# This test generates a fake data stream, applies a filter (defined
	# above) to produce a related data stream, and measures the transfer
	# function (TF) at sparse frequency points.  The TF is resampled so
	# that the lowest frequency point is at 0 Hz.  The resampled TF is
	# compared to the original in a plot.
	#

	# Original data
	data = test_common.test_src(pipeline, rate = sr, test_duration = test_duration, wave = 5, src_suffix = '0')
	data = pipeparts.mktee(pipeline, data)
	# Filtered data
	newdata = calibration_parts.mkcomplexfirbank(pipeline, data, latency = fir_latency, fir_matrix = [filt[::-1]], time_domain = True)

	data = calibration_parts.mkinterleave(pipeline, [newdata, data])
	data = pipeparts.mkprogressreport(pipeline, data, "progress_before_TF")
	data = pipeparts.mktee(pipeline, data)
	calibration_parts.mktransferfunction(pipeline, data, fft_length = fft_length, fft_overlap = fft_overlap, num_ffts = num_ffts, use_median = True, update_samples = 99999999999, filename = "tfs1.txt", df = df, f0 = f0)
	calibration_parts.mktransferfunction(pipeline, data, fft_length = fft_length, fft_overlap = fft_overlap, num_ffts = num_ffts, use_median = True, update_samples = 99999999999, filename = "tfs0.txt", df = df, f0 = f0, make_fir_filters = 1.0, fir_length = fir_length)

	#
	# done
	#
	
	return pipeline
	
#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(laltf_shiftfreq_test_01, "laltf_shiftfreq_test_01")

# Get TF data
tf_length = int(sr // 2 // df)
print("Loading TF txt files")
# Remove unwanted lines from TF file, and re-format wanted lines
f = open("tfs0.txt", "r")
lines = f.readlines()
f.close()
os.system("rm tfs0.txt")
f = open("tfs0.txt", "w")
j = 3
while(j < len(lines) and lines[j] != '\n'):
	f.write(lines[j].replace(' + ', '\t').replace(' - ', '\t-').replace('i', ''))
	j += 1
f.close()

TF0 = np.loadtxt("tfs0.txt")

# Remove unwanted lines from TF file, and re-format wanted lines
f = open("tfs1.txt", "r")
lines = f.readlines()
f.close()
os.system("rm tfs1.txt")
f = open("tfs1.txt", "w")
j = 3
while(j < len(lines) and lines[j] != '\n'):
	f.write(lines[j].replace(' + ', '\t').replace(' - ', '\t-').replace('i', ''))
	j += 1
f.close()

TF1 = np.loadtxt("tfs1.txt")
print("Done loading TF txt files")

TF0fvec = []
TF0mag = []
TF0phase = []
for j in range(len(TF0)):
	TF0fvec.append(TF0[j][0])
	tf_at_f = (TF0[j][1] + 1j * TF0[j][2])
	TF0mag.append(abs(tf_at_f))
	TF0phase.append(np.angle(tf_at_f) * 180.0 / np.pi)

TF1fvec = []
TF1mag = []
TF1phase = []
for j in range(len(TF1)):
	TF1fvec.append(TF1[j][0])
	tf_at_f = (TF1[j][1] + 1j * TF1[j][2])
	TF1mag.append(abs(tf_at_f))
	TF1phase.append(np.angle(tf_at_f) * 180.0 / np.pi)

# Plot transfer functions against frequency
plt.figure(figsize = (100, 100))
plt.subplot(211)
plt.plot(TF1fvec, TF1mag, 'blue', linestyle = 'None', marker = '.', markersize = 2.0, label = "Measured")
plt.plot(TF0fvec, TF0mag, 'green', linestyle = 'None', marker = '.', markersize = 2.0, label = "Shifted")
ticks_and_grid(plt.gca(), xscale = 'linear', yscale = 'linear')
plt.ylabel('Magnitude')
plt.title('Test of TF Resampling')
leg = plt.legend(fancybox = True, markerscale = 16.0, numpoints = 1, loc = 'lower right')
leg.get_frame().set_alpha(0.8)
plt.subplot(212)
plt.plot(TF1fvec, TF1phase, 'blue', linestyle = 'None', marker = '.', markersize = 2.0)
plt.plot(TF0fvec, TF0phase, 'green', linestyle = 'None', marker = '.', markersize = 2.0)
ticks_and_grid(plt.gca(), xscale = 'linear', yscale = 'linear')
plt.ylabel('Phase [deg]')
plt.xlabel('Frequency [Hz]')

plt.savefig("TF_%dHz_start_%dHz_space.png" % (int(f0), int(df)))


