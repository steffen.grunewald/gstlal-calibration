#!/usr/bin/env python3
# Copyright (C) 2022  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
from math import pi
import resource
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['legend.fontsize'] = 14
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt

from ticks_and_grid import ticks_and_grid

from optparse import OptionParser, Option

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
import test_common
from gi.repository import Gst
from lal import LIGOTimeGPS
from ligo import segments

parser = OptionParser()
parser.add_option("--filters", metavar = "name", type = str, help = "GDS filters file to use")
parser.add_option("--error", type = float, default = 0.02, help = "Magnitude of the error to simulate")
parser.add_option("--line-amplitude", type = int, default = 300, help = "Amplitide of injected lines, in terms of the factor by which they rise above the local noise floor in an ASD")

options, filenames = parser.parse_args()

gps_start_time = 1269120000
gps_end_time = 1269127000
chop_time = 5000
filters_name = options.filters
error_magnitude = float(options.error)
line_amplitude = int(options.line_amplitude)


# Get the filters
filters = np.load("/home/aaron.viets/src/gstlal/gstlal-calibration/tests/check_calibration/gds-dcs-filter-generation/filters/PreER15/GDSFilters/%s" % filters_name)


#
# Find height above the noise floor for Pcal lines, minimising RMS uncertainty given
# the power we have and the injection frequencies.
#

def find_amplitudes(freqs, amplitude_rms):

	amplitude_ms = amplitude_rms * amplitude_rms
	amplitude_squared = len(freqs) * amplitude_ms
	asd = np.transpose(np.loadtxt("asd_derr"))[1]
	R = filters['response_function'][1] + 1j * filters['response_function'][2]
	asd *= abs(R)

	# Initialize very low-SNR lines, and add to them in a way that minimizes power
	dA2 = amplitude_ms / 1000
	A2 = np.ones(len(freqs)) * dA2
	while sum(A2) < amplitude_squared:
		power = (asd[np.asarray(np.round(4 * freqs), dtype = int)])**2 * A2 * freqs**4
		power1 = (asd[np.asarray(np.round(4 * freqs), dtype = int)])**2 * (A2 + dA2) * freqs**4
		unc_squared = 1.0 / A2
		unc_squared1 = 1.0 / (A2 + dA2)
		A2[np.argmax((unc_squared - unc_squared1) / (power1 - power))] += dA2

	return np.sqrt(A2)


#
# A function to add lines to d_err data.  Simulates the injection channels with
# lines at the given frequencies with the given amplitudes.  Transfer functions
# at those frequencies are used to compute the lines in the real data.  A
# provided DQ vector indicates when the lines are on.
#

def add_lines(pipeline, data, freqs, inj_rate, amplitude, tfs, dq, hoft_tfs = None):

	asd = np.transpose(np.loadtxt("asd_derr"))[1]
	amplitudes = []
	for i in range(len(freqs)):
		idx = int(round(4 * freqs[i]))
		noise = np.median(asd[idx - 20 : idx + 20])
		amplitudes.append(noise * amplitude[i] / abs(tfs[i]))

	dq = pipeparts.mkgeneric(pipeline, dq, "lal_typecast")
	dq = pipeparts.mktee(pipeline, pipeparts.mkcapsfilter(pipeline, dq, "audio/x-raw,format=F64LE"))
	inj_dq = pipeparts.mktee(pipeline, calibration_parts.mkresample(pipeline, dq, 0, False, inj_rate))
	data_dq = pipeparts.mktee(pipeline, calibration_parts.mkresample(pipeline, dq, 0, False, 16384))
	inj_lines = []
	data_lines = [data]
	hoft_lines = None if hoft_tfs is None else []
	for i in range(len(freqs)):
		inj_lines.append(pipeparts.mkgeneric(pipeline, pipeparts.mkgeneric(pipeline, inj_dq, "lal_demodulate", line_frequency = -freqs[i], prefactor_real = amplitudes[i], prefactor_imag = 0), "creal"))
		data_lines.append(pipeparts.mkgeneric(pipeline, pipeparts.mkgeneric(pipeline, data_dq, "lal_demodulate", line_frequency = -freqs[i], prefactor_real = amplitudes[i] * np.real(tfs[i]), prefactor_imag = amplitudes[i] * np.imag(tfs[i])), "creal"))
		if hoft_tfs is not None:
			hoft_lines.append(pipeparts.mkgeneric(pipeline, pipeparts.mkgeneric(pipeline, data_dq, "lal_demodulate", line_frequency = -freqs[i], prefactor_real = amplitudes[i] * np.real(hoft_tfs[i]), prefactor_imag = amplitudes[i] * np.imag(hoft_tfs[i])), "creal"))
	inj = calibration_parts.mkadder(pipeline, inj_lines)
	data = calibration_parts.mkadder(pipeline, data_lines)
	if hoft_lines is not None:
		hoft_lines = calibration_parts.mkadder(pipeline, hoft_lines)

	return inj, data, hoft_lines


#
# A function to simulate frequency-dependent error
#

def simulate_error(df, fmax, amplitude):
	n = int(round(fmax / df)) + 1
	x = np.zeros(n)
	x[0] = np.random.rand() - 0.5
	i = 1
	log = -1
	while i < n:
		newlog = int(np.log(i) / np.log(12))
		if newlog > log:
			x[i] = np.random.rand() - 0.5
			log = newlog
		else:
			x[i] = x[i - 1]
		i += 1
	mag = np.ones(n)
	for i in range(n):
		win = np.hanning(i+1)
		if i == 1:
			win[0] = 1
			win[1] = 1
		win /= sum(win)
		mag[i] += 2 * amplitude * sum(win * x[:i+1])

	x = np.zeros(n)
	x[0] = np.random.rand() - 0.5
	i = 1
	log = -1
	while i < n:
		newlog = int(np.log(i) / np.log(12))
		if newlog > log:
			x[i] = np.random.rand() - 0.5
			log = newlog
		else:
			x[i] = x[i - 1]
		i += 1
	phase = np.zeros(n)
	for i in range(n):
		win = np.hanning(i+1)
		if i == 1:
			win[0] = 1
			win[1] = 1
		win /= sum(win)
		phase[i] = 2 * amplitude * sum(win * x[:i+1])

	return mag * np.exp(1j * phase)

#
# A function to plot ASDs showing h(t) and injections
#

def plot_asd(hoft, pcal, tst, pum, uim, fmin, fmax):
	hoft_f = hoft[0]
	hoft = hoft[1]
	pcal_f = pcal[0]
	pcal = pcal[1]
	tst_f = tst[0]
	tst = tst[1]
	pum_f = pum[0]
	pum = pum[1]
	uim_f = uim[0]
	uim = uim[1]
	hoft_df = hoft_f[1] - hoft_f[0]
	pcal_df = pcal_f[1] - pcal_f[0]
	tst_df = tst_f[1] - tst_f[0]
	pum_df = pum_f[1] - pum_f[0]
	uim_df = uim_f[1] - uim_f[0]
	asd_min = min(hoft[int(fmin / hoft_df) : int(np.ceil(fmax / hoft_df))])
	asd_min = pow(10, np.floor(np.log10(asd_min)) - 1)
	asd_max = max(max(hoft[int(fmin / hoft_df) : int(np.ceil(fmax / hoft_df))]), max(pcal[int(fmin / pcal_df) : int(np.ceil(fmax / pcal_df))]), max(tst[int(fmin / tst_df) : int(np.ceil(fmax / tst_df))]), max(pum[int(fmin / pum_df) : int(np.ceil(fmax / pum_df))]), max(uim[int(fmin / uim_df) : int(np.ceil(fmax / uim_df))]))
	asd_max = pow(10, np.ceil(np.log10(asd_max)) + 1)

	f_scale = 'log' if fmax / fmin > 4 else 'linear'

	plt.figure(figsize = (10, 6))
	colors = ['red', 'green', 'gray', 'blue', 'purple']
	labels = ['h(t)', 'Pcal', 'TST', 'PUM', 'UIM']
	plt.plot(hoft_f, hoft, colors[0], linewidth = 0.75, label = labels[0])
	plt.plot(pcal_f, pcal, colors[1], linewidth = 0.75, label = labels[1])
	plt.plot(tst_f, tst, colors[2], linewidth = 0.75, label = labels[2])
	plt.plot(pum_f, pum, colors[3], linewidth = 0.75, label = labels[3])
	plt.plot(uim_f, uim, colors[4], linewidth = 0.75, label = labels[4])
	patches = [mpatches.Patch(color = colors[j], label = r'$%s$' % labels[j]) for j in range(len(labels))]
	plt.legend(handles = patches, loc = 'upper right', ncol = 1)
	plt.ylabel(r'${\rm ASD}\ \left[{\rm strain / }\sqrt{\rm Hz}\right]$')
	plt.xlabel(r'${\rm Frequency \ [Hz]}$')
	ticks_and_grid(plt.gca(), xmin = fmin, xmax = fmax, ymin = asd_min, ymax = asd_max, xscale = f_scale, yscale = 'log')
	plt.savefig("calibcorr_lines_ASD_%s_%d-%dHz_amplitude%d.png" % (filters_name.replace('.', '_'), int(fmin), int(fmax), line_amplitude))


# Errors in the models
if os.path.exists("C_error_%dpercent" % int(100 * error_magnitude)):
	C_error = np.loadtxt("C_error_%dpercent" % int(100 * error_magnitude), dtype=complex, converters={0: lambda s: complex(s.decode().replace('+-', '-'))})
else:
	C_error = simulate_error(0.25, 8192, error_magnitude)
	np.savetxt("C_error_%dpercent" % int(100 * error_magnitude), C_error)
if os.path.exists("tst_error_%dpercent" % int(100 * error_magnitude)):
	tst_error = np.loadtxt("tst_error_%dpercent" % int(100 * error_magnitude), dtype=complex, converters={0: lambda s: complex(s.decode().replace('+-', '-'))})
else:
	tst_error = simulate_error(0.25, 8192, error_magnitude)
	np.savetxt("tst_error_%dpercent" % int(100 * error_magnitude), tst_error)
if os.path.exists("pum_error_%dpercent" % int(100 * error_magnitude)):
	pum_error = np.loadtxt("pum_error_%dpercent" % int(100 * error_magnitude), dtype=complex, converters={0: lambda s: complex(s.decode().replace('+-', '-'))})
else:
	pum_error = simulate_error(0.25, 8192, error_magnitude)
	np.savetxt("pum_error_%dpercent" % int(100 * error_magnitude), pum_error)
if os.path.exists("uim_error_%dpercent" % int(100 * error_magnitude)):
	uim_error = np.loadtxt("uim_error_%dpercent" % int(100 * error_magnitude), dtype=complex, converters={0: lambda s: complex(s.decode().replace('+-', '-'))})
else:
	uim_error = simulate_error(0.25, 8192, error_magnitude)
	np.savetxt("uim_error_%dpercent" % int(100 * error_magnitude), uim_error)

#
# Read real data from raw frames and calibrate it with real GDS calibration filters.
# No TDCFs are computed or applied for simplicity.  Simulate small deviations from
# the models of C, A_TST, A_PUM, and A_UIM.  Apply these to simulated comb injections
# From Pcal, x_TST, x_PUM, and x_UIM to compute and add their effect into d_err. Use
# lal_calibcorr to compute and compensate for this error and test whether it works.
#

def lal_calibcorr_01(pipeline, name):

	# Get stuff we need from the filters.
	if "res_highpass" in filters:
		invsens_highpass = filters["res_highpass"]
		invsens_highpass_delay = int(filters['res_highpass_delay'])
		invsens_highpass_sr = int(filters['res_highpass_sr']) if 'res_highpass_sr' in filters else hoft_sr
	elif "inv_sensing_highpass" in filters:
		invsens_highpass = filters["inv_sensing_highpass"]
		invsens_highpass_delay = int(filters['invsens_highpass_delay'])
		invsens_highpass_sr = int(filters['invsens_highpass_sr']) if 'invsens_highpass_sr' in filters else hoft_sr
	else:
		invsens_highpass = []
	reschaindelay = int(filters["res_corr_delay"])
	reschainfilt = filters["res_corr_filter"]
	hoft_sr = 16384
	tstdelay = pumdelay = uimdelay = int(filters["ctrl_corr_delay"])
	tstfilt = filters["TST_corr_filter"] if "TST_corr_filter" in filters else filters["ctrl_corr_filter"]
	pumfilt = filters["PUM_corr_filter"] if "PUM_corr_filter" in filters else filters["ctrl_corr_filter"]
	uimfilt = filters["UIM_corr_filter"] if "UIM_corr_filter" in filters else filters["ctrl_corr_filter"]
	tstchainsr = pumchainsr = uimchainsr = int(filters["ctrl_corr_sr"])
	reschainsr = 16384

	channel_list = []
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_TST_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_PUM_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_CTRL_UIM_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DELTAL_RESIDUAL_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DARM_ERR_DBL_DQ'))
	channel_list.append(('H1', 'CAL-DARM_CTRL_DBL_DQ'))

	# Models of calibration components
	cres_calibcorr = filters['cres_calibcorr']
	all_freqs = cres_calibcorr[0]
	cres_calibcorr = cres_calibcorr[1] + 1j * cres_calibcorr[2]
	D_calibcorr = filters['D_calibcorr']
	D_calibcorr = D_calibcorr[1] + 1j * D_calibcorr[2]
	tst_calibcorr = filters['tst_calibcorr']
	tst_calibcorr = tst_calibcorr[1] + 1j * tst_calibcorr[2]
	Ftst_calibcorr = filters['Ftst_calibcorr']
	Ftst_calibcorr = Ftst_calibcorr[1] + 1j * Ftst_calibcorr[2]
	pum_calibcorr = filters['pum_calibcorr']
	pum_calibcorr = pum_calibcorr[1] + 1j * pum_calibcorr[2]
	Fpum_calibcorr = filters['Fpum_calibcorr']
	Fpum_calibcorr = Fpum_calibcorr[1] + 1j * Fpum_calibcorr[2]
	uim_calibcorr = filters['uim_calibcorr']
	uim_calibcorr = uim_calibcorr[1] + 1j * uim_calibcorr[2]
	Fuim_calibcorr = filters['Fuim_calibcorr']
	Fuim_calibcorr = Fuim_calibcorr[1] + 1j * Fuim_calibcorr[2]

	pcal_corr_calibcorr = filters['pcal_corr_calibcorr']
	daqdownsampling_tst_calibcorr = filters['daqdownsampling_tst_calibcorr']
	daqdownsampling_pum_calibcorr = filters['daqdownsampling_pum_calibcorr']
	daqdownsampling_uim_calibcorr = filters['daqdownsampling_uim_calibcorr']

	pcal_freqs = pcal_corr_calibcorr[0]
	tst_freqs = daqdownsampling_tst_calibcorr[0]
	pum_freqs = daqdownsampling_pum_calibcorr[0]
	uim_freqs = daqdownsampling_uim_calibcorr[0]
	pcal_corr_calibcorr = pcal_corr_calibcorr[1] + 1j * pcal_corr_calibcorr[2]
	daqdownsampling_tst_calibcorr = daqdownsampling_tst_calibcorr[1] + 1j * daqdownsampling_tst_calibcorr[2]
	daqdownsampling_pum_calibcorr = daqdownsampling_pum_calibcorr[1] + 1j * daqdownsampling_pum_calibcorr[2]
	daqdownsampling_uim_calibcorr = daqdownsampling_uim_calibcorr[1] + 1j * daqdownsampling_uim_calibcorr[2]
	pcal_indices = []
	tst_indices = []
	pum_indices = []
	uim_indices = []
	for i in range(len(all_freqs)):
		if all_freqs[i] in pcal_freqs:
			pcal_indices.append(i)
		elif all_freqs[i] in tst_freqs:
			tst_indices.append(i)
		elif all_freqs[i] in pum_freqs:
			pum_indices.append(i)
		elif all_freqs[i] in uim_freqs:
			uim_indices.append(i)
	pcal_indices = np.asarray(pcal_indices)
	tst_indices = np.asarray(tst_indices)
	pum_indices = np.asarray(pum_indices)
	uim_indices = np.asarray(uim_indices)

	# Errors in the models
	C_error_calibcorr = []
	tst_error_calibcorr = []
	pum_error_calibcorr = []
	uim_error_calibcorr = []
	for f in all_freqs:
		w1 = 4 * f - int(4 * f)
		w0 = 1.0 - w1
		C_error_calibcorr.append(w0 * C_error[int(4 * f)] + w1 * C_error[1 + int(4 * f)])
		tst_error_calibcorr.append(w0 * tst_error[int(4 * f)] + w1 * tst_error[1 + int(4 * f)])
		pum_error_calibcorr.append(w0 * pum_error[int(4 * f)] + w1 * pum_error[1 + int(4 * f)])
		uim_error_calibcorr.append(w0 * uim_error[int(4 * f)] + w1 * uim_error[1 + int(4 * f)])
	C_error_calibcorr = np.asarray(C_error_calibcorr)
	tst_error_calibcorr = np.asarray(tst_error_calibcorr)
	pum_error_calibcorr = np.asarray(pum_error_calibcorr)
	uim_error_calibcorr = np.asarray(uim_error_calibcorr)

	# Apply the errors
	cres_calibcorr *= C_error_calibcorr
	tst_calibcorr *= tst_error_calibcorr
	pum_calibcorr *= pum_error_calibcorr
	uim_calibcorr *= uim_error_calibcorr

	# Find the response function R
	fcc = float(filters['fcc'])
	fs_squared = float(filters['fs_squared'])
	srcQ = float(filters['srcQ'])
	if float(filters['fs_squared']) < 0:
		srcQ = -1j * srcQ
	fs_over_Q = np.real(np.sqrt(complex(fs_squared)) / srcQ)

	R_calibcorr = (1.0 + 1j * all_freqs / fcc) / cres_calibcorr / all_freqs / all_freqs * (all_freqs * all_freqs + fs_squared - 1j * all_freqs * fs_over_Q) + D_calibcorr * (tst_calibcorr * Ftst_calibcorr + pum_calibcorr * Fpum_calibcorr + uim_calibcorr * Fuim_calibcorr)

	pcal_tfs = pcal_corr_calibcorr / R_calibcorr[pcal_indices]
	tst_tfs = tst_calibcorr[tst_indices] / R_calibcorr[tst_indices] / daqdownsampling_tst_calibcorr
	pum_tfs = pum_calibcorr[pum_indices] / R_calibcorr[pum_indices] / daqdownsampling_pum_calibcorr
	uim_tfs = uim_calibcorr[uim_indices] / R_calibcorr[uim_indices] / daqdownsampling_uim_calibcorr

	data = pipeparts.mklalcachesrc(pipeline, location = "H1_easy_raw_frames.cache", cache_dsc_regex = 'H1', use_mmap = True)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = False, channel_list = list(map("%s:%s".__mod__, channel_list)))

	derr = calibration_parts.hook_up(pipeline, data, 'CAL-DARM_ERR_DBL_DQ', 'H1', 1.0)
	derr = pipeparts.mktee(pipeline, calibration_parts.caps_and_progress(pipeline, derr, "audio/x-raw,format=F64LE,rate=16384", "darm_err"))
	dctrl = calibration_parts.hook_up(pipeline, data, 'CAL-DARM_CTRL_DBL_DQ', 'H1', 1.0)
	dctrl = pipeparts.mktee(pipeline, calibration_parts.caps_and_progress(pipeline, dctrl, "audio/x-raw,format=F64LE,rate=16384", "darm_ctrl"))
	pipeparts.mkfakesink(pipeline, dctrl) # In case we don't use it
	res = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_RESIDUAL_DBL_DQ', 'H1', 1.0)
	res = calibration_parts.caps_and_progress(pipeline, res, "audio/x-raw,format=F64LE,rate=16384", "res")
	tst = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_TST_DBL_DQ', 'H1', 1.0)
	tst = calibration_parts.caps_and_progress(pipeline, tst, "audio/x-raw,format=F64LE,rate=4096", "tst")
	tst = calibration_parts.mkresample(pipeline, tst, 5, False, 2048)
	pum = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_PUM_DBL_DQ', 'H1', 1.0)
	pum = calibration_parts.caps_and_progress(pipeline, pum, "audio/x-raw,format=F64LE,rate=4096", "pum")
	pum = calibration_parts.mkresample(pipeline, pum, 5, False, 2048)
	uim = calibration_parts.hook_up(pipeline, data, 'CAL-DELTAL_CTRL_UIM_DBL_DQ', 'H1', 1.0)
	uim = calibration_parts.caps_and_progress(pipeline, uim, "audio/x-raw,format=F64LE,rate=4096", "uim")
	uim = calibration_parts.mkresample(pipeline, uim, 5, False, 2048)

	# Apply static calibration filters
	tst = pipeparts.mkfirbank(pipeline, tst, latency = tstdelay, fir_matrix = [tstfilt[::-1]], time_domain = True)
	pum = pipeparts.mkfirbank(pipeline, pum, latency = pumdelay, fir_matrix = [pumfilt[::-1]], time_domain = True)
	uim = pipeparts.mkfirbank(pipeline, uim, latency = uimdelay, fir_matrix = [uimfilt[::-1]], time_domain = True)
	# Apply a high-pass filter to the residual path separately.
	if any(invsens_highpass):
		if invsens_highpass_sr != hoft_sr:
			# Magic trick to apply a high-pass filter to the inverse sensing path at a lower sample rate without losing information above the Nyquist frequency.
			res = pipeparts.mktee(pipeline, res)
			res_lowfreq = calibration_parts.mkresample(pipeline, res, 4, False, invsens_highpass_sr, frequency_resolution = (invsens_highpass_sr / 2.0 - 10.0) / 2)
			# Use spectral inversion to make a low-pass filter with a gain of -1.
			invsens_highpass[invsens_highpass_delay] = invsens_highpass[invsens_highpass_delay] - 1.0
			# Apply this filter to the inverse sensing path at a lower sample rate to get only the low frequency components
			res_lowfreq = pipeparts.mkfirbank(pipeline, res_lowfreq, latency = invsens_highpass_delay, fir_matrix = [invsens_highpass[::-1]], time_domain = True)
			# Upsample
			res_lowfreq = calibration_parts.mkresample(pipeline, res_lowfreq, 4, False, hoft_sr, frequency_resolution = (invsens_highpass_sr / 2.0 - 10.0) / 2)
			# Add to the inverse sensing path to get rid of the low frequencies
			res = calibration_parts.mkadder(pipeline, calibration_parts.list_srcs(pipeline, res, res_lowfreq))
	res = pipeparts.mkfirbank(pipeline, res, latency = reschaindelay, fir_matrix = [reschainfilt[::-1]], time_domain = True)

	res = pipeparts.mktee(pipeline, res)
	tst = pipeparts.mktee(pipeline, tst)
	pum = pipeparts.mktee(pipeline, pum)
	uim = pipeparts.mktee(pipeline, uim)

	tst_up = calibration_parts.mkresample(pipeline, tst, 4, False, hoft_sr)
	pum_up = calibration_parts.mkresample(pipeline, pum, 4, False, hoft_sr)
	uim_up = calibration_parts.mkresample(pipeline, uim, 4, False, hoft_sr)

	deltal_model = calibration_parts.mkadder(pipeline, [res, tst_up, pum_up, uim_up])
	deltal_model = pipeparts.mktee(pipeline, deltal_model)

	# Simulate injection channels with fake lines and add these into d_err.
	dq = calibration_parts.mkpow(pipeline, derr, exponent = 0)
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_demodulate", line_frequency = -0.00005, prefactor_real = 0.89034, prefactor_imag = 0.4553)
	dq = pipeparts.mkgeneric(pipeline, dq, "creal")
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_add_constant", value = 0.2)
	dq = calibration_parts.mkresample(pipeline, dq, 0, False, 16)
	dq = pipeparts.mkgeneric(pipeline, dq, "lal_typecast")
	dq = pipeparts.mkcapsfilter(pipeline, dq, "audio/x-raw,format=U32LE")
	dq = pipeparts.mktee(pipeline, dq)
	pipeparts.mknxydumpsink(pipeline, dq, "dq.txt")

	amplitudes = find_amplitudes(pcal_freqs, line_amplitude)
	pcal, derr, hoft_pcal_lines = add_lines(pipeline, derr, pcal_freqs, 16384, amplitudes, pcal_tfs, dq, hoft_tfs = pcal_tfs * R_calibcorr[pcal_indices] / float(filters['arm_length']))
	tstexc, derr, hoft_tstexc_lines = add_lines(pipeline, derr, tst_freqs, 2048, amplitudes, tst_tfs, dq, hoft_tfs = tst_tfs * R_calibcorr[tst_indices] / float(filters['arm_length']))
	pumexc, derr, hoft_pumexc_lines = add_lines(pipeline, derr, pum_freqs, 1024, amplitudes, pum_tfs, dq, hoft_tfs = pum_tfs * R_calibcorr[pum_indices] / float(filters['arm_length']))
	uimexc, derr, hoft_uimexc_lines = add_lines(pipeline, derr, uim_freqs, 512, amplitudes, uim_tfs, dq, hoft_tfs = uim_tfs * R_calibcorr[uim_indices] / float(filters['arm_length']))

	derr = pipeparts.mktee(pipeline, derr)

	res_calibcorr, tst_calibcorr, pum_calibcorr, uim_calibcorr = calibration_parts.remove_systematic_error(pipeline, res, 16384, tst, tstchainsr, pum, pumchainsr, uim, uimchainsr, derr, pcal, 16384, dq, tstexc, 2048, dq, pumexc, 1024, dq, uimexc, 512, dq, 16, None, None, None, None, None, None, None, None, None, None, filters, 0.125, filename = "calibcorr_filters.txt", use_median = True)

	tst_calibcorr = pipeparts.mktee(pipeline, tst_calibcorr)
	pum_calibcorr = pipeparts.mktee(pipeline, pum_calibcorr)
	uim_calibcorr = pipeparts.mktee(pipeline, uim_calibcorr)
	res_calibcorr = pipeparts.mktee(pipeline, res_calibcorr)

	tst_up = calibration_parts.mkresample(pipeline, tst_calibcorr, 4, False, hoft_sr)
	pum_up = calibration_parts.mkresample(pipeline, pum_calibcorr, 4, False, hoft_sr)
	uim_up = calibration_parts.mkresample(pipeline, uim_calibcorr, 4, False, hoft_sr)

	deltal_calibcorr = calibration_parts.mkadder(pipeline, [res_calibcorr, tst_up, pum_up, uim_up])

	tf_dur = gps_end_time - gps_start_time - chop_time - 50
	num_ffts = int((tf_dur - 56) / 8)
	tf_dur = 56 + 8 * num_ffts

	tst_tf = calibration_parts.mkinterleave(pipeline, [tst, calibration_parts.mkresample(pipeline, dctrl, 5, False, tstchainsr, window = 3, f_cut = 0.95)])
	tst_tf = pipeparts.mkprogressreport(pipeline, tst_tf, "progress_tst_tf")
	calibration_parts.mktransferfunction(pipeline, tst_tf, fft_length = 64 * tstchainsr, fft_overlap = 56 * tstchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * tstchainsr, filename = "%s_tst_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "tst_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)
	tst_calibcorr_tf = calibration_parts.mkinterleave(pipeline, [tst_calibcorr, calibration_parts.mkresample(pipeline, dctrl, 5, False, tstchainsr, window = 3, f_cut = 0.95)])
	tst_calibcorr_tf = pipeparts.mkprogressreport(pipeline, tst_calibcorr_tf, "progress_tst_calibcorr_tf")
	calibration_parts.mktransferfunction(pipeline, tst_calibcorr_tf, fft_length = 64 * tstchainsr, fft_overlap = 56 * tstchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * tstchainsr, filename = "%s_calibcorr_tst_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "tst_calibcorr_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)

	pum_tf = calibration_parts.mkinterleave(pipeline, [pum, calibration_parts.mkresample(pipeline, dctrl, 5, False, pumchainsr, window = 3, f_cut = 0.95)])
	pum_tf = pipeparts.mkprogressreport(pipeline, pum_tf, "progress_pum_tf")
	calibration_parts.mktransferfunction(pipeline, pum_tf, fft_length = 64 * pumchainsr, fft_overlap = 56 * pumchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * pumchainsr, filename = "%s_pum_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "pum_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)
	pum_calibcorr_tf = calibration_parts.mkinterleave(pipeline, [pum_calibcorr, calibration_parts.mkresample(pipeline, dctrl, 5, False, pumchainsr, window = 3, f_cut = 0.95)])
	pum_calibcorr_tf = pipeparts.mkprogressreport(pipeline, pum_calibcorr_tf, "progress_pum_calibcorr_tf")
	calibration_parts.mktransferfunction(pipeline, pum_calibcorr_tf, fft_length = 64 * pumchainsr, fft_overlap = 56 * pumchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * pumchainsr, filename = "%s_calibcorr_pum_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "pum_calibcorr_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)

	uim_tf = calibration_parts.mkinterleave(pipeline, [uim, calibration_parts.mkresample(pipeline, dctrl, 5, False, uimchainsr, window = 3, f_cut = 0.95)])
	uim_tf = pipeparts.mkprogressreport(pipeline, uim_tf, "progress_uim_tf")
	calibration_parts.mktransferfunction(pipeline, uim_tf, fft_length = 64 * uimchainsr, fft_overlap = 56 * uimchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * uimchainsr, filename = "%s_uim_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "uim_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)
	uim_calibcorr_tf = calibration_parts.mkinterleave(pipeline, [uim_calibcorr, calibration_parts.mkresample(pipeline, dctrl, 5, False, uimchainsr, window = 3, f_cut = 0.95)])
	uim_calibcorr_tf = pipeparts.mkprogressreport(pipeline, uim_calibcorr_tf, "progress_uim_calibcorr_tf")
	calibration_parts.mktransferfunction(pipeline, uim_calibcorr_tf, fft_length = 64 * uimchainsr, fft_overlap = 56 * uimchainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * uimchainsr, filename = "%s_calibcorr_uim_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "uim_calibcorr_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)

	res_tf = calibration_parts.mkinterleave(pipeline, [res, calibration_parts.mkresample(pipeline, derr, 5, False, reschainsr, window = 3, f_cut = 0.95)])
	res_tf = pipeparts.mkprogressreport(pipeline, res_tf, "progress_res_tf")
	calibration_parts.mktransferfunction(pipeline, res_tf, fft_length = 64 * reschainsr, fft_overlap = 56 * reschainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * reschainsr, filename = "%s_res_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "res_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)
	res_calibcorr_tf = calibration_parts.mkinterleave(pipeline, [res_calibcorr, calibration_parts.mkresample(pipeline, derr, 5, False, reschainsr, window = 3, f_cut = 0.95)])
	res_calibcorr_tf = pipeparts.mkprogressreport(pipeline, res_calibcorr_tf, "progress_res_calibcorr_tf")
	calibration_parts.mktransferfunction(pipeline, res_calibcorr_tf, fft_length = 64 * reschainsr, fft_overlap = 56 * reschainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * reschainsr, filename = "%s_calibcorr_res_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "res_calibcorr_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)

	response_tf = calibration_parts.mkinterleave(pipeline, [deltal_model, calibration_parts.mkresample(pipeline, derr, 5, False, reschainsr, window = 3, f_cut = 0.95)])
	response_tf = pipeparts.mkprogressreport(pipeline, response_tf, "progress_response_tf")
	calibration_parts.mktransferfunction(pipeline, response_tf, fft_length = 64 * reschainsr, fft_overlap = 56 * reschainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * reschainsr, filename = "%s_response_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "response_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)
	response_calibcorr_tf = calibration_parts.mkinterleave(pipeline, [deltal_calibcorr, calibration_parts.mkresample(pipeline, derr, 5, False, reschainsr, window = 3, f_cut = 0.95)])
	response_calibcorr_tf = pipeparts.mkprogressreport(pipeline, response_calibcorr_tf, "progress_response_calibcorr_tf")
	calibration_parts.mktransferfunction(pipeline, response_calibcorr_tf, fft_length = 64 * reschainsr, fft_overlap = 56 * reschainsr, num_ffts = num_ffts, use_median = True, update_samples = 1e15, update_delay_samples = chop_time * reschainsr, filename = "%s_calibcorr_response_filters_transfer_function_%d-%d.txt" % (filters_name.split('/')[-1].replace('.', '_'), gps_start_time + chop_time, tf_dur), name = "response_calibcorr_filters_tf", use_fir_fft = True, fft_window_type = 0, frequency_resolution = 0.25)

	# Make ASDs to see line heights
	hoft = pipeparts.mkaudioamplify(pipeline, deltal_model, 1 / float(filters['arm_length']))
	hoft = calibration_parts.mkgate(pipeline, hoft, dq, 1)
	pipeparts.mkgeneric(pipeline, hoft, "lal_asd", fft_samples = 128 * 16384, overlap_samples = 96 * 16384, window_type = 0, frequency_resolution = 1.0 / 16, filename = "hoft_asd.txt")
	hoft_pcal_lines = calibration_parts.mkgate(pipeline, hoft_pcal_lines, dq, 1)
	pipeparts.mkgeneric(pipeline, hoft_pcal_lines, "lal_asd", fft_samples = 128 * 16384, overlap_samples = 96 * 16384, window_type = 0, frequency_resolution = 1.0 / 16, filename = "pcal_asd.txt")
	hoft_tstexc_lines = calibration_parts.mkgate(pipeline, hoft_tstexc_lines, dq, 1)
	pipeparts.mkgeneric(pipeline, hoft_tstexc_lines, "lal_asd", fft_samples = 128 * 16384, overlap_samples = 96 * 16384, window_type = 0, frequency_resolution = 1.0 / 16, filename = "tstexc_asd.txt")
	hoft_pumexc_lines = calibration_parts.mkgate(pipeline, hoft_pumexc_lines, dq, 1)
	pipeparts.mkgeneric(pipeline, hoft_pumexc_lines, "lal_asd", fft_samples = 128 * 16384, overlap_samples = 96 * 16384, window_type = 0, frequency_resolution = 1.0 / 16, filename = "pumexc_asd.txt")
	hoft_uimexc_lines = calibration_parts.mkgate(pipeline, hoft_uimexc_lines, dq, 1)
	pipeparts.mkgeneric(pipeline, hoft_uimexc_lines, "lal_asd", fft_samples = 128 * 16384, overlap_samples = 96 * 16384, window_type = 0, frequency_resolution = 1.0 / 16, filename = "uimexc_asd.txt")

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(lal_calibcorr_01, "lal_calibcorr_01", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * gps_start_time), LIGOTimeGPS(0, 1000000000 * gps_end_time))))


# Plot ASDs
hoft_asd = np.transpose(np.loadtxt("hoft_asd.txt"))
pcal_asd = np.transpose(np.loadtxt("pcal_asd.txt"))
tst_asd = np.transpose(np.loadtxt("tstexc_asd.txt"))
pum_asd = np.transpose(np.loadtxt("pumexc_asd.txt"))
uim_asd = np.transpose(np.loadtxt("uimexc_asd.txt"))

plot_asd(hoft_asd, pcal_asd, tst_asd, pum_asd, uim_asd, 10, 8000)

# Find clusters of lines and zoom in on them
cres_calibcorr = filters['cres_calibcorr']
all_freqs = cres_calibcorr[0]
i = 0
while i < len(all_freqs):
	fmin = fmax = all_freqs[i]
	i += 1
	if i < len(all_freqs):
		fmax = all_freqs[i]
		df = fmax - fmin
	while i < len(all_freqs) and all_freqs[i] - all_freqs[i - 1] < 4 * df:
		fmax = all_freqs[i]
		i += 1
	f_range = fmax - fmin
	fmin -= 0.5 + f_range / 6
	fmax += 0.5 + f_range / 3
	plot_asd(hoft_asd, pcal_asd, tst_asd, pum_asd, uim_asd, fmin, fmax)


# Compute Models including errors
invsens_model = filters['invsens_model']
tst_model = filters['tst_model']
pum_model = filters['pum_model']
uim_model = filters['uim_model']
D_model = filters['D_model']

cinv_with_error = (invsens_model[1] + 1j * invsens_model[2]) / C_error
tst_with_error = tst_error * (tst_model[1] + 1j * tst_model[2])
pum_with_error = pum_error * (pum_model[1] + 1j * pum_model[2])
uim_with_error = uim_error * (uim_model[1] + 1j * uim_model[2])
R_with_error = cinv_with_error + (D_model[1] + 1j * D_model[2]) * (tst_with_error + pum_with_error + uim_with_error)

cinv_with_error = np.array((invsens_model[0], np.real(cinv_with_error), np.imag(cinv_with_error)))
tst_with_error = np.array((invsens_model[0], np.real(tst_with_error), np.imag(tst_with_error)))
pum_with_error = np.array((invsens_model[0], np.real(pum_with_error), np.imag(pum_with_error)))
uim_with_error = np.array((invsens_model[0], np.real(uim_with_error), np.imag(uim_with_error)))
R_with_error = np.array((invsens_model[0], np.real(R_with_error), np.imag(R_with_error)))

# Identify any files that have filters transfer functions in them
os.system("rm *_ratio_magnitude.txt *_ratio_phase.txt *_reformatted.txt")
tf_files0 = [f for f in os.listdir('.') if (os.path.isfile(f) and ('GDS' in f or 'DCS' in f) and 'npz' in f and 'filters_transfer_function' in f and '.txt' in f)]
tf_files = []

k = 0
indices = []
# Put the TF files in order
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_res_" in tf_files0[i] and not ("calibcorr" in tf_files0[i]):
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_res_" in tf_files0[i] and "calibcorr" in tf_files0[i]:
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
indices.append(k)
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_tst_" in tf_files0[i] and not ("calibcorr" in tf_files0[i]):
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_tst_" in tf_files0[i] and "calibcorr" in tf_files0[i]:
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
indices.append(k)
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_pum_" in tf_files0[i] and not ("calibcorr" in tf_files0[i]):
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_pum_" in tf_files0[i] and "calibcorr" in tf_files0[i]:
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
indices.append(k)
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_uim_" in tf_files0[i] and not ("calibcorr" in tf_files0[i]):
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
for i in range(0, len(tf_files0)):
	if '_filters_transfer_function_' in tf_files0[i] and "_uim_" in tf_files0[i] and "calibcorr" in tf_files0[i]:
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
indices.append(k)
for i in range(0, len(tf_files0)):
	if '_response_filters_transfer_function_' in tf_files0[i] and not ("calibcorr" in tf_files0[i]):
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
for i in range(0, len(tf_files0)):
	if '_response_filters_transfer_function_' in tf_files0[i] and "calibcorr" in tf_files0[i]:
		response_file = tf_files0[i]
		tf_files.append(response_file)
		k += 1
indices.append(k)

# Plot limits
freq_min = 10
freq_max = 5000
phase_min = -200
phase_max = 200
ratio_freq_min = 10
ratio_freq_max = 5000
ratio_mag_min = 1.0 - 4 * error_magnitude
ratio_mag_max = 1.0 + 4 * error_magnitude
response_ratio_mag_min = 1.0 - 8 * error_magnitude
response_ratio_mag_max = 1.0 + 8 * error_magnitude
ratio_phase_min = -300 * error_magnitude
ratio_phase_max = 300 * error_magnitude
response_ratio_phase_min = -600 * error_magnitude
response_ratio_phase_max = 600 * error_magnitude

k = 0
for tf_file in tf_files:
	k += 1
	model_jump_delay = 0.0
	# Determine what transfer function this is
	if '_tst_' in tf_file:
		plot_title = "TST Transfer Function"
	elif '_pum_' in tf_file:
		plot_title = "PUM Transfer Function"
	elif '_uim_' in tf_file:
		plot_title = "UIM Transfer Function"
	elif '_res_' in tf_file:
		plot_title = "Inverse Sensing Transfer Function"
	elif '_response_filters_transfer_function_' in tf_file:
		plot_title = "Response Function"
	else:
		plot_title = "Transfer Function"

	ifo = ''
	if 'H1' in tf_file:
		ifo = 'H1'
	if 'L1' in tf_file:
		ifo = 'L1'

	if 'calibcorr' in tf_file:
		cal_version = 'CalibCorr'
		color = 'maroon'
	else:
		cal_version = 'Model'
		color = 'royalblue'

	if '_tst_' in tf_file:
		component = 'TST'
		model = tst_with_error
	elif '_pum_' in tf_file:
		component = 'PUM'
		model = pum_with_error
	elif '_uim_' in tf_file:
		component = 'UIM'
		model = uim_with_error
	elif '_res_' in tf_file:
		component = 'C inv'
		model = cinv_with_error
	elif '_response_filters_transfer_function_' in tf_file:
		component = 'Response'
		model = R_with_error

	# Remove unwanted lines from transfer function file, and re-format wanted lines
	f = open(tf_file, 'r')
	lines = f.readlines()
	f.close()
	tf_length = len(lines) - 5
	f = open(tf_file.replace('.txt', '_reformatted.txt'), 'w')
	for j in range(3, 3 + tf_length):
		f.write(lines[j].replace(' + ', '\t').replace(' - ', '\t-').replace('i', ''))
	f.close()

	# Read data from re-formatted file and find frequency vector, magnitude, and phase
	data = np.loadtxt(tf_file.replace('.txt', '_reformatted.txt'))
	os.remove(tf_file.replace('.txt', '_reformatted.txt'))
	filters_tf = []
	frequency = []
	magnitude = []
	phase = []
	df = data[1][0] - data[0][0] # frequency spacing
	for j in range(0, len(data)):
		frequency.append(data[j][0])
		tf_at_f = (data[j][1] + 1j * data[j][2]) * np.exp(-2j * np.pi * data[j][0] * model_jump_delay)
		filters_tf.append(tf_at_f)
		magnitude.append(abs(tf_at_f))
		phase.append(np.angle(tf_at_f) * 180.0 / np.pi)

	# Find frequency-domain models in filters file if they are present and resample if necessary
	model_tf = []
	model_magnitude = []
	model_phase = []
	ratio = []
	ratio_magnitude = []
	ratio_phase = []
	# Check the frequency spacing of the model
	model_df = model[0][1] - model[0][0]
	cadence = df / model_df
	index = 0
	# This is a linear resampler (it just connects the dots with straight lines)
	while index < tf_length:
		before_idx = int(np.floor(cadence * index))
		after_idx = int(np.ceil(cadence * index + 1e-10))
		# Check if we've hit the end of the model transfer function
		if after_idx >= len(model[0]):
			if before_idx == cadence * index:
				model_tf.append(model[1][before_idx] + 1j * model[2][before_idx])
				model_magnitude.append(abs(model_tf[index]))
				model_phase.append(np.angle(model_tf[index]) * 180.0 / np.pi)
				ratio.append(filters_tf[index] / model_tf[index])
				ratio_magnitude.append(abs(ratio[index]))
				ratio_phase.append(np.angle(ratio[index]) * 180.0 / np.pi)
			index = tf_length
		else:
			before = model[1][before_idx] + 1j * model[2][before_idx]
			after = model[1][after_idx] + 1j * model[2][after_idx]
			before_weight = after_idx - cadence * index
			after_weight = cadence * index - before_idx
			model_tf.append(before_weight * before + after_weight * after)
			model_magnitude.append(abs(model_tf[index]))
			model_phase.append(np.angle(model_tf[index]) * 180.0 / np.pi)
			ratio.append(filters_tf[index] / model_tf[index])
			ratio_magnitude.append(abs(ratio[index]))
			ratio_phase.append(np.angle(ratio[index]) * 180.0 / np.pi)
			index += 1

	np.savetxt(tf_file.replace('.txt', '_ratio_magnitude.txt'), np.transpose(np.array([frequency, ratio_magnitude])), fmt='%.5e', delimiter='\t')
	np.savetxt(tf_file.replace('.txt', '_ratio_phase.txt'), np.transpose(np.array([frequency, ratio_phase])), fmt='%.5f', delimiter='\t')

	# Filter transfer function plots
	if k == 1 or k in np.array(indices) + 1:
		plt.figure(figsize = (10, 8))
	plt.subplot(221)
	plt.plot(frequency, magnitude, color, linewidth = 1.0, label = r'${\rm %s \ %s \ %s}$' % (ifo, cal_version, component))
	leg = plt.legend(fancybox = True)
	leg.get_frame().set_alpha(0.5)
	plt.ylabel(r'${\rm Magnitude \ [m/ct]}$')
	ticks_and_grid(plt.gca(), xmin = freq_min, xmax = freq_max, xscale = 'log', yscale = 'log')
	ax = plt.subplot(223)
	plt.plot(frequency, phase, color, linewidth = 1.0)
	plt.ylabel(r'${\rm Phase [deg]}$')
	plt.xlabel(r'${\rm Frequency \ [Hz]}$')
	ticks_and_grid(plt.gca(), xmin = freq_min, xmax = freq_max, ymin = phase_min, ymax = phase_max, xscale = 'log', yscale = 'linear')

	# Plots of the ratio filters / model
	plt.subplot(222)
	plt.plot(frequency, ratio_magnitude, color, linewidth = 1.0, label = r'${\rm %s \ %s / Actual}$' % (ifo, cal_version))
	leg = plt.legend(fancybox = True)
	leg.get_frame().set_alpha(0.5)
	if component == 'Response':
		ticks_and_grid(plt.gca(), xmin = ratio_freq_min, xmax = ratio_freq_max, ymin = response_ratio_mag_min, ymax = response_ratio_mag_max, xscale = 'log', yscale = 'linear')
	else:
		ticks_and_grid(plt.gca(), xmin = ratio_freq_min, xmax = ratio_freq_max, ymin = ratio_mag_min, ymax = ratio_mag_max, xscale = 'log', yscale = 'linear')
	ax = plt.subplot(224)
	plt.plot(frequency, ratio_phase, color, linewidth = 1.0)
	plt.xlabel(r'${\rm Frequency \ [Hz]}$')
	if component == 'Response':
		ticks_and_grid(plt.gca(), xmin = ratio_freq_min, xmax = ratio_freq_max, ymin = response_ratio_phase_min, ymax = response_ratio_phase_max, xscale = 'log', yscale = 'linear')
	else:
		ticks_and_grid(plt.gca(), xmin = ratio_freq_min, xmax = ratio_freq_max, ymin = ratio_phase_min, ymax = ratio_phase_max, xscale = 'log', yscale = 'linear')

	if k in indices:
		# Now add the true model
		plt.subplot(221)
		plt.plot(frequency, model_magnitude, 'orangered', linewidth = 1.0, ls = '--', label = r'${\rm %s \ Actual \ %s}$' % (ifo, component))
		leg = plt.legend(fancybox = True)
		leg.get_frame().set_alpha(0.5)
		#plt.title(plot_title)
		plt.ylabel(r'${\rm Magnitude \ [m/ct]}$')
		mag_min = min(model_magnitude[int(np.floor(freq_min / df)) : int(np.ceil(freq_max / df))])
		mag_max = max(model_magnitude[int(np.floor(freq_min / df)) : int(np.ceil(freq_max / df))])
		mag_min = pow(10, np.floor(np.log10(mag_min)) - 1)
		mag_max = pow(10, np.ceil(np.log10(mag_max)) + 1)
		ticks_and_grid(plt.gca(), xmin = freq_min, xmax = freq_max, ymin = mag_min, ymax = mag_max, xscale = 'log', yscale = 'log')
		ax = plt.subplot(223)
		plt.plot(frequency, model_phase, 'orangered', linewidth = 1.0, ls = '--')
		plt.ylabel(r'${\rm Phase \ [deg]}$')
		plt.xlabel(r'${\rm Frequency \ [Hz]}$')
		ticks_and_grid(plt.gca(), xmin = freq_min, xmax = freq_max, ymin = phase_min, ymax = phase_max, xscale = 'log', yscale = 'linear')
		plt.savefig(tf_file.replace('.txt', '_ratio_%dpercenterror_amplitude%d.png' % (int(round(100 * error_magnitude)), line_amplitude)))




