#!/usr/bin/env python3
#
# Copyright (C) 2015 Madeline Wade, Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os

from gstlal import pipeparts
import numpy
import time
import numbers

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst
gi.require_version('GstController', '1.0')
from gi.repository import GstController
Gst.init(None)

from gstlalcalibration import FIRtools as fir

#
# Utilities
#

def GstValueArraySet(properties, gst_value_array_list):
	# Convert all properties in gst_value_array_list to a Gst.ValueArray
	for va_name in [value for value in gst_value_array_list if value in properties]:
		va = properties[va_name]
		try:
			# Is it a matrix?
			mat = []
			for i in range(len(va)):
				row = []
				for j in range(len(va[i])):
					row.append(float(va[i][j]))
				mat.append(Gst.ValueArray(row))
			properties[va_name] = Gst.ValueArray(mat)
		except:
			try:
				# Is it a vector?
				vec = []
				for i in range(len(va)):
					vec.append(float(va[i]))
				properties[va_name] = Gst.ValueArray(vec)
			except:
				if isinstance(va, numbers.Number):
					vec = [va]
					properties[va_name] = Gst.ValueArray(vec)
				else:
					properties[va_name] = Gst.ValueArray([])

	return properties

def get_epics(filters):
	# Try to get all EPICS from a filters file
	# Older filters have numbered EPICS based on the O2 numbering scheme.
	# Newer filters (ER15 and later) have variable names for each EPICS value.
	if not ("ATinvRratio_fT_re" in filters or "EP1_real" in filters):
		print("WARNING: Cannot compute time-dependent correction factors, as the needed EPICS are not contained in the specified filters file.")
	ATinvRratio_fT_re = float(filters["ATinvRratio_fT_re"]) if "ATinvRratio_fT_re" in filters else float(filters["EP1_real"]) if "EP1_real" in filters else 1.0
	ATinvRratio_fT_im = float(filters["ATinvRratio_fT_im"]) if "ATinvRratio_fT_im" in filters else float(filters["EP1_imag"]) if "EP1_imag" in filters else 1.0
	C0_f2_re = float(filters["C0_f2_re"]) if "C0_f2_re" in filters else float(filters["EP6_real"]) if "EP6_real" in filters else 1.0
	C0_f2_im = float(filters["C0_f2_im"]) if "C0_f2_im" in filters else float(filters["EP6_imag"]) if "EP6_imag" in filters else 1.0
	D_f2_re = float(filters["D_f2_re"]) if "D_f2_re" in filters else float(filters["EP7_real"]) if "EP7_real" in filters else 1.0
	D_f2_im = float(filters["D_f2_im"]) if "D_f2_im" in filters else float(filters["EP7_imag"]) if "EP7_imag" in filters else 1.0
	AT_f2_re = float(filters["AT_f2_re"]) if "AT_f2_re" in filters else float(filters["EP8_real"]) if "EP8_real" in filters else 1.0
	AT_f2_im = float(filters["AT_f2_im"]) if "AT_f2_im" in filters else float(filters["EP8_imag"]) if "EP8_imag" in filters else 1.0

	if not ("C0_f1_re" in filters or "EP11_real" in filters):
		print("INFO: Cannot compute SRC spring frequency or Q, as the needed EPICS are not contained in the specified filters file.")
	C0_f1_re = float(filters["C0_f1_re"]) if "C0_f1_re" in filters else float(filters["EP11_real"]) if "EP11_real" in filters else 1.0
	C0_f1_im = float(filters["C0_f1_im"]) if "C0_f1_im" in filters else float(filters["EP11_imag"]) if "EP11_imag" in filters else 1.0
	D_f1_re = float(filters["D_f1_re"]) if "D_f1_re" in filters else float(filters["EP12_real"]) if "EP12_real" in filters else 1.0
	D_f1_im = float(filters["D_f1_im"]) if "D_f1_im" in filters else float(filters["EP12_imag"]) if "EP12_imag" in filters else 1.0
	AT_f1_re = float(filters["AT_f1_re"]) if "AT_f1_re" in filters else float(filters["EP13_real"]) if "EP13_real" in filters else 1.0
	AT_f1_im = float(filters["AT_f1_im"]) if "AT_f1_im" in filters else float(filters["EP13_imag"]) if "EP13_imag" in filters else 1.0

	if not ("APinvRratio_fP_re" in filters or "EP15_real" in filters):
		print("WARNING: Cannot compute kappa_PUM or kappa_UIM, as the needed EPICS are not contained in the specified filters file.")
	APinvRratio_fP_re = float(filters["APinvRratio_fP_re"]) if "APinvRratio_fP_re" in filters else float(filters["EP15_real"]) if "EP15_real" in filters else 1.0
	APinvRratio_fP_im = float(filters["APinvRratio_fP_im"]) if "APinvRratio_fP_im" in filters else float(filters["EP15_imag"]) if "EP15_imag" in filters else 1.0
	AP_f2_re = float(filters["AP_f2_re"]) if "AP_f2_re" in filters else float(filters["EP18_real"]) if "EP18_real" in filters else 1.0
	AP_f2_im = float(filters["AP_f2_im"]) if "AP_f2_im" in filters else float(filters["EP18_imag"]) if "EP18_imag" in filters else 1.0
	AU_f2_re = float(filters["AU_f2_re"]) if "AU_f2_re" in filters else float(filters["EP19_real"]) if "EP19_real" in filters else 1.0
	AU_f2_im = float(filters["AU_f2_im"]) if "AU_f2_im" in filters else float(filters["EP19_imag"]) if "EP19_imag" in filters else 1.0
	AP_f1_re = float(filters["AP_f1_re"]) if "AP_f1_re" in filters else float(filters["EP20_real"]) if "EP20_real" in filters else 1.0
	AP_f1_im = float(filters["AP_f1_im"]) if "AP_f1_im" in filters else float(filters["EP20_imag"]) if "EP20_imag" in filters else 1.0
	AU_f1_re = float(filters["AU_f1_re"]) if "AU_f1_re" in filters else float(filters["EP21_real"]) if "EP21_real" in filters else 1.0
	AU_f1_im = float(filters["AU_f1_im"]) if "AU_f1_im" in filters else float(filters["EP21_imag"]) if "EP21_imag" in filters else 1.0
	AUinvRratio_fU_re = float(filters["AUinvRratio_fU_re"]) if "AUinvRratio_fU_re" in filters else float(filters["EP22_real"]) if "EP22_real" in filters else 1.0
	AUinvRratio_fU_im = float(filters["AUinvRratio_fU_im"]) if "AUinvRratio_fU_im" in filters else float(filters["EP22_imag"]) if "EP22_imag" in filters else 1.0

	if not ("R_f1_re" in filters or "EP43_real" in filters):
		print("INFO: Cannot use ComputeExactKappas, as the needed EPICS are not in the filters file.")
	C0DAT_f1_re = float(filters["C0DAT_f1_re"]) if "C0DAT_f1_re" in filters else float(filters["EP25_real"]) if "EP25_real" in filters else 1.0
	C0DAT_f1_im = float(filters["C0DAT_f1_im"]) if "C0DAT_f1_im" in filters else float(filters["EP25_imag"]) if "EP25_imag" in filters else 1.0
	C0DAP_f1_re = float(filters["C0DAP_f1_re"]) if "C0DAP_f1_re" in filters else float(filters["EP26_real"]) if "EP26_real" in filters else 1.0
	C0DAP_f1_im = float(filters["C0DAP_f1_im"]) if "C0DAP_f1_im" in filters else float(filters["EP26_imag"]) if "EP26_imag" in filters else 1.0
	C0DAU_f1_re = float(filters["C0DAU_f1_re"]) if "C0DAU_f1_re" in filters else float(filters["EP27_real"]) if "EP27_real" in filters else 1.0
	C0DAU_f1_im = float(filters["C0DAU_f1_im"]) if "C0DAU_f1_im" in filters else float(filters["EP27_imag"]) if "EP27_imag" in filters else 1.0
	C0DAT_f2_re = float(filters["C0DAT_f2_re"]) if "C0DAT_f2_re" in filters else float(filters["EP28_real"]) if "EP28_real" in filters else 1.0
	C0DAT_f2_im = float(filters["C0DAT_f2_im"]) if "C0DAT_f2_im" in filters else float(filters["EP28_imag"]) if "EP28_imag" in filters else 1.0
	C0DAP_f2_re = float(filters["C0DAP_f2_re"]) if "C0DAP_f2_re" in filters else float(filters["EP29_real"]) if "EP29_real" in filters else 1.0
	C0DAP_f2_im = float(filters["C0DAP_f2_im"]) if "C0DAP_f2_im" in filters else float(filters["EP29_imag"]) if "EP29_imag" in filters else 1.0
	C0DAU_f2_re = float(filters["C0DAU_f2_re"]) if "C0DAU_f2_re" in filters else float(filters["EP30_real"]) if "EP30_real" in filters else 1.0
	C0DAU_f2_im = float(filters["C0DAU_f2_im"]) if "C0DAU_f2_im" in filters else float(filters["EP30_imag"]) if "EP30_imag" in filters else 1.0
	C0AT0_fT_re = float(filters["C0AT0_fT_re"]) if "C0AT0_fT_re" in filters else float(filters["EP31_real"]) if "EP31_real" in filters else 1.0
	C0AT0_fT_im = float(filters["C0AT0_fT_im"]) if "C0AT0_fT_im" in filters else float(filters["EP31_imag"]) if "EP31_imag" in filters else 1.0
	C0DAT_fT_re = float(filters["C0DAT_fT_re"]) if "C0DAT_fT_re" in filters else float(filters["EP32_real"]) if "EP32_real" in filters else 1.0
	C0DAT_fT_im = float(filters["C0DAT_fT_im"]) if "C0DAT_fT_im" in filters else float(filters["EP32_imag"]) if "EP32_imag" in filters else 1.0
	C0DAP_fT_re = float(filters["C0DAP_fT_re"]) if "C0DAP_fT_re" in filters else float(filters["EP33_real"]) if "EP33_real" in filters else 1.0
	C0DAP_fT_im = float(filters["C0DAP_fT_im"]) if "C0DAP_fT_im" in filters else float(filters["EP33_imag"]) if "EP33_imag" in filters else 1.0
	C0DAU_fT_re = float(filters["C0DAU_fT_re"]) if "C0DAU_fT_re" in filters else float(filters["EP34_real"]) if "EP34_real" in filters else 1.0
	C0DAU_fT_im = float(filters["C0DAU_fT_im"]) if "C0DAU_fT_im" in filters else float(filters["EP34_imag"]) if "EP34_imag" in filters else 1.0
	C0AP0_fP_re = float(filters["C0AP0_fP_re"]) if "C0AP0_fP_re" in filters else float(filters["EP35_real"]) if "EP35_real" in filters else 1.0
	C0AP0_fP_im = float(filters["C0AP0_fP_im"]) if "C0AP0_fP_im" in filters else float(filters["EP35_imag"]) if "EP35_imag" in filters else 1.0
	C0DAT_fP_re = float(filters["C0DAT_fP_re"]) if "C0DAT_fP_re" in filters else float(filters["EP36_real"]) if "EP36_real" in filters else 1.0
	C0DAT_fP_im = float(filters["C0DAT_fP_im"]) if "C0DAT_fP_im" in filters else float(filters["EP36_imag"]) if "EP36_imag" in filters else 1.0
	C0DAP_fP_re = float(filters["C0DAP_fP_re"]) if "C0DAP_fP_re" in filters else float(filters["EP37_real"]) if "EP37_real" in filters else 1.0
	C0DAP_fP_im = float(filters["C0DAP_fP_im"]) if "C0DAP_fP_im" in filters else float(filters["EP37_imag"]) if "EP37_imag" in filters else 1.0
	C0DAU_fP_re = float(filters["C0DAU_fP_re"]) if "C0DAU_fP_re" in filters else float(filters["EP38_real"]) if "EP38_real" in filters else 1.0
	C0DAU_fP_im = float(filters["C0DAU_fP_im"]) if "C0DAU_fP_im" in filters else float(filters["EP38_imag"]) if "EP38_imag" in filters else 1.0
	C0AU0_fU_re = float(filters["C0AU0_fU_re"]) if "C0AU0_fU_re" in filters else float(filters["EP39_real"]) if "EP39_real" in filters else 1.0
	C0AU0_fU_im = float(filters["C0AU0_fU_im"]) if "C0AU0_fU_im" in filters else float(filters["EP39_imag"]) if "EP39_imag" in filters else 1.0
	C0DAT_fU_re = float(filters["C0DAT_fU_re"]) if "C0DAT_fU_re" in filters else float(filters["EP40_real"]) if "EP40_real" in filters else 1.0
	C0DAT_fU_im = float(filters["C0DAT_fU_im"]) if "C0DAT_fU_im" in filters else float(filters["EP40_imag"]) if "EP40_imag" in filters else 1.0
	C0DAP_fU_re = float(filters["C0DAP_fU_re"]) if "C0DAP_fU_re" in filters else float(filters["EP41_real"]) if "EP41_real" in filters else 1.0
	C0DAP_fU_im = float(filters["C0DAP_fU_im"]) if "C0DAP_fU_im" in filters else float(filters["EP41_imag"]) if "EP41_imag" in filters else 1.0
	C0DAU_fU_re = float(filters["C0DAU_fU_re"]) if "C0DAU_fU_re" in filters else float(filters["EP42_real"]) if "EP42_real" in filters else 1.0
	C0DAU_fU_im = float(filters["C0DAU_fU_im"]) if "C0DAU_fU_im" in filters else float(filters["EP42_imag"]) if "EP42_imag" in filters else 1.0
	R_f1_re = float(filters["R_f1_re"]) if "R_f1_re" in filters else float(filters["EP43_real"]) if "EP43_real" in filters else 1.0
	R_f1_im = float(filters["R_f1_im"]) if "R_f1_im" in filters else float(filters["EP43_imag"]) if "EP43_imag" in filters else 1.0
	R_f2_re = float(filters["R_f2_re"]) if "R_f2_re" in filters else float(filters["EP44_real"]) if "EP44_real" in filters else 1.0
	R_f2_im = float(filters["R_f2_im"]) if "R_f2_im" in filters else float(filters["EP44_imag"]) if "EP44_imag" in filters else 1.0
	RAT0inv_fT_re = float(filters["RAT0inv_fT_re"]) if "RAT0inv_fT_re" in filters else float(filters["EP45_real"]) if "EP45_real" in filters else 1.0
	RAT0inv_fT_im = float(filters["RAT0inv_fT_im"]) if "RAT0inv_fT_im" in filters else float(filters["EP45_imag"]) if "EP45_imag" in filters else 1.0
	RAP0inv_fP_re = float(filters["RAP0inv_fP_re"]) if "RAP0inv_fP_re" in filters else float(filters["EP46_real"]) if "EP46_real" in filters else 1.0
	RAP0inv_fP_im = float(filters["RAP0inv_fP_im"]) if "RAP0inv_fP_im" in filters else float(filters["EP46_imag"]) if "EP46_imag" in filters else 1.0
	RAU0inv_fU_re = float(filters["RAU0inv_fU_re"]) if "RAU0inv_fU_re" in filters else float(filters["EP47_real"]) if "EP47_real" in filters else 1.0
	RAU0inv_fU_im = float(filters["RAU0inv_fU_im"]) if "RAU0inv_fU_im" in filters else float(filters["EP47_imag"]) if "EP47_imag" in filters else 1.0
	AT0_fT_re = float(filters["AT0_fT_re"]) if "AT0_fT_re" in filters else float(filters["EP10_real"]) if "EP10_real" in filters else 1.0
	AT0_fT_im = float(filters["AT0_fT_im"]) if "AT0_fT_im" in filters else float(filters["EP10_imag"]) if "EP10_imag" in filters else 1.0
	AP0_fP_re = float(filters["AP0_fP_re"]) if "AP0_fP_re" in filters else float(filters["EP23_real"]) if "EP23_real" in filters else 1.0
	AP0_fP_im = float(filters["AP0_fP_im"]) if "AP0_fP_im" in filters else float(filters["EP23_imag"]) if "EP23_imag" in filters else 1.0
	AU0_fU_re = float(filters["AU0_fU_re"]) if "AU0_fU_re" in filters else float(filters["EP24_real"]) if "EP24_real" in filters else 1.0
	AU0_fU_im = float(filters["AU0_fU_im"]) if "AU0_fU_im" in filters else float(filters["EP24_imag"]) if "EP24_imag" in filters else 1.0

	return ATinvRratio_fT_re, ATinvRratio_fT_im, APinvRratio_fP_re, APinvRratio_fP_im, AUinvRratio_fU_re, AUinvRratio_fU_im, C0_f2_re, C0_f2_im, D_f2_re, D_f2_im, AT_f2_re, AT_f2_im, AP_f2_re, AP_f2_im, AU_f2_re, AU_f2_im, C0_f1_re, C0_f1_im, D_f1_re, D_f1_im, AT_f1_re, AT_f1_im, AP_f1_re, AP_f1_im, AU_f1_re, AU_f1_im, C0DAT_f1_re, C0DAT_f1_im, C0DAP_f1_re, C0DAP_f1_im, C0DAU_f1_re, C0DAU_f1_im, C0DAT_f2_re, C0DAT_f2_im, C0DAP_f2_re, C0DAP_f2_im, C0DAU_f2_re, C0DAU_f2_im, C0AT0_fT_re, C0AT0_fT_im, C0DAT_fT_re, C0DAT_fT_im, C0DAP_fT_re, C0DAP_fT_im, C0DAU_fT_re, C0DAU_fT_im, C0AP0_fP_re, C0AP0_fP_im, C0DAT_fP_re, C0DAT_fP_im, C0DAP_fP_re, C0DAP_fP_im, C0DAU_fP_re, C0DAU_fP_im, C0AU0_fU_re, C0AU0_fU_im, C0DAT_fU_re, C0DAT_fU_im, C0DAP_fU_re, C0DAP_fU_im, C0DAU_fU_re, C0DAU_fU_im, R_f1_re, R_f1_im, R_f2_re, R_f2_im, RAT0inv_fT_re, RAT0inv_fT_im, RAP0inv_fP_re, RAP0inv_fP_im, RAU0inv_fU_re, RAU0inv_fU_im, AT0_fT_re, AT0_fT_im, AP0_fP_re, AP0_fP_im, AU0_fU_re, AU0_fU_im

#
# Shortcut functions for common element combos/properties
#

def mkqueue(pipeline, head, length = 0, min_length = 0):
	if length < 0:
		return head
	else:
		return pipeparts.mkqueue(pipeline, head, max_size_time = int(1000000000 * length), max_size_buffers = 0, max_size_bytes = 0, min_threshold_time = int(1000000000 * min_length))

def mkcomplexqueue(pipeline, head, length = 0, min_length = 0):
	head = pipeparts.mktogglecomplex(pipeline, head)
	head = mkqueue(pipeline, head, length = length, min_length = min_length)
	head = pipeparts.mktogglecomplex(pipeline, head)
	return head

def mkcapsfiltersetter(pipeline, head, caps, **properties):
	# Make a capsfilter followed by a capssetter
	head = pipeparts.mkcapsfilter(pipeline, head, caps)
	#head = pipeparts.mkcapssetter(pipeline, head, caps, replace = True, **properties)
	return head

def mkinsertgap(pipeline, head, **properties):
	properties = GstValueArraySet(properties, ["bad_data_intervals"])
	return pipeparts.mkgeneric(pipeline, head, "lal_insertgap", **properties)

def mkmatmix(pipeline, head, **properties):
	properties = GstValueArraySet(properties, ["matrix"])
	return pipeparts.mkgeneric(pipeline, head, "lal_matmix", **properties)

def mkresample(pipeline, head, quality, zero_latency, caps, window = 0, frequency_resolution = 0.0, f_cut = 0.0):
	if type(caps) is int:
		caps = "audio/x-raw,rate=%d,channel-mask=(bitmask)0x0" % caps
	head = pipeparts.mkgeneric(pipeline, head, "lal_resample", quality = quality, zero_latency = zero_latency, window = window, frequency_resolution = frequency_resolution, f_cut = f_cut)
	head = pipeparts.mkcapsfilter(pipeline, head, caps)
	return head

def mkcomplexfirbank(pipeline, head, **properties):
	properties = GstValueArraySet(properties, ["fir_matrix"])
	return pipeparts.mkgeneric(pipeline, head, "lal_complexfirbank", **properties)

def mktdepfirfilt(pipeline, head, **properties):
	properties = GstValueArraySet(properties, ["kernel"])
	return pipeparts.mkgeneric(pipeline, head, "lal_tdepfirfilt", **properties)

def mkfccupdate(pipeline, src, **properties):
	properties = GstValueArraySet(properties, ["fir_matrix"])
	return pipeparts.mkgeneric(pipeline, src, "lal_fcc_update", **properties)

def mkcalibcorr(pipeline, src, **properties):
	properties = GstValueArraySet(properties, ["D", "cinv_freqs", "cres_model", "measured_response", "measured_response_uncertainty", "tst_freqs", "Ftst_model", "tst_model", "tst_tf", "tst_tf_uncertainty", "pum_freqs", "Fpum_model", "pum_model", "pum_tf", "pum_tf_uncertainty", "uim_freqs", "Fuim_model", "uim_model", "uim_tf", "uim_tf_uncertainty"])
	return pipeparts.mkgeneric(pipeline, src, "lal_calibcorr", **properties)

def mktransferfunction(pipeline, src, **properties):
	if "fft_window_type" in properties:
		win = properties.pop("fft_window_type")
		if win in ['hann', 'Hann', 'HANN', 'hanning', 'Hanning', 'HANNING', 4]:
			win = 4
		elif win in ['blackman', 'Blackman', 'BLACKMAN', 3]:
			win = 3
		elif win in ['DC', 'dolph_chebyshev', 'DolphChebyshev', 'DOLPH_CHEBYSHEV', 2]:
			win = 2
		elif win in ['kaiser', 'Kaiser', 'KAISER', 1]:
			win = 1
		elif win in ['dpss', 'DPSS', 'Slepian', 'slepian', 'SLEPIAN', 0]:
			win = 0
		else:
			raise ValueError("Unknown window function %s" % win)
		properties["fft_window_type"] = win
	if "fir_window_type" in properties:
		win = properties.pop("fir_window_type")
		if win in ['hann', 'Hann', 'HANN', 'hanning', 'Hanning', 'HANNING', 4]:
			win = 4
		elif win in ['blackman', 'Blackman', 'BLACKMAN', 3]:
			win = 3
		elif win in ['DC', 'dolph_chebyshev', 'DolphChebyshev', 'DOLPH_CHEBYSHEV', 2]:
			win = 2
		elif win in ['kaiser', 'Kaiser', 'KAISER', 1]:
			win = 1
		elif win in ['dpss', 'DPSS', 'Slepian', 'slepian', 'SLEPIAN', 0]:
			win = 0
		else:
			raise ValueError("Unknown window function %s" % win)
		properties["fir_window_type"] = win

	properties = GstValueArraySet(properties, ["notch_frequencies"])

	return pipeparts.mkgeneric(pipeline, src, "lal_transferfunction", **properties)

def mkadaptivefirfilt(pipeline, src, **properties):
	if "window_type" in properties:
		win = properties.pop("window_type")
		if win in [None, 5]:
			win = 5
		elif win in ['hann', 'Hann', 'HANN', 'hanning', 'Hanning', 'HANNING', 4]:
			win = 4
		elif win in ['blackman', 'Blackman', 'BLACKMAN', 3]:
			win = 3
		elif win in ['DC', 'dolph_chebyshev', 'DolphChebyshev', 'DOLPH_CHEBYSHEV', 2]:
			win = 2
		elif win in ['kaiser', 'Kaiser', 'KAISER', 1]:
			win = 1
		elif win in ['dpss', 'DPSS', 'Slepian', 'slepian', 'SLEPIAN', 0]:
			win = 0
		else:
			raise ValueError("Unknown window function %s" % win)
		properties["window_type"] = win
	if "average_samples" in properties:
		navg = properties.pop("average_samples")
		navg = navg if navg > 0 else 1
		properties["average_samples"] = navg

	properties = GstValueArraySet(properties, ["static_filter", "static_model", "static_poles", "static_zeros"])

	return pipeparts.mkgeneric(pipeline, src, "lal_adaptivefirfilt", **properties)

def mkpow(pipeline, src, **properties):
	return pipeparts.mkgeneric(pipeline, src, "cpow", **properties)

def mkmultiplier(pipeline, srcs, sync = True, queue_length = [0]):
	elem = pipeparts.mkgeneric(pipeline, None, "lal_adder", sync=sync, mix_mode="product")
	if srcs is not None:
		for i in range(len(srcs)):
			mkqueue(pipeline, srcs[i], length = queue_length[min(i, len(queue_length) - 1)]).link(elem)
	return elem

def mkadder(pipeline, srcs, sync = True, queue_length = [0]):
	elem = pipeparts.mkgeneric(pipeline, None, "lal_adder", sync=sync)
	if srcs is not None:
		for i in range(len(srcs)):
			mkqueue(pipeline, srcs[i], length = queue_length[min(i, len(queue_length) - 1)]).link(elem)
	return elem

def mkgate(pipeline, src, control, threshold, queue_length = 0, **properties):
	elem = pipeparts.mkgate(pipeline, mkqueue(pipeline, src, length = queue_length), control = mkqueue(pipeline, control, length = queue_length), threshold = threshold, **properties)
	return elem

def mkinterleave(pipeline, srcs, complex_data = False, queue_length = [0]):
	complex_factor = 1 + int(complex_data)
	num_srcs = complex_factor * len(srcs)
	i = 0
	mixed_srcs = []
	for src in srcs:
		matrix = [numpy.zeros(num_srcs)]
		matrix[0][i] = 1
		mixed_srcs.append(mkmatmix(pipeline, src, matrix = matrix))
		i += complex_factor
	elem = mkadder(pipeline, tuple(mixed_srcs), queue_length = queue_length)

	return elem

def mkdeinterleave(pipeline, src, num_channels, complex_data = False):
	complex_factor = 1 + int(complex_data)
	head = pipeparts.mktee(pipeline, src)
	streams = []
	for i in range(num_channels):
		matrix = numpy.zeros((num_channels, complex_factor))
		matrix[i][0] = 1.0
		streams.append(mkmatmix(pipeline, head, matrix = matrix))

	return tuple(streams)


#
# Write a pipeline graph function
#

def write_graph(demux, pipeline, name):
	pipeparts.write_dump_dot(pipeline, "%s.%s" % (name, "PLAYING"), verbose = True)

#
# Common element combo functions
#

def hook_up(pipeline, demux, channel_name, instrument, buffer_length, element_name_suffix = "", wait_time = 0, start_time = 0):
	if channel_name.endswith("UNCERTAINTY"):
		head = mkinsertgap(pipeline, None, bad_data_intervals = [-1e35, -1e-35, 1e-35, 1e35], insert_gap = False, remove_gap = True, fill_discont = True, block_duration = int(1000000000 * buffer_length), replace_value = 1, name = "insertgap_%s%s" % (channel_name, element_name_suffix), wait_time = int(1000000000 * wait_time), start_time = int(1000000000 * start_time))
	else:
		head = mkinsertgap(pipeline, None, bad_data_intervals = [-1e35, -1e-35, 1e-35, 1e35], insert_gap = False, remove_gap = True, fill_discont = True, block_duration = int(1000000000 * buffer_length), replace_value = 0, name = "insertgap_%s%s" % (channel_name, element_name_suffix), wait_time = int(1000000000 * wait_time), start_time = int(1000000000 * start_time))
	pipeparts.src_deferred_link(demux, "%s:%s" % (instrument, channel_name), head.get_static_pad("sink"))

	return head

def caps_and_progress(pipeline, head, caps, progress_name):
	head = pipeparts.mkgeneric(pipeline, head, "lal_typecast")
	head = pipeparts.mkcapsfilter(pipeline, head, caps)
	head = pipeparts.mkprogressreport(pipeline, head, "progress_src_%s" % progress_name)

	return head


#
# Function to make a list of heads to pass to, i.e. the multiplier or adder
#

def list_srcs(pipeline, *args):
	out = []
	for src in args:
		out.append(src)
	return tuple(out)

#
# Various filtering functions
#

def demodulate(pipeline, head, freq, td, rate, filter_time, filter_latency, prefactor_real = 1.0, prefactor_imag = 0.0, freq_update = None):
	# demodulate input at a given frequency freq

	head = pipeparts.mkgeneric(pipeline, head, "lal_demodulate", line_frequency = freq, prefactor_real = prefactor_real, prefactor_imag = prefactor_imag)
	if type(freq_update) is list:
		freq_update[0].connect("notify::timestamped-average", update_timestamped_property, head, "timestamped_average", "line_frequency", 1)
		freq_update[1].connect("notify::timestamped-average", update_timestamped_property, head, "timestamped_average", "prefactor_real", 1)
		freq_update[2].connect("notify::timestamped-average", update_timestamped_property, head, "timestamped_average", "prefactor_imag", 1)
	elif freq_update is not None:
		freq_update.connect("notify::timestamped-average", update_timestamped_property, head, "timestamped_average", "line_frequency", 1)
	head = mkresample(pipeline, head, 4, filter_latency == 0.0, rate)
	if filter_latency != 0:
		# Remove the first several seconds of output, which depend on start time
		head = pipeparts.mkgeneric(pipeline, head, "lal_insertgap", chop_length = 7000000000)
	head = lowpass(pipeline, head, rate, length = filter_time, fcut = 0, filter_latency = filter_latency, td = td)

	return head

def line_coherence(pipeline, signal1, signal2, freq, num_median = 2048, num_avg = 160, rate = 16, demodulated = False, td = True, filter_time = 20, filter_latency = 0):
	# Find the coherence between two signals at a particular frequency

	if not demodulated:
		# Then demodulate the lines in both signals
		signal1 = demodulate(pipeline, signal1, freq, td, rate, filter_time, filter_latency)
		signal2 = demodulate(pipeline, signal2, freq, td, rate, filter_time, filter_latency)
	else:
		# Make sure the demodulated signals are at the desired sample rate
		signal1 = mkresample(pipeline, signal1, 4, filter_latency == 0, rate)
		signal2 = mkresample(pipeline, signal2, 4, filter_latency == 0, rate)

	signal1 = pipeparts.mktee(pipeline, signal1)
	signal2 = pipeparts.mktee(pipeline, signal2)

	csd = mkmultiplier(pipeline, [pipeparts.mkgeneric(pipeline, signal1, "conj"), signal2])
	csd = pipeparts.mkgeneric(pipeline, csd, "lal_smoothkappas", default_kappa_re = 1.0, array_size = num_median, avg_array_size = num_avg, filter_latency = filter_latency)
	csd = mkpow(pipeline, pipeparts.mkgeneric(pipeline, csd, "cabs"), exponent = 2)
	psd1 = mkpow(pipeline, pipeparts.mkgeneric(pipeline, signal1, "cabs"), exponent = 2)
	psd1 = pipeparts.mkgeneric(pipeline, psd1, "lal_smoothkappas", default_kappa_re = 1.0, array_size = num_median, avg_array_size = num_avg, filter_latency = filter_latency)
	psd1_inv = complex_inverse(pipeline, psd1)
	psd2 = mkpow(pipeline, pipeparts.mkgeneric(pipeline, signal2, "cabs"), exponent = 2)
	psd2 = pipeparts.mkgeneric(pipeline, psd2, "lal_smoothkappas", default_kappa_re = 1.0, array_size = num_median, avg_array_size = num_avg, filter_latency = filter_latency)
	psd2_inv = complex_inverse(pipeline, psd2)

	return mkmultiplier(pipeline, [csd, psd1_inv, psd2_inv])

def remove_harmonics(pipeline, signal, f0, num_harmonics, f0_var, filter_latency, compute_rate = 16, rate_out = 16384):
	# remove any line(s) from a spectrum. filter length for demodulation (given in seconds) is adjustable
	# function argument caps must be complex caps

	filter_param = 0.0625
	head = pipeparts.mktee(pipeline, head)
	elem = pipeparts.mkgeneric(pipeline, None, "lal_adder", sync = True)
	mkqueue(pipeline, head).link(elem)
	for i in range(1, num_harmonics + 1):
		line = pipeparts.mkgeneric(pipeline, head, "lal_demodulate", line_frequency = i * f0)
		line = mkresample(pipeline, line, 4, filter_latency == 0, compute_rate)
		line_in_witness = lowpass(pipeline, line_in_witness, compute_rate, length = filter_param / (f0_var * i), fcut = 0, filter_latency = filter_latency)
		line = mkresample(pipeline, line, 3, filter_latency == 0.0, rate_out)
		line = pipeparts.mkgeneric(pipeline, line, "lal_demodulate", line_frequency = -1.0 * i * f0, prefactor_real = -2.0)
		line = pipeparts.mkgeneric(pipeline, line, "creal")
		mkqueue(pipeline, line).link(elem)

	return elem

def remove_lines_with_witnesses(pipeline, signal, witnesses, freqs, freq_vars, freq_channels, amp_channels, filter_latency = 0, compute_rate = 16, rate_out = 16384, num_median = 2048, num_avg = 160, noisesub_gate_bit = None, filter_length = 20, queue_length = 0.0):
	# remove line(s) from a spectrum. filter length for demodulation (given in seconds) is adjustable
	# function argument caps must be complex caps

	# Re-format inputs if necessary
	if type(witnesses) is not list and type(witnesses) is not tuple and type(witnesses) is not numpy.ndarray:
		print("remove_lines_with_witnesses(): argument 3 should be type list.  Converting %s to list" % type(witnesses))
		witnesses = [[witnesses]]
	if type(freqs) is not list and type(freqs) is not tuple and type(freqs) is not numpy.ndarray:
		print("remove_lines_with_witnesses(): argument 4 should be type list.  Converting %s to list" % type(freqs))
		freqs = [[freqs]]
	if type(freq_vars) is not list and type(freq_vars) is not tuple and type(freq_vars) is not numpy.ndarray:
		print("remove_lines_with_witnesses(): argument 5 should be type list.  Converting %s to list" % type(freq_vars))
		freq_vars = [freq_vars]
	for i in range(len(witnesses) - len(freqs)):
		print("remove_lines_with_witnesses(): Warning: not enough elements in argument 4")
		freqs.append(freqs[-1])
	for i in range(len(witnesses) - len(freq_vars)):
		print("remove_lines_with_witnesses(): Warning: not enough elements in argument 5")
		freq_vars.append(freq_vars[-1])
	if len(freqs) > len(witnesses):
		print("remove_lines_with_witnesses(): Warning: too many elements in argument 4")
		freqs = freqs[:len(witnesses)]
	if len(freq_vars) > len(witnesses):
		print("remove_lines_with_witnesses(): Warning: too many elements in argument 5")
		freq_vars = freq_vars[:len(witnesses)]
	for i in range(len(witnesses)):
		if type(witnesses[i]) is not list and type(witnesses[i]) is not tuple and type(witnesses[i]) is not numpy.ndarray:
			print("remove_lines_with_witnesses(): argument 3 should be list of lists.  Converting %s to list" % type(witnesses[i]))
			witnesses[i] = [witnesses[i]]
		if type(freqs[i]) is not list and type(freqs[i]) is not tuple and type(freqs[i]) is not numpy.ndarray:
			print("remove_lines_with_witnesses(): argument 4 should be list of lists.  Converting %s to list" % type(freqs[i]))
			freqs[i] = [freqs[i]]

	filter_param = 0.0625
	downsample_quality = 4
	upsample_quality = 4
	resample_shift = 16.0 + 16.5
	zero_latency = filter_latency == 0.0

	for i in range(len(witnesses)):
		for j in range(len(witnesses[i])):
			witnesses[i][j] = pipeparts.mktee(pipeline, witnesses[i][j])
	signal = pipeparts.mktee(pipeline, signal)
	signal_minus_lines = [signal]

	for m in range(len(witnesses)):
		# If freqs[m][0] strays from its nominal value and there is a timestamp shift in the signal
		# (e.g., to achieve zero latency), we need to correct the phase in the reconstructed
		# signal. To do this, we measure the frequency in the witness and find the beat
		# frequency between that and the nominal frequency freqs[m][0].
		if filter_latency != 0.5 and freq_vars[m]:
			# The low-pass and resampling filters are not centered in time
			f0_measured = pipeparts.mkgeneric(pipeline, witnesses[m][0], "lal_trackfrequency", num_halfcycles = int(round((filter_param / freq_vars[m] / 2 + resample_shift / compute_rate) * freqs[m][0])))
			f0_measured = mkresample(pipeline, f0_measured, 3, zero_latency, compute_rate)
			f0_measured = pipeparts.mkgeneric(pipeline, f0_measured, "lal_smoothkappas", array_size = 1, avg_array_size = int(round((filter_param / freq_vars[m] / 2 * compute_rate + resample_shift) / 2)), default_kappa_re = 0, default_to_median = True, filter_latency = filter_latency)
			f0_beat_frequency = pipeparts.mkgeneric(pipeline, f0_measured, "lal_add_constant", value = -freqs[m][0])
			f0_beat_frequency = pipeparts.mktee(pipeline, f0_beat_frequency)

		for n in range(len(freqs[m])):
			# Length of low-pass filter
			filter_samples = int(filter_length * compute_rate) + (1 - int(filter_length * compute_rate) % 2)
			filter_length = (filter_samples + 0.1) / compute_rate
			sample_shift = filter_samples // 2 - int((filter_samples - 1) * filter_latency + 0.5)
			# shift of timestamp relative to data
			time_shift = float(sample_shift) / compute_rate + zero_latency * resample_shift / compute_rate
			two_n_pi_delta_t = 2 * freqs[m][n] / freqs[m][0] * numpy.pi * time_shift

			# Only do this if we have to
			if filter_latency != 0.5 and freq_vars[m]:
				# Find phase shift due to timestamp shift for each harmonic
				phase_shift = mkmatmix(pipeline, f0_beat_frequency, matrix=[[0, two_n_pi_delta_t]])
				phase_shift = pipeparts.mktogglecomplex(pipeline, phase_shift)
				phase_factor = pipeparts.mkgeneric(pipeline, phase_shift, "cexp")
				phase_factor = pipeparts.mktee(pipeline, phase_factor)

			# Find amplitude and phase of line in signal
			line_in_signal = pipeparts.mkgeneric(pipeline, signal, "lal_demodulate", line_frequency = freqs[m][n])
			# Connect to line frequency updater if given
			if any(freq_channels):
				if freq_channels[m][n] is not None:
					if type(freq_channels[m][n]) is float:
						# It's a harmonic of the frequency in freq_channels[m][0]
						freq_channels[m][0].connect("notify::timestamped-average", update_timestamped_property, line_in_signal, "timestamped_average", "line_frequency", freq_channels[m][n])
					else:
						# The channel carries the correct frequency
						freq_channels[m][n].connect("notify::timestamped-average", update_timestamped_property, line_in_signal, "timestamped_average", "line_frequency", 1)
			line_in_signal = mkresample(pipeline, line_in_signal, downsample_quality, zero_latency, compute_rate)
			line_in_signal = lowpass(pipeline, line_in_signal, compute_rate, length = filter_length, fcut = 0, filter_latency = filter_latency)
			line_in_signal = pipeparts.mktee(pipeline, line_in_signal)

			# Make ones for use in matrix equation
			if m == 0 and n == 0:
				ones = pipeparts.mktee(pipeline, mkpow(pipeline, line_in_signal, exponent = 0.0))
			if any(amp_channels):
				if amp_channels[m][n] is not None:
					line_in_signal = mkgate(pipeline, line_in_signal, amp_channels[m][n], 1e-35, attack_length = -((1.0 - filter_latency) * filter_samples), name = "linesub_amp_gate_%d_%d" % (m, n), queue_length = queue_length)
					line_in_signal = pipeparts.mktee(pipeline, line_in_signal)

			line_in_witnesses = []
			tfs_at_f = [None] * len(witnesses[m]) * (len(witnesses[m]) + 1)
			for i in range(len(witnesses[m])):
				# Find amplitude and phase of each harmonic in each witness channel
				line_in_witness = pipeparts.mkgeneric(pipeline, witnesses[m][i], "lal_demodulate", line_frequency = freqs[m][n])
				# Connect to line frequency updater if given
				if any(freq_channels):
					if freq_channels[m][n] is not None:
						if type(freq_channels[m][n]) is float:
							# It's a harmonic of the frequency in freq_channels[m][0]
							freq_channels[m][0].connect("notify::timestamped-average", update_timestamped_property, line_in_witness, "timestamped_average", "line_frequency", freq_channels[m][n])
						else:
							# The channel carries the correct frequency
							freq_channels[m][n].connect("notify::timestamped-average", update_timestamped_property, line_in_witness, "timestamped_average", "line_frequency", 1)
				line_in_witness = mkresample(pipeline, line_in_witness, downsample_quality, zero_latency, compute_rate)
				line_in_witness = lowpass(pipeline, line_in_witness, compute_rate, length = filter_length, fcut = 0, filter_latency = filter_latency)
				line_in_witness = pipeparts.mktee(pipeline, line_in_witness)
				line_in_witnesses.append(line_in_witness)

				# Find transfer function between witness channel and signal at this frequency
				# The division below causes the warning "GStreamer-Audio-CRITICAL...:
				# gst_audio_format_info_fill_silence: assertion 'dest != NULL' failed"
				# It appears to be harmless in this case, and occurs when both sink pads
				# receive zero-length buffers at the start of stream.
				tf_at_f = complex_division(pipeline, line_in_signal, line_in_witness)

				# To prevent oversubtraction, reduce the TF with line coherence
				#coherence = line_coherence(pipeline, line_in_signal, line_in_witness, freqs[m][n], num_median = num_median, num_avg = num_avg, rate = compute_rate, demodulated = True, filter_time = filter_length, filter_latency = filter_latency)
				#tf_at_f = mkmultiplier(pipeline, [tf_at_f, pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, coherence, matrix = [[1, 0]]))])

				# Remove worthless data from computation of transfer function if we can
				if noisesub_gate_bit is not None:
					tf_at_f = mkgate(pipeline, tf_at_f, noisesub_gate_bit, 1, attack_length = -((1.0 - filter_latency) * filter_samples), name = "linesub_gate_%d_%d_%d" % (m, n, i), queue_length = queue_length)
				tfs_at_f[i] = pipeparts.mkgeneric(pipeline, tf_at_f, "lal_smoothkappas", default_kappa_re = 0.0, default_kappa_im = 0.0, array_size = num_median, avg_array_size = num_avg, default_to_median = True, filter_latency = filter_latency)
				#tfs_at_f[i] = mkmultiplier(pipeline, [tfs_at_f[i], pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, coherence, matrix = [[1, 0]]))])
				tfs_at_f[(i + 1) * len(witnesses[m]) + i] = ones

			for i in range(len(witnesses[m])):
				for j in range(len(witnesses[m])):
					if(i != j):
						# Find transfer function between 2 witness channels at this frequency
						tf_at_f = complex_division(pipeline, line_in_witnesses[j], line_in_witnesses[i])

						# Remove worthless data from computation of transfer function if we can
						if noisesub_gate_bit is not None:
							tf_at_f = mkgate(pipeline, tf_at_f, noisesub_gate_bit, 1, attack_length = -((1.0 - filter_latency) * filter_samples), name = "linesub_gate_%d_%d_%d_%d" % (m, n, i, j), queue_length = queue_length)
						tfs_at_f[(i + 1) * len(witnesses[m]) + j] = pipeparts.mkgeneric(pipeline, tf_at_f, "lal_smoothkappas", default_kappa_re = 0.0, default_kappa_im = 0.0, array_size = num_median, avg_array_size = num_avg, default_to_median = True, filter_latency = filter_latency)

			tfs_at_f = mkinterleave(pipeline, tfs_at_f, complex_data = True)
			tfs_at_f = pipeparts.mkgeneric(pipeline, tfs_at_f, "lal_matrixsolver")
			tfs_at_f = mkdeinterleave(pipeline, tfs_at_f, len(witnesses[m]), complex_data = True)

			for i in range(len(witnesses[m])):
				# Use gated, averaged transfer function to reconstruct the sinusoid as it appears in the signal from the witness channel
				if filter_latency == 0.5 or not freq_vars[m]:
					reconstructed_line_in_signal = mkmultiplier(pipeline, list_srcs(pipeline, tfs_at_f[i], line_in_witnesses[i]), queue_length = [queue_length])
				else:
					reconstructed_line_in_signal = mkmultiplier(pipeline, list_srcs(pipeline, tfs_at_f[i], line_in_witnesses[i], phase_factor), queue_length = [queue_length])
				reconstructed_line_in_signal = mkresample(pipeline, reconstructed_line_in_signal, upsample_quality, zero_latency, rate_out)
				reconstructed_line_in_signal = pipeparts.mkgeneric(pipeline, reconstructed_line_in_signal, "lal_demodulate", line_frequency = -1.0 * freqs[m][n], prefactor_real = -2.0)
				# Connect to line frequency updater if given
				if any(freq_channels):
					if freq_channels[m][n] is not None:
						if type(freq_channels[m][n]) is float:
							# It's a harmonic of the frequency in freq_channels[m][0]
							freq_channels[m][0].connect("notify::timestamped-average", update_timestamped_property, reconstructed_line_in_signal, "timestamped_average", "line_frequency", -1.0 * freq_channels[m][n])
						else:
							# The channel carries the correct frequency
							freq_channels[m][n].connect("notify::timestamped-average", update_timestamped_property, reconstructed_line_in_signal, "timestamped_average", "line_frequency", -1.0)
				reconstructed_line_in_signal = pipeparts.mkgeneric(pipeline, reconstructed_line_in_signal, "creal")

				signal_minus_lines.append(reconstructed_line_in_signal)

	clean_signal = mkadder(pipeline, tuple(signal_minus_lines), queue_length = [queue_length])
	# Replace any nan's or inf's with zeros
	clean_signal = mkinsertgap(pipeline, clean_signal, insert_gap = False, replace_value = 0.0)
	return clean_signal

def removeDC(pipeline, head, rate):
	head = pipeparts.mktee(pipeline, head)
	DC = mkresample(pipeline, head, 4, True, 16)
	#DC = pipeparts.mkgeneric(pipeline, DC, "lal_smoothkappas", default_kappa_re = 0, array_size = 1, avg_array_size = 64)
	DC = mkresample(pipeline, DC, 4, True, rate)
	DC = pipeparts.mkaudioamplify(pipeline, DC, -1)

	return mkadder(pipeline, list_srcs(pipeline, head, DC))

def lowpass(pipeline, head, rate, length = 1.0, fcut = 500, filter_latency = 0.5, freq_res = 0.0, td = True):
	length = int(length * rate)

	# Find alpha, and the actual frequency resolution
	alpha = freq_res * length / rate if freq_res > 0.0 else 3.0
	alpha = 1.0 if alpha < 1.0 else alpha
	freq_res = alpha * rate / length

	# Adjust the cutoff frequency to "protect" the passband.
	if fcut != 0.0:
		fcut += 0.75 * freq_res

	# Compute a low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(fcut) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= fir.kaiser(length, numpy.pi * alpha) # fir.DPSS(length, alpha, max_time = 10)
	lowpass /= numpy.sum(lowpass)
	lowpass = numpy.float64(lowpass)

	# Now apply the filter
	return mkcomplexfirbank(pipeline, head, latency = int((length - 1) * filter_latency + 0.25), fir_matrix = [lowpass], time_domain = td)

def highpass(pipeline, head, rate, length = 1.0, fcut = 10.0, filter_latency = 0.5, freq_res = 0.0, td = True):
	length = int(length * rate)

	# Find alpha, and the actual frequency resolution
	alpha = freq_res * length / rate if freq_res > 0.0 else 3.0
	alpha = 1.0 if alpha < 1.0 else alpha
	freq_res = alpha * rate / length

	# Adjust the cutoff frequency to "protect" the passband.
	fcut -= 0.75 * freq_res

	# Compute a low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(fcut) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= fir.kaiser(length, numpy.pi * alpha) # fir.DPSS(length, alpha, max_time = 10)
	lowpass /= numpy.sum(lowpass)

	# Create a high-pass filter from the low-pass filter through spectral inversion.
	highpass = -lowpass
	highpass[int((length - 1) // 2)] += 1

	highpass = numpy.float64(highpass)

	# Now apply the filter
	return mkcomplexfirbank(pipeline, head, latency = int((length - 1) * filter_latency + 0.25), fir_matrix = [highpass], time_domain = td)

def bandpass(pipeline, head, rate, length = 1.0, f_low = 100, f_high = 400, filter_latency = 0.5, freq_res = 0.0, td = True):
	length = int(length * rate)

	# Find alpha, and the actual frequency resolution
	alpha = freq_res * length / rate if freq_res > 0.0 else 3.0
	alpha = 1.0 if alpha < 1.0 else alpha
	freq_res = alpha * rate / length

	# Adjust the cutoff frequency to "protect" the passband.
	f_low -= 0.75 * freq_res

	# Make a DPSS window
	dpss = fir.kaiser(length, numpy.pi * alpha) # fir.DPSS(length, alpha, max_time = 10)

	# Compute a temporary low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(f_low) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= dpss
	lowpass /= numpy.sum(lowpass)

	# Create the high-pass filter from the low-pass filter through spectral inversion.
	highpass = -lowpass
	highpass[(length - 1) // 2] += 1

	# Adjust the cutoff frequency to "protect" the passband.
	f_high += 0.75 * freq_res

	# Compute the low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(f_high) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= dpss
	lowpass /= numpy.sum(lowpass)

	# Do a circular convolution of the high-pass and low-pass filters to make a band-pass filter.
	bandpass = numpy.zeros(length, dtype = numpy.float128)
	for i in range(length):
		bandpass[i] = numpy.sum(highpass * numpy.roll(lowpass, (length - 1) // 2 - i))

	bandpass = numpy.float64(bandpass)

	# Now apply the filter
	return mkcomplexfirbank(pipeline, head, latency = int((length - 1) * 2 * filter_latency + 0.25), fir_matrix = [bandpass], time_domain = td)

def bandstop(pipeline, head, rate, length = 1.0, f_low = 100, f_high = 400, filter_latency = 0.5, freq_res = 0.0, td = True):
	length = int(length * rate)

	# Find alpha, and the actual frequency resolution
	alpha = freq_res * length / rate if freq_res > 0.0 else 3.0
	alpha = 1.0 if alpha < 1.0 else alpha
	freq_res = alpha * rate / length

	# Adjust the cutoff frequency to "protect" the passband.
	f_low += 0.75 * freq_res

	# Make a DPSS window
	dpss = fir.kaiser(length, numpy.pi * alpha) # fir.DPSS(length, alpha, max_time = 10)

	# Compute a temporary low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(f_low) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= dpss
	lowpass /= numpy.sum(lowpass)

	# Create the high-pass filter from the low-pass filter through spectral inversion.
	highpass = -lowpass
	highpass[(length - 1) // 2] += 1

	# Adjust the cutoff frequency to "protect" the passband.
	f_high -= 0.75 * freq_res

	# Compute the low-pass filter.
	lowpass = numpy.sinc(2 * numpy.float128(f_high) / rate * (numpy.arange(numpy.float128(length)) - (length - 1) // 2))
	lowpass *= dpss
	lowpass /= numpy.sum(lowpass)

	# Do a circular convolution of the high-pass and low-pass filters to make a temporary band-pass filter.
	bandpass = numpy.zeros(length, dtype = numpy.float128)
	for i in range(length):
		bandpass[i] = numpy.sum(highpass * numpy.roll(lowpass, (length - 1) // 2 - i))

	# Create a band-stop filter from the band-pass filter through spectral inversion.
	bandstop = -bandpass
	bandstop[(length - 1) // 2] += 1

	bandstop = numpy.float64(bandstop)

	# Now apply the filter
	return mkcomplexfirbank(pipeline, head, latency = int((length - 1) * 2 * filter_latency + 0.25), fir_matrix = [bandstop], time_domain = td)

def linear_phase_filter(pipeline, head, shift_samples, num_samples = 256, gain = 1.0, filter_update = None, sample_rate = 2048, update_samples = 320, average_samples = 1, phase_measurement_frequency = 100, taper_length = 320, kernel_endtime = None, filter_timeshift = 0):

	# Apply a linear-phase filter to shift timestamps.  shift_samples is the number
	# of samples of timestamp shift.  It need not be an integer.  A positive value
	# advances the output data relative to the timestamps, and a negative value
	# delays the output.

	# Compute filter using odd filter length
	odd_num_samples = int(num_samples) - (1 - int(num_samples) % 2)

	filter_latency_samples = int(num_samples / 2) + int(numpy.floor(shift_samples))
	fractional_shift_samples = shift_samples % 1

	# Make a filter using a sinc table, slightly shifted relative to the samples
	sinc_arg = numpy.arange(-int(odd_num_samples / 2), 1 + int(odd_num_samples / 2)) + fractional_shift_samples
	sinc_filter = numpy.sinc(sinc_arg)
	# Apply a Blackman window
	sinc_filter *= numpy.blackman(odd_num_samples)
	# Normalize the filter
	sinc_filter *= gain / numpy.sum(sinc_filter)
	# In case filter length is actually even
	if not int(num_samples) % 2:
		sinc_filter = numpy.insert(sinc_filter, 0, 0.0)

	# Filter the data
	if filter_update is None:
		# Static filter
		head = mkcomplexfirbank(pipeline, head, latency = filter_latency_samples, fir_matrix = [sinc_filter[::-1]], time_domain = True)
	else:
		# Filter gets updated with variable time delay and gain
		if kernel_endtime is None:
			# Update filter as soon as new filter is available, and do it with minimal latency
			head = mktdepfirfilt(pipeline, head, kernel = sinc_filter[::-1], latency = filter_latency_samples, taper_length = taper_length)
			filter_update = mkadaptivefirfilt(pipeline, filter_update, variable_filter_length = num_samples, adaptive_filter_length = num_samples, update_samples = update_samples, average_samples = average_samples, filter_sample_rate = sample_rate, phase_measurement_frequency = phase_measurement_frequency)
			filter_update.connect("notify::adaptive-filter", update_filter, head, "adaptive_filter", "kernel")
		else:
			# Update filters at specified timestamps to ensure reproducibility
			head = mktdepfirfilt(pipeline, mkqueue(pipeline, head), kernel = sinc_filter[::-1], latency = filter_latency_samples, taper_length = taper_length, kernel_endtime = kernel_endtime)
			filter_update = mkadaptivefirfilt(pipeline, filter_update, variable_filter_length = num_samples, adaptive_filter_length = num_samples, update_samples = update_samples, average_samples = average_samples, filter_sample_rate = sample_rate, phase_measurement_frequency = phase_measurement_frequency, filter_timeshift = filter_timeshift)
			filter_update.connect("notify::adaptive-filter", update_filter, head, "adaptive_filter", "kernel")
			filter_update.connect("notify::filter-endtime", update_property_simple, head, "filter_endtime", "kernel_endtime", 1)
	return head

def whiten(pipeline, head, num_samples = 512, nyq_magnitude = 1e15, scale = 'log', td = True):
	# Number of filter samples should be even, since numpy's inverse real fft returns an even length array
	num_samples += num_samples % 2
	fd_num_samples = num_samples // 2 + 1
	fd_filter = numpy.ones(fd_num_samples)
	fd_filter[-1] = nyq_magnitude
	if scale == 'log':
		log_nyq_mag = numpy.log10(nyq_magnitude)
		for i in range(1, fd_num_samples - 1):
			fd_filter[i] = pow(10, log_nyq_mag * float(i) / (fd_num_samples - 1))
	elif scale == 'linear':
		for i in range(1, fd_num_samples - 1):
			fd_filter[i] = nyq_magnitude * float(i) / (fd_num_samples - 1)
	else:
		raise ValueError("calibration_parts.whiten(): scale must be either 'log' or 'linear'.")
		return head

	# Take an inverse fft to get a time-domain filter
	whiten_filter = numpy.fft.irfft(fd_filter)
	# Add delay of half the filter length
	whiten_filter = numpy.roll(whiten_filter, num_samples // 2)
	# Window the filter
	whiten_filter *= numpy.blackman(num_samples)

	# Apply the filter
	return mkcomplexfirbank(pipeline, head, latency = num_samples // 2, fir_matrix = [whiten_filter[::-1]], time_domain = td)

def compute_rms(pipeline, head, rate, median_time, average_time, f_min = None, f_max = None, filter_latency = 0.5, rate_out = 16, td = True, gate = None, threshold = 1):
	# Find the root mean square amplitude of a signal between two frequencies
	# Downsample to save computational cost
	head = mkresample(pipeline, head, 4, filter_latency == 0.0, rate)

	# Remove any frequency content we don't care about
	if (f_min is not None) and (f_max is not None):
		head = bandpass(pipeline, head, rate, f_low = f_min, f_high = f_max, filter_latency = filter_latency, td = td)
	elif f_min is not None:
		head = highpass(pipeline, head, rate, fcut = f_min, filter_latency = filter_latency, td = td)
	elif f_max is not None:
		head = lowpass(pipeline, head, rate, fcut = f_max, filter_latency = filter_latency, td = td)

	# Square it
	head = mkpow(pipeline, head, exponent = 2.0)

	# Downsample again to save computational cost
	head = mkresample(pipeline, head, 4, filter_latency == 0.0, rate_out)

	# Gate if you don't want it to be updated during certain "bad" times
	# Resampling filters 33 samples at the rate_out, bandpass filters 16
	if gate is not None:
		attack_length = -16 * (1 - filter_latency) - (33 if filter_latency == 0.0 else 17)
		hold_length = 16 * filter_latency + (0 if filter_latency == 0.0 else 17)
		head = mkgate(pipeline, head, gate, threshold, attack_length = attack_length, hold_length = hold_length)

	# Compute running median and/or average
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", default_kappa_re = 0.0, array_size = median_time * rate_out, avg_array_size = average_time * rate_out, filter_latency = filter_latency)

	# Take the square root
	head = mkpow(pipeline, head, exponent = 0.5)

	return head

#
# Calibration factor related functions
#

def smooth_kappas_no_coherence(pipeline, head, var, expected, N, Nav, default_to_median, filter_latency):
	# Find median of calibration factors array with size N and smooth out medians with an average over Nav samples
	# Use the maximum_offset_re property to determine whether input kappas are good or not
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", maximum_offset_re = var, default_kappa_re = expected, array_size = N, avg_array_size = Nav, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def smooth_complex_kappas_no_coherence(pipeline, head, real_var, imag_var, real_expected, imag_expected, N, Nav, default_to_median, filter_latency):
	# Find median of complex calibration factors array with size N, split into real and imaginary parts, and smooth out medians with an average over Nav samples
	# Use the maximum_offset_re and maximum_offset_im properties to determine whether input kappas are good or not
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", maximum_offset_re = real_var, maximum_offset_im = imag_var, default_kappa_re = real_expected, default_kappa_im = imag_expected, array_size = N, avg_array_size = Nav, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def smooth_kappas(pipeline, head, expected, N, Nav, default_to_median, filter_latency):
	# Find median of calibration factors array with size N and smooth out medians with an average over Nav samples
	# Assume input was previously gated with coherence uncertainty to determine if input kappas are good or not
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", default_kappa_re = expected, array_size = N, avg_array_size = Nav, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def smooth_complex_kappas(pipeline, head, real_expected, imag_expected, N, Nav, default_to_median, filter_latency):
	# Find median of complex calibration factors array with size N and smooth out medians with an average over Nav samples
	# Assume input was previously gated with coherence uncertainty to determine if input kappas are good or not

	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", default_kappa_re = real_expected, default_kappa_im = imag_expected, array_size = N, avg_array_size = Nav, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def track_bad_kappas_no_coherence(pipeline, head, var, expected, N, Nav, default_to_median, filter_latency):
	# Produce output of 1's or 0's that correspond to median not corrupted (1) or corrupted (0) based on whether median of input array is defualt value.
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", maximum_offset_re = var, default_kappa_re = expected, array_size = N, avg_array_size = Nav if default_to_median else 1, track_bad_kappa = True, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def track_bad_complex_kappas_no_coherence(pipeline, head, real_var, imag_var, real_expected, imag_expected, N, Nav, default_to_median, filter_latency):
	# Produce output of 1's or 0's that correspond to median not corrupted (1) or corrupted (0) based on whether median of input array is defualt value.
	# Real and imaginary parts are done separately (outputs of lal_smoothkappas can be 1+i, 1, i, or 0)
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", maximum_offset_re = real_var, maximum_offset_im = imag_var, default_kappa_re = real_expected, default_kappa_im = imag_expected, array_size = N, avg_array_size = Nav if default_to_median else 1, track_bad_kappa = True, default_to_median = default_to_median, filter_latency = filter_latency)
	re, im = split_into_real(pipeline, head)
	return re, im

def track_bad_kappas(pipeline, head, expected, N, Nav, default_to_median, filter_latency):
	# Produce output of 1's or 0's that correspond to median not corrupted (1) or corrupted (0) based on whether median of input array is defualt value.
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", default_kappa_re = expected, array_size = N, avg_array_size = Nav if default_to_median else 1, track_bad_kappa = True, default_to_median = default_to_median, filter_latency = filter_latency)
	return head

def track_bad_complex_kappas(pipeline, head, real_expected, imag_expected, N, Nav, default_to_median, filter_latency):
	# Produce output of 1's or 0's that correspond to median not corrupted (1) or corrupted (0) based on whether median of input array is defualt value.
	# Real and imaginary parts are done separately (outputs of lal_smoothkappas can be 1+i, 1, i, or 0)

	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", default_kappa_re = real_expected, default_kappa_im = imag_expected, array_size = N, avg_array_size = Nav if default_to_median else 1, track_bad_kappa = True, default_to_median = default_to_median, filter_latency = filter_latency)
	re, im = split_into_real(pipeline, head)
	return re, im

def smooth_kappas_no_coherence_test(pipeline, head, var, expected, N, Nav, default_to_median, filter_latency):
	# Find median of calibration factors array with size N and smooth out medians with an average over Nav samples
	head = pipeparts.mktee(pipeline, head)
	pipeparts.mknxydumpsink(pipeline, head, "raw_kappatst.txt")
	head = pipeparts.mkgeneric(pipeline, head, "lal_smoothkappas", maximum_offset_re = var, default_kappa_re = expected, array_size = N, avg_array_size = Nav, default_to_median = default_to_median, filter_latency = filter_latency)
	head = pipeparts.mktee(pipeline, head)
	pipeparts.mknxydumpsink(pipeline, head, "smooth_kappatst.txt")
	return head

def compute_kappa_bits(pipeline, smooth, expected_real, expected_imag, real_ok_var, imag_ok_var, median_samples, avg_samples, status_out_smooth = 1, starting_rate=16, ending_rate=16):

	# Compensate for digital error in the running average
	expected_real_sum = 0.0
	expected_imag_sum = 0.0
	for i in range(avg_samples):
		expected_real_sum = expected_real_sum + expected_real
		expected_imag_sum = expected_imag_sum + expected_imag
	expected_real = expected_real_sum / avg_samples
	expected_imag = expected_imag_sum / avg_samples

	# Compute the property bad-data-intervals
	if type(real_ok_var) is not list:
		real_ok_var = [expected_real - real_ok_var, expected_real + real_ok_var]
	if type(imag_ok_var) is not list:
		imag_ok_var = [expected_imag - imag_ok_var, expected_imag + imag_ok_var]
	bad_data_intervals = [real_ok_var[0], imag_ok_var[0], expected_real, expected_imag, expected_real, expected_imag, real_ok_var[1], imag_ok_var[1]]

	# Use lal_insertgap to check if the data is within the required range
	smoothInRange = mkinsertgap(pipeline, smooth, bad_data_intervals = bad_data_intervals, insert_gap = True, remove_gap = False, replace_value = 0, fill_discont = True, block_duration = Gst.SECOND)
	# Turn it into a bit vector
	smoothInRange = pipeparts.mkbitvectorgen(pipeline, smoothInRange, nongap_is_control = True, bit_vector = status_out_smooth)
	smoothInRange = pipeparts.mkcapsfilter(pipeline, smoothInRange, "audio/x-raw, format=U32LE, rate=%d, channel-mask=(bitmask)0x0" % starting_rate)
	if starting_rate != ending_rate:
		smoothInRange = pipeparts.mkgeneric(pipeline, smoothInRange, "lal_logicalundersample", required_on = status_out_smooth, status_out = status_out_smooth)
		smoothInRange = pipeparts.mkcapsfilter(pipeline, smoothInRange, "audio/x-raw, format=U32LE, rate=%d, channel-mask=(bitmask)0x0" % ending_rate)
	smoothInRangetee = pipeparts.mktee(pipeline, smoothInRange)

	# Require that kappas have been in range for enough time for the smoothing process to settle
	min_samples = int(median_samples / 2) + avg_samples
	smoothInRange = mkgate(pipeline, smoothInRangetee, smoothInRangetee, status_out_smooth, attack_length = -min_samples)
	smoothInRange = pipeparts.mkbitvectorgen(pipeline, smoothInRange, nongap_is_control = True, bit_vector = status_out_smooth)
	smoothInRange = pipeparts.mkcapsfilter(pipeline, smoothInRange, "audio/x-raw, format=U32LE, rate=%d, channel-mask=(bitmask)0x0" % ending_rate)

	return smoothInRange

def compute_kappa_bits_only_real(pipeline, smooth, expected, ok_var, median_samples, avg_samples, status_out_smooth = 1, starting_rate=16, ending_rate=16):

	# Compensate for digital error in the running average
	expected_sum = 0.0
	for i in range(avg_samples):
		expected_sum = expected_sum + expected
	expected = expected_sum / avg_samples

	if type(ok_var) is list:
		smoothInRange = mkinsertgap(pipeline, smooth, bad_data_intervals = [ok_var[0], expected, expected, ok_var[1]], insert_gap = True, remove_gap = False, replace_value = 0, fill_discont = True, block_duration = Gst.SECOND)
	else:
		smoothInRange = mkinsertgap(pipeline, smooth, bad_data_intervals = [expected - ok_var, expected, expected, expected + ok_var], insert_gap = True, remove_gap = False, replace_value = 0, fill_discont = True, block_duration = Gst.SECOND)
	smoothInRange = pipeparts.mkbitvectorgen(pipeline, smoothInRange, nongap_is_control = True, bit_vector = status_out_smooth)
	smoothInRange = pipeparts.mkcapsfilter(pipeline, smoothInRange, "audio/x-raw, format=U32LE, rate=%d, channel-mask=(bitmask)0x0" % starting_rate)
	if starting_rate != ending_rate:
		smoothInRange = pipeparts.mkgeneric(pipeline, smoothInRange, "lal_logicalundersample", required_on = status_out_smooth, status_out = status_out_smooth)
		smoothInRange = pipeparts.mkcapsfilter(pipeline, smoothInRange, "audio/x-raw, format=U32LE, rate=%d, channel-mask=(bitmask)0x0" % ending_rate)
	smoothInRangetee = pipeparts.mktee(pipeline, smoothInRange)
	min_samples = int(median_samples / 2) + avg_samples
	smoothInRange = mkgate(pipeline, smoothInRangetee, smoothInRangetee, status_out_smooth, attack_length = -min_samples)
	smoothInRange = pipeparts.mkbitvectorgen(pipeline, smoothInRange, nongap_is_control = True, bit_vector = status_out_smooth)

	return smoothInRange

def merge_into_complex(pipeline, real, imag):
	# Merge real and imag into one complex channel with complex caps
	head = mkinterleave(pipeline, list_srcs(pipeline, real, imag))
	head = pipeparts.mktogglecomplex(pipeline, head)
	return head

def split_into_real(pipeline, complex_chan):
	# split complex channel with complex caps into two channels (real and imag) with real caps

	complex_chan = pipeparts.mktee(pipeline, complex_chan)
	real = pipeparts.mkgeneric(pipeline, complex_chan, "creal")
	imag = pipeparts.mkgeneric(pipeline, complex_chan, "cimag")

#	elem = pipeparts.mkgeneric(pipeline, elem, "deinterleave", keep_positions=True)
#	real = pipeparts.mkgeneric(pipeline, None, "identity")
#	pipeparts.src_deferred_link(elem, "src_0", real.get_static_pad("sink"))
#	imag = pipeparts.mkgeneric(pipeline, None, "identity")
#	pipeparts.src_deferred_link(elem, "src_1", imag.get_static_pad("sink"))

	return real, imag

def complex_audioamplify(pipeline, chan, WR, WI, return_mixer = False):
	# Multiply a complex channel chan by a complex number WR+I WI
	# Re[out] = -chanI*WI + chanR*WR
	# Im[out] = chanR*WI + chanI*WR

	head = pipeparts.mktogglecomplex(pipeline, chan)
	mixer = mkmatmix(pipeline, head, matrix=[[WR, WI],[-WI, WR]])
	head = pipeparts.mktogglecomplex(pipeline, mixer)

	if return_mixer:
		return head, mixer
	else:
		return head

def complex_inverse(pipeline, head):
	# Invert a complex number (1/z)

	head = mkpow(pipeline, head, exponent = -1)

	return head

def complex_division(pipeline, a, b):
	# Perform complex division of c = a/b and output the complex quotient c

	bInv = complex_inverse(pipeline, b)
	c = mkmultiplier(pipeline, list_srcs(pipeline, a, bInv))

	return c

def compute_kappatst_from_filters_file(pipeline, derrfesd, tstexcfesd, pcalfdarm, derrfdarm, ktstfacR, ktstfacI):

	#
	# \kappa_TST = ktstfac * (derrfesd/tstexcfesd) * (pcalfdarm/derrfdarm)
	# ktstfac = ATinvRratio_fT = (1/A0fesd) * (C0fdarm/(1+G0fdarm)) * ((1+G0fesd)/C0fesd)
	#

	derrfdarminv = complex_inverse(pipeline, derrfdarm)
	tstexcfesdinv = complex_inverse(pipeline, tstexcfesd)
	ktst = mkmultiplier(pipeline, list_srcs(pipeline, pcalfdarm, derrfdarminv, tstexcfesdinv, derrfesd))
	ktst = complex_audioamplify(pipeline, ktst, ktstfacR, ktstfacI)

	return ktst

def compute_kappatst(pipeline, derrfesd, tstexcfesd, pcalfdarm, derrfdarm, ktstfac):

	#
	# \kappa_TST = ktstfac * (derrfesd/tstexcfesd) * (pcalfdarm/derrfdarm)
	# ktstfac = ATinvRratio_fT = (1/A0fesd) * (C0fdarm/(1+G0fdarm)) * ((1+G0fesd)/C0fesd)
	#

	derrfdarminv = complex_inverse(pipeline, derrfdarm)
	tstexcfesdinv = complex_inverse(pipeline, tstexcfesd)
	ktst = mkmultiplier(pipeline, list_srcs(pipeline, ktstfac, pcalfdarm, derrfdarminv, tstexcfesdinv, derrfesd))

	return ktst

def compute_kappapum_from_filters_file(pipeline, derrfpum, pumexcfpum, pcalfpcal, derrfpcal, kpumfacR, kpumfacI):

	#
	# \kappa_PUM = kpumfac * [derr(fpum) / pumexc(fpum)] * [pcal(fpcal) / derr(fpcal)]
	# kpumfac = APinvRratio_fP = [1 / A_PUM0(fpum)] * [C0(fpcal) / (1 + G0(fpcal))] * [(1 + G0(fpum)) / C0(fpum)]
	#

	pumexcfpuminv = complex_inverse(pipeline, pumexcfpum)
	derrfpcalinv = complex_inverse(pipeline, derrfpcal)
	kpum = mkmultiplier(pipeline, list_srcs(pipeline, derrfpum, pumexcfpuminv, pcalfpcal, derrfpcalinv))
	kpum = complex_audioamplify(pipeline, kpum, kpumfacR, kpumfacI)

	return kpum

def compute_kappapum(pipeline, derrfpum, pumexcfpum, pcalfpcal, derrfpcal, kpumfac):

	#
	# \kappa_PUM = kpumfac * [derr(fpum) / pumexc(fpum)] * [pcal(fpcal) / derr(fpcal)]
	# kpumfac = APinvRratio_fP = [1 / A_PUM0(fpum)] * [C0(fpcal) / (1 + G0(fpcal))] * [(1 + G0(fpum)) / C0(fpum)]
	#

	pumexcfpuminv = complex_inverse(pipeline, pumexcfpum)
	derrfpcalinv = complex_inverse(pipeline, derrfpcal)
	kpum = mkmultiplier(pipeline, list_srcs(pipeline, kpumfac, derrfpum, pumexcfpuminv, pcalfpcal, derrfpcalinv))

	return kpum

def compute_kappauim_from_filters_file(pipeline, derrfuim, uimexcfuim, pcalfpcal, derrfpcal, kuimfacR, kuimfacI):

	#
	# \kappa_UIM = kuimfac * [derr(fuim) / uimexc(fuim)] * [pcal(fpcal) / derr(fpcal)]
	# kuimfac = AUinvRratio_fU = [1 / A_UIM0(fuim)] * [C0(fpcal) / (1 + G0(fpcal))] * [(1 + G0(fuim)) / C0(fuim)]
	#

	uimexcfuiminv = complex_inverse(pipeline, uimexcfuim)
	derrfpcalinv = complex_inverse(pipeline, derrfpcal)
	kuim = mkmultiplier(pipeline, list_srcs(pipeline, derrfuim, uimexcfuiminv, pcalfpcal, derrfpcalinv))
	kuim = complex_audioamplify(pipeline, kuim, kuimfacR, kuimfacI)

	return kuim

def compute_kappauim(pipeline, derrfuim, uimexcfuim, pcalfpcal, derrfpcal, kuimfac):

	#
	# \kappa_UIM = kuimfac * [derr(fuim) / uimexc(fuim)] * [pcal(fpcal) / derr(fpcal)]
	# kuimfac = AUinvRratio_fU = [1 / A_UIM0(fuim)] * [C0(fpcal) / (1 + G0(fpcal))] * [(1 + G0(fuim)) / C0(fuim)]
	#

	uimexcfuiminv = complex_inverse(pipeline, uimexcfuim)
	derrfpcalinv = complex_inverse(pipeline, derrfpcal)
	kuim = mkmultiplier(pipeline, list_srcs(pipeline, kuimfac, derrfuim, uimexcfuiminv, pcalfpcal, derrfpcalinv))

	return kuim

def compute_exact_kappas(pipeline, X, freqs, freq_channels, EPICS, EPICS_channels, rate, default_fcc = 400, default_fs_squared = 1.0, default_fs_over_Q = 1.0, max_error = 1e-5):

	#
	# See P1900052, Section 5.2.6 for details.  All constants are contained in the list
	# variable EPICS.  The variable freqs is a list containing calibration line
	# frequencies, stored in the order f1, f2, fT, fP, fU, i.e., the Pcal lines come
	# first, and then the actuator lines.  All other quantities evaluated at the
	# calibration lines are stored in the same order.  The list variable X contains the
	# ratios X[i] = injection(f_i) / d_err(f_i) for each calibration line frequency.
	#

	kappas = []
	num_lines = len(freqs)
	num_stages = num_lines - 2 # Stages of actuation (currently 3)
	MV_matrix = list(numpy.zeros(2 * num_stages * (2 * num_stages + 1)))
	Y = []
	Yreal = []
	Yimag = []
	CAX = []
	CAXreal = []
	CAXimag = []
	Cinv_tdep = []

	# For real-time updates of properties in low-latency
	receivers = []
	if EPICS_channels is not None:
		for i in range(len(EPICS_channels)):
			EPICS_channels[i] = pipeparts.mkgeneric(pipeline, EPICS_channels[i], "lal_property", update_when_change = True)

	for i in range(num_lines):
		if i < 2:
			# Then it's a Pcal line
			Yi, mixer = complex_audioamplify(pipeline, X[i], EPICS[2 * (1 + num_stages) * i], EPICS[2 * (1 + num_stages) * i + 1], return_mixer = True)
			Y.append(pipeparts.mktee(pipeline, Yi))
			receivers.append(mixer)
			Yreal.append(pipeparts.mktee(pipeline, mkcapsfiltersetter(pipeline, pipeparts.mkgeneric(pipeline, Y[i], "creal"), "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate, name = "capsfilter_Yreal_%d" % i)))
			Yimag.append(pipeparts.mktee(pipeline, mkcapsfiltersetter(pipeline, pipeparts.mkgeneric(pipeline, Y[i], "cimag"), "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate, name = "capsfilter_Yimag_%d" % i)))
		else:
			# It's an actuator line
			CAXi, mixer = complex_audioamplify(pipeline, X[i], EPICS[2 * (1 + num_stages) * i], EPICS[2 * (1 + num_stages) * i + 1], return_mixer = True)
			CAX.append(pipeparts.mktee(pipeline, CAXi))
			receivers.append(mixer)
			CAXreal.append(pipeparts.mktee(pipeline, mkcapsfiltersetter(pipeline, pipeparts.mkgeneric(pipeline, CAX[-1], "creal"), "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate, name = "capsfilter_CAXreal_%d" % i)))
			CAXimag.append(pipeparts.mktee(pipeline, mkcapsfiltersetter(pipeline, pipeparts.mkgeneric(pipeline, CAX[-1], "cimag"), "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate, name = "capsfilter_CAXimag_%d" % i)))

	# Let's start by computing the V's of Eqs. 5.2.78 and 5.2.79
	for j in range(num_stages):
		factor1 = pow(freqs[0], -2) - pow(freqs[2 + j], -2)
		factor2 = pow(freqs[2 + j], -2) - pow(freqs[1], -2)
		factor3 = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2))
		factor4 = freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2))
		amp1 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, Yreal[1], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor1)
		amp2 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, Yreal[0], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor2)
		Vj = mkadder(pipeline, list_srcs(pipeline, amp1, amp2))
		receivers.extend((amp1, amp2))
		Vj = pipeparts.mkcapsfilter(pipeline, Vj, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate)
		amp1 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, Yimag[1], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor3)
		amp2 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, Yimag[0], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor4)
		Vjplus3 = mkadder(pipeline, list_srcs(pipeline, amp1, amp2))
		receivers.extend((amp1, amp2))
		Vjplus3 = pipeparts.mkcapsfilter(pipeline, Vjplus3, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate)
		MV_matrix[j] = Vj
		MV_matrix[num_stages + j] = Vjplus3

	# Now let's compute the elements of the matrix M, given by Eqs. 5.2.70 - 5.2.77
	# Many of the elements are constant, so make a stream of ones to multiply
	if num_stages > 1:
		ones = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkcapsfilter(pipeline, Yreal[0], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), exponent = 0.0))
	for j in range(num_stages):
		# Time-dependent matrix elements
		factor = pow(freqs[0], -2) - pow(freqs[1], -2)
		addend = (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[2 * ((1 + num_stages) + 1 + j)] + (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[2 * (1 + j)] - (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		amp = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, CAXreal[j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
		Mjj = pipeparts.mkgeneric(pipeline, amp, "lal_add_constant", value = addend)
		receivers.extend((amp, Mjj))
		factor = -2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2))
		addend = -2.0 * numpy.pi * freqs[1] * (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + j)] - 2.0 * numpy.pi * freqs[0] * (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * (1 + j)] + 2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		amp = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, CAXimag[j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
		Mjjplus3 = pipeparts.mkgeneric(pipeline, amp, "lal_add_constant", value = addend)
		receivers.extend((amp, Mjjplus3))
		factor = -freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2))
		addend = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + j)] + freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[1 + 2 * (1 + j)] + freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		amp = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, CAXimag[j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
		Mjplus3j = pipeparts.mkgeneric(pipeline, amp, "lal_add_constant", value = addend)
		receivers.extend((amp, Mjplus3j))
		factor = -2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2))
		addend = 2.0 * numpy.pi * pow(freqs[1], 2) * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[2 * ((1 + num_stages) + 1 + j)] + 2.0 * numpy.pi * pow(freqs[0], 2) * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[2 * (1 + j)] + 2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		amp = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, CAXreal[j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
		Mjplus3jplus3 = pipeparts.mkgeneric(pipeline, amp, "lal_add_constant", value = addend)
		receivers.extend((amp, Mjplus3jplus3))
		# Add these into the matrix
		MV_matrix[(1 + j) * 2 * num_stages + j] = Mjj
		MV_matrix[(1 + j) * 2 * num_stages + num_stages + j] = Mjjplus3
		MV_matrix[(1 + num_stages + j) * 2 * num_stages + j] = Mjplus3j
		MV_matrix[(1 + num_stages + j) * 2 * num_stages + num_stages + j] = Mjplus3jplus3

		# Constant matrix elements
		knotequalj = list(numpy.arange(num_stages))
		knotequalj.remove(j)
		for k in knotequalj:
			factor = (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[2 * ((1 + num_stages) + 1 + k)] + (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[2 * (1 + k)] - (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			Mjk = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, ones, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
			receivers.append(Mjk)
			factor = -2.0 * numpy.pi * freqs[1] * (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + k)] - 2.0 * numpy.pi * freqs[0] * (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * (1 + k)] + 2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			Mjkplus3 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, ones, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
			receivers.append(Mjkplus3)
			factor = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + k)] + freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[1 + 2 * (1 + k)] + freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			Mjplus3k = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, ones, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
			receivers.append(Mjplus3k)
			factor = 2.0 * numpy.pi * pow(freqs[1], 2) * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[2 * ((1 + num_stages) + 1 + k)] + 2.0 * numpy.pi * pow(freqs[0], 2) * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[2 * (1 + k)] + 2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			Mjplus3kplus3 = pipeparts.mkaudioamplify(pipeline, pipeparts.mkcapsfilter(pipeline, ones, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), factor)
			receivers.append(Mjplus3kplus3)
			# Add these into the matrix
			MV_matrix[(1 + j) * 2 * num_stages + k] = Mjk
			MV_matrix[(1 + j) * 2 * num_stages + num_stages + k] = Mjkplus3
			MV_matrix[(1 + num_stages + j) * 2 * num_stages + k] = Mjplus3k
			MV_matrix[(1 + num_stages + j) * 2 * num_stages + num_stages + k] = Mjplus3kplus3

	# Now pass these to the matrix solver to find kappa_T, kappa_P, kappa_U, tau_T, tau_P, and tau_U.
	MV_matrix = mkinterleave(pipeline, MV_matrix)
	MV_matrix = pipeparts.mkcapsfilter(pipeline, MV_matrix, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=%d" % (rate, (2 * num_stages) * (2 * num_stages + 1)))
	kappas = pipeparts.mkgeneric(pipeline, MV_matrix, "lal_matrixsolver")
	kappas = list(mkdeinterleave(pipeline, pipeparts.mkcapsfilter(pipeline, kappas, "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=%d" % (rate, 2 * num_stages)), 2 * num_stages))
	for i in range(len(kappas)):
		kappas[i] = pipeparts.mkcapsfilter(pipeline, kappas[i], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate)
		if i >= len(kappas) // 2:
			kappas[i] = mkmultiplier(pipeline, [kappas[i], mkpow(pipeline, kappas[i - len(kappas) // 2], exponent = -1.0)])
		kappas[i] = pipeparts.mktee(pipeline, kappas[i])

	# Next, compute the sensing function time dependence.
	# Start by computing G_res at each frequency, defined in Eq. 5.2.30
	for n in range(2):
		Cinv_tdep_components = [Y[n]]
		for j in range(num_stages):
			minuskappajGresjatn = mkmatmix(pipeline, pipeparts.mkcapsfilter(pipeline, kappas[j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), matrix = [[-EPICS[2 * (n * (1 + num_stages) + 1 + j)], -EPICS[1 + 2 * (n * (1 + num_stages) + 1 + j)]]])
			receivers.append(minuskappajGresjatn)
			minuskappajGresjatn = pipeparts.mktogglecomplex(pipeline, minuskappajGresjatn)
			i_omega_tau = mkmatmix(pipeline, pipeparts.mkcapsfilter(pipeline, kappas[num_stages + j], "audio/x-raw,format=F64LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate), matrix = [[0, 2.0 * numpy.pi * freqs[n]]])
			receivers.append(i_omega_tau)
			i_omega_tau = pipeparts.mktogglecomplex(pipeline, i_omega_tau)
			i_omega_tau = mkcapsfiltersetter(pipeline, i_omega_tau, "audio/x-raw,format=Z128LE,rate=%d,channel-mask=(bitmask)0x0,channels=1" % rate)
			phase = pipeparts.mkgeneric(pipeline, i_omega_tau, "cexp")
			Cinv_tdep_components.append(mkmultiplier(pipeline, list_srcs(pipeline, minuskappajGresjatn, phase)))
		Cinv_tdep.append(mkadder(pipeline, Cinv_tdep_components))

	sensing_inputs = mkinterleave(pipeline, Cinv_tdep, complex_data = True)
	sensing_outputs = pipeparts.mkgeneric(pipeline, sensing_inputs, "lal_sensingtdcfs", sensing_model = 0, freq1 = freqs[0], freq2 = freqs[1], current_fcc = default_fcc, current_fs_squared = default_fs_squared, current_fs_over_Q = default_fs_over_Q, max_error = max_error)
	receivers.append(sensing_outputs)
	sensing_outputs = list(mkdeinterleave(pipeline, sensing_outputs, 4))

	# For real-time updates of properties in low-latency
	if EPICS_channels is not None:
		for i in range(len(freq_channels)):
			freq_channels[i].connect("notify::current-average", update_exact_kappas, freq_channels, EPICS_channels, receivers, freqs, EPICS)
		for i in range(len(EPICS_channels)):
			EPICS_channels[i].connect("notify::current-average", update_exact_kappas, freq_channels, EPICS_channels, receivers, freqs, EPICS)

	kappas += sensing_outputs

	return kappas

def compute_S_from_filters_file(pipeline, fpcal2, C0_f2R, C0_f2I, pcalfpcal2, derrfpcal2, D_f2R, D_f2I, ftst, ktst, apply_complex_ktst, AT_f2R, AT_f2I, fpum, kpum, apply_complex_kpum, AP_f2R, AP_f2I, fuim, kuim, apply_complex_kuim, AU_f2R, AU_f2I):

	#
	# S = (1 / C0_f2) * (pcalfpcal2 / derrfpcal2 - D_f2 * (ktst * AT_f2 + kpum * AP_f2 + kuim * AU_f2))^(-1)
	#

	if apply_complex_ktst:
		ktst = pipeparts.mkgeneric(pipeline, ktst, "lpshiftfreq", frequency_ratio = fpcal2 / ftst)
		ep8_ktst = complex_audioamplify(pipeline, ktst, AT_f2R, AT_f2I)
	else:
		ep8_ktst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, ktst, "cabs"), matrix = [[AT_f2R, AT_f2I]]))
	if apply_complex_kpum:
		kpum = pipeparts.mkgeneric(pipeline, kpum, "lpshiftfreq", frequency_ratio = fpcal2 / fpum)
		ep18_kpum = complex_audioamplify(pipeline, kpum, AP_f2R, AP_f2I)
	else:
		ep18_kpum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kpum, "cabs"), matrix = [[AP_f2R, AP_f2I]]))
	if apply_complex_kuim:
		kuim = pipeparts.mkgeneric(pipeline, kuim, "lpshiftfreq", frequency_ratio = fpcal2 / fuim)
		ep19_kuim = complex_audioamplify(pipeline, kuim, AU_f2R, AU_f2I)
	else:
		ep19_kuim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kuim, "cabs"), matrix = [[AU_f2R, AU_f2I]]))

	pcal_over_derr = complex_division(pipeline, pcalfpcal2, derrfpcal2)
	A_at_fpcal2 = mkadder(pipeline, list_srcs(pipeline, ep8_ktst, ep18_kpum, ep19_kuim))
	DA_at_fpcal2 = complex_audioamplify(pipeline, A_at_fpcal2, -1.0 * D_f2R, -1.0 * D_f2I)
	Sinv = mkadder(pipeline, list_srcs(pipeline, pcal_over_derr, DA_at_fpcal2))
	Sinv = complex_audioamplify(pipeline, Sinv, C0_f2R, C0_f2I)
	S = complex_inverse(pipeline, Sinv)

	return S

def compute_S(pipeline, fpcal2, C0_f2, pcalfpcal2, derrfpcal2, D_f2, ftst, ktst, apply_complex_ktst, AT_f2, fpum, kpum, apply_complex_kpum, AP_f2, fuim, kuim, apply_complex_kuim, AU_f2):

	#
	# S = (1 / C0_f2) * (pcalfpcal2 / derrfpcal2 - D_f2 * (ktst * AT_f2 + kpu * AP_f2 + kuim * AU_f2))^(-1)
	#

	if apply_complex_ktst:
		ktst = pipeparts.mkgeneric(pipeline, ktst, "lpshiftfreq", frequency_ratio = fpcal2 / ftst)
	else:
		ktst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, ktst, "cabs"), matrix = [[1.0, 0.0]]))
	if apply_complex_kpum:
		kpum = pipeparts.mkgeneric(pipeline, kpum, "lpshiftfreq", frequency_ratio = fpcal2 / fpum)
	else:
		kpum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kpum, "cabs"), matrix = [[1.0, 0.0]]))
	if apply_complex_kuim:
		kuim = pipeparts.mkgeneric(pipeline, kuim, "lpshiftfreq", frequency_ratio = fpcal2 / fuim)
	else:
		kuim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kuim, "cabs"), matrix = [[1.0, 0.0]]))

	pcal_over_derr = complex_division(pipeline, pcalfpcal2, derrfpcal2)
	ep8_ktst = mkmultiplier(pipeline, list_srcs(pipeline, ktst, AT_f2))
	ep18_kpum = mkmultiplier(pipeline, list_srcs(pipeline, kpum, AP_f2))
	ep19_kuim = mkmultiplier(pipeline, list_srcs(pipeline, kuim, AU_f2))
	A_at_fpcal2 = mkadder(pipeline, list_srcs(pipeline, ep8_ktst, ep18_kpum, ep19_kuim))
	DA_at_fpcal2 = mkmultiplier(pipeline, list_srcs(pipeline, complex_audioamplify(pipeline, D_f2, -1.0, 0.0), A_at_fpcal2))
	Sinv = mkadder(pipeline, list_srcs(pipeline, pcal_over_derr, DA_at_fpcal2))
	Sinv = mkmultiplier(pipeline, list_srcs(pipeline, C0_f2, Sinv))
	S = complex_inverse(pipeline, Sinv)

	return S

def compute_kappac(pipeline, SR, SI):

	#
	# \kappa_C = |S|^2 / Re[S]
	#

	SR = pipeparts.mktee(pipeline, SR)
	S2 = mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, SR, exponent=2.0), mkpow(pipeline, SI, exponent=2.0)))
	kc = mkmultiplier(pipeline, list_srcs(pipeline, S2, mkpow(pipeline, SR, exponent=-1.0)))
	return kc

def compute_fcc(pipeline, SR, SI, fpcal2, freq_update = None):

	#
	# f_cc = - (Re[S]/Im[S]) * fpcal2
	#

	
	fcc = mkmultiplier(pipeline, list_srcs(pipeline, pipeparts.mkaudioamplify(pipeline, SR, -1.0), mkpow(pipeline, SI, exponent=-1.0)))
	fcc = pipeparts.mkaudioamplify(pipeline, fcc, fpcal2)
	if freq_update is not None:
		freq_update.connect("notify::timestamped-average", update_timestamped_property, fcc, "timestamped_average", "amplification", 1)
	return fcc

def compute_Xi_from_filters_file(pipeline, pcalfpcal4, darmfpcal4, fpcal4, C0_f1R, C0_f1I, D_f1R, D_f1I, AT_f1R, AT_f1I, AP_f1R, AP_f1I, AU_f1R, AU_f1I, ftst, ktst, apply_complex_ktst, fpum, kpum, apply_complex_kpum, fuim, kuim, apply_complex_kuim, kc, fcc):

	#
	# Xi = -1 + ((C0_f1 * kc) / (1 + i * f_src / f_cc)) * (pcalfpcal4 / derrfpcal4 - D_f1 * (ktst * AT_f1 + kpum * AP_f1 + kuim * AU_f1))
	#

	if apply_complex_ktst:
		ktst = pipeparts.mkgeneric(pipeline, ktst, "lpshiftfreq", frequency_ratio = fpcal4 / ftst)
		Atst = complex_audioamplify(pipeline, ktst, AT_f1R, AT_f1I)
	else:
		Atst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, ktst, "cabs"), matrix = [[AT_f1R, AT_f1I]]))
	if apply_complex_kpum:
		kpum = pipeparts.mkgeneric(pipeline, kpum, "lpshiftfreq", frequency_ratio = fpcal4 / fpum)
		Apum = complex_audioamplify(pipeline, kpum, AP_f1R, AP_f1I)
	else:
		Apum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kpum, "cabs"), matrix = [[AP_f1R, AP_f1I]]))
	if apply_complex_kuim:
		kuim = pipeparts.mkgeneric(pipeline, kuim, "lpshiftfreq", frequency_ratio = fpcal4 / fuim)
		Auim = complex_audioamplify(pipeline, kuim, AU_f1R, AU_f1I)
	else:
		Auim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kuim, "cabs"), matrix = [[AU_f1R, AU_f1I]]))

	A = mkadder(pipeline, list_srcs(pipeline, Atst, Apum, Auim))
	minusAD = complex_audioamplify(pipeline, A, -1.0 * D_f1R, -1.0 * D_f1I)
	pcal_over_derr = complex_division(pipeline, pcalfpcal4, darmfpcal4)
	pcal_over_derr_res = mkadder(pipeline, list_srcs(pipeline, pcal_over_derr, minusAD))
	fpcal4_over_fcc = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, fcc, exponent = -1.0), fpcal4)
	i_fpcal4_over_fcc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, fpcal4_over_fcc, matrix = [[0, 1]]))
	i_fpcal4_over_fcc_plus_one = pipeparts.mkgeneric(pipeline, i_fpcal4_over_fcc, "lal_add_constant", value = 1.0)
	i_fpcal4_over_fcc_plus_one_inv = complex_inverse(pipeline, i_fpcal4_over_fcc_plus_one)
	kc_C0_f1 = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kc, matrix = [[C0_f1R, C0_f1I]]))
	Xi_plus_one = mkmultiplier(pipeline, list_srcs(pipeline, kc_C0_f1, i_fpcal4_over_fcc_plus_one_inv, pcal_over_derr_res))
	Xi = pipeparts.mkgeneric(pipeline, Xi_plus_one, "lal_add_constant", value = -1.0)

	return Xi

def compute_Xi(pipeline, pcalfpcal4, darmfpcal4, fpcal4, C0_f1, D_f1, AT_f1, AP_f1, AU_f1, ftst, ktst, apply_complex_ktst, fpum, kpum, apply_complex_kpum, fuim, kuim, apply_complex_kuim, kc, fcc):

	#
	# Xi = -1 + ((C0_f1 * kc) / (1 + i * f_src / f_cc)) * (pcalfpcal4 / derrfpcal4 - D_f1 * (ktst * AT_f1 + kpum * AP_f1 + kuim * AU_f1))
	#

	if apply_complex_ktst:
		ktst = pipeparts.mkgeneric(pipeline, ktst, "lpshiftfreq", frequency_ratio = fpcal4 / ftst)
	else:
		ktst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, ktst, "cabs"), matrix = [[1.0, 0.0]]))
	if apply_complex_kpum:
		kpum = pipeparts.mkgeneric(pipeline, kpum, "lpshiftfreq", frequency_ratio = fpcal4 / fpum)
	else:
		kpum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kpum, "cabs"), matrix = [[1.0, 0.0]]))
	if apply_complex_kuim:
		kuim = pipeparts.mkgeneric(pipeline, kuim, "lpshiftfreq", frequency_ratio = fpcal4 / fuim)
	else:
		kuim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, pipeparts.mkgeneric(pipeline, kuim, "cabs"), matrix = [[1.0, 0.0]]))

	complex_kc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kc, matrix=[[1,0]]))
	Atst = mkmultiplier(pipeline, list_srcs(pipeline, AT_f1, ktst))
	Apum = mkmultiplier(pipeline, list_srcs(pipeline, AP_f1, kpum))
	Auim = mkmultiplier(pipeline, list_srcs(pipeline, AU_f1, kuim))
	A = mkadder(pipeline, list_srcs(pipeline, Atst, Apum, Auim))
	minusAD = mkmultiplier(pipeline, list_srcs(pipeline, complex_audioamplify(pipeline, D_f1, -1.0, 0.0), A))
	pcal_over_derr = complex_division(pipeline, pcalfpcal4, darmfpcal4)
	pcal_over_derr_res = mkadder(pipeline, list_srcs(pipeline, pcal_over_derr, minusAD))
	fpcal4_over_fcc = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, fcc, exponent = -1.0), fpcal4)
	i_fpcal4_over_fcc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, fpcal4_over_fcc, matrix = [[0, 1]]))
	i_fpcal4_over_fcc_plus_one = pipeparts.mkgeneric(pipeline, i_fpcal4_over_fcc, "lal_add_constant", value = 1.0)
	i_fpcal4_over_fcc_plus_one_inv = complex_inverse(pipeline, i_fpcal4_over_fcc_plus_one)
	Xi_plus_one = mkmultiplier(pipeline, list_srcs(pipeline, C0_f1, complex_kc, i_fpcal4_over_fcc_plus_one_inv, pcal_over_derr_res))
	Xi = pipeparts.mkgeneric(pipeline, Xi_plus_one, "lal_add_constant", value = -1.0)

	return Xi

def compute_uncertainty_reduction(pipeline, head, demod_samples, median_samples, avg_samples):
	#
	# How much is the uncertainty of the TDCFs reduced by the running median
	# and average, given the length of the demodulation filter?
	#

	# Represent each process as a filter with the same effect on uncertainty
	demod_filt = fir.kaiser(demod_samples, 3 * numpy.pi)
	demod_filt /= numpy.sum(demod_filt)
	if demod_samples < 1:
		demod_filt = numpy.ones(1)

	demod_uncertainty_reduction = numpy.sqrt(sum(pow(demod_filt, 2.0)))

	# In the limit of large N, a median reduces uncertainty by sqrt(pi/(2N)),
	# so pretend it's a filter where each coefficient equals sqrt(pi/2) / N.
	median_filt = numpy.ones(median_samples) / median_samples * numpy.sqrt(numpy.pi / 2.0)
	if median_samples < 1:
		median_filt = numpy.ones(1)

	avg_filt = numpy.ones(avg_samples) / avg_samples
	if avg_samples < 1:
		avg_filt = numpy.ones(1)

	effective_filt = numpy.convolve(numpy.convolve(demod_filt, median_filt), avg_filt)
	uncertainty_reduction = numpy.sqrt(sum(pow(effective_filt, 2.0)))

	return pipeparts.mkaudioamplify(pipeline, head, uncertainty_reduction / demod_uncertainty_reduction)

def compute_calline_uncertainty(pipeline, coh_unc, coh_samples, demod_samples, median_samples, avg_samples):
	#
	# The coherence uncertainties may not be equal to the
	# uncertainties in the line ratios after the low-pass filtering
	# (kaiser window), running median, and running mean.
	#

	# I assume that the uncertainty computed from coherence assumes
	# that a mean was used.
	assumed_unc_reduction = 1.0 / numpy.sqrt(coh_samples)

	# Represent each element as a filter with the same effect on uncertainty
	demod_filt = fir.kaiser(demod_samples, 3 * numpy.pi)
	demod_filt /= numpy.sum(demod_filt)
	if demod_samples < 1:
		demod_filt = numpy.ones(1)

	# In the limit of large N, a median reduces uncertainty by sqrt(pi/(2N)),
	# so pretend it's a filter where each coefficient equals sqrt(pi/2) / N.
	median_filt = numpy.ones(median_samples) / median_samples * numpy.sqrt(numpy.pi / 2.0)
	if median_samples < 1:
		median_filt = numpy.ones(1)

	avg_filt = numpy.ones(avg_samples) / avg_samples
	if avg_samples < 1:
		avg_filt = numpy.ones(1)

	effective_filt = numpy.convolve(numpy.convolve(demod_filt, median_filt), avg_filt)
	uncertainty_reduction = numpy.sqrt(sum(pow(effective_filt, 2.0)))

	return pipeparts.mkaudioamplify(pipeline, coh_unc, uncertainty_reduction / assumed_unc_reduction)

def compute_act_stage_uncertainty(pipeline, pcaly_line1_coh, sus_line_coh, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples, coherence_unc_threshold):
	pcaly_line1_coh_clipped = mkinsertgap(pipeline, pcaly_line1_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	sus_line_coh_clipped = mkinsertgap(pipeline, sus_line_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	pcaly_line1_unc = compute_calline_uncertainty(pipeline, pcaly_line1_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	sus_line_unc = compute_calline_uncertainty(pipeline, sus_line_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	return mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, pcaly_line1_unc, exponent = 2.0), mkpow(pipeline, sus_line_unc, exponent = 2.0))), exponent = 0.5)

def compute_S_c_uncertainty_from_filters_file(pipeline, C0_f2_real, C0_f2_imag, D_f2_real, D_f2_imag, opt_gain_fcc_line_freq, X2, pcaly_line2_coh, AT_f2_real, AT_f2_imag, ktst, tau_tst, apply_complex_kappatst, ktst_unc, AP_f2_real, AP_f2_imag, kpum, tau_pum, apply_complex_kappapum, kpum_unc, AU_f2_real, AU_f2_imag, kuim, tau_uim, apply_complex_kappauim, kuim_unc, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples, coherence_unc_threshold):

	#
	# S_c = (1 / C0_f2) * (X2 - D_f2 * (ktst * AT_f2 + kpum * AP_f2 + kuim * AU_f2))^(-1)
	#

	D_f2_mag = pow(D_f2_real * D_f2_real + D_f2_imag * D_f2_imag, 0.5)
	AT_f2_mag = pow(AT_f2_real * AT_f2_real + AT_f2_imag * AT_f2_imag, 0.5)
	AP_f2_mag = pow(AP_f2_real * AP_f2_real + AP_f2_imag * AP_f2_imag, 0.5)
	AU_f2_mag = pow(AU_f2_real * AU_f2_real + AU_f2_imag * AU_f2_imag, 0.5)

	X2 = pipeparts.mktee(pipeline, X2)
	pcaly_line2_coh_clipped = mkinsertgap(pipeline, pcaly_line2_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	X2_unc = compute_calline_uncertainty(pipeline, pcaly_line2_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	X2_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, X2_unc, pipeparts.mkgeneric(pipeline, X2, "cabs")))

	A_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, ktst, matrix = [[AT_f2_real, AT_f2_imag]]))
	if apply_complex_kappatst:
		i_omega_tau_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_tst, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_tst = pipeparts.mkgeneric(pipeline, i_omega_tau_tst, "cexp")
		A_tst = mkmultiplier(pipeline, list_srcs(pipeline, A_tst, exp_i_omega_tau_tst))
	A_tst_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, ktst, ktst_unc)), AT_f2_mag)
	A_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kpum, matrix = [[AP_f2_real, AP_f2_imag]]))
	if apply_complex_kappapum:
		i_omega_tau_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_pum, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_pum = pipeparts.mkgeneric(pipeline, i_omega_tau_pum, "cexp")
		A_pum = mkmultiplier(pipeline, list_srcs(pipeline, A_pum, exp_i_omega_tau_pum))
	A_pum_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, kpum, kpum_unc)), AP_f2_mag)
	A_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kuim, matrix = [[AU_f2_real, AU_f2_imag]]))
	if apply_complex_kappauim:
		i_omega_tau_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_uim, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_uim = pipeparts.mkgeneric(pipeline, i_omega_tau_uim, "cexp")
		A_uim = mkmultiplier(pipeline, list_srcs(pipeline, A_uim, exp_i_omega_tau_uim))
	A_uim_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, kuim, kuim_unc)), AU_f2_mag)
	A = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, A_tst, A_pum, A_uim)))
	A_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, A_tst_unc_abs, exponent = 2.0), mkpow(pipeline, A_pum_unc_abs, exponent = 2.0), mkpow(pipeline, A_uim_unc_abs, exponent = 2.0))), exponent = 0.5)
	minus_DA = complex_audioamplify(pipeline, A, -D_f2_real, -D_f2_imag)
	DA_unc_abs = pipeparts.mkaudioamplify(pipeline, A_unc_abs, D_f2_mag)
	X2_minus_DA = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, X2, minus_DA)))
	X2_minus_DA_mag = pipeparts.mkgeneric(pipeline, X2_minus_DA, "cabs")
	X2_minus_DA_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, X2_unc_abs, exponent = 2.0), mkpow(pipeline, DA_unc_abs, exponent = 2.0))), exponent = 0.5)
	S_c_unc = pipeparts.mktee(pipeline, complex_division(pipeline, X2_minus_DA_unc_abs, X2_minus_DA_mag))
	S_c = pipeparts.mktee(pipeline, complex_inverse(pipeline, complex_audioamplify(pipeline, X2_minus_DA, C0_f2_real, C0_f2_imag)))
	S_c_real = pipeparts.mkgeneric(pipeline, S_c, "creal")
	S_c_imag = pipeparts.mkgeneric(pipeline, S_c, "cimag")
	S_c_product = pipeparts.mkgeneric(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real, S_c_imag)), "cabs")
	S_c_square_modulus = pipeparts.mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "cabs"), exponent = 2.0)
	S_c_square_modulus_over_S_c_product = complex_division(pipeline, S_c_square_modulus, S_c_product)
	fcc_unc = mkmultiplier(pipeline, list_srcs(pipeline, S_c_square_modulus_over_S_c_product, S_c_unc))

	return S_c, S_c_unc, fcc_unc

def compute_S_c_uncertainty(pipeline, C0_f2, D_f2, opt_gain_fcc_line_freq, X2, pcaly_line2_coh, AT_f2, ktst, tau_tst, apply_complex_kappatst, ktst_unc, AP_f2, kpum, tau_pum, apply_complex_kappapum, kpum_unc, AU_f2, kuim, tau_uim, apply_complex_kappauim, kuim_unc, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples, coherence_unc_threshold):

	#
	# S_c = (1 / C0_f2) * (X2 - D_f2 * (ktst * AT_f2 + kpum * AP_f2 + kuim * AU_f2))^(-1)
	#

	D_f2_mag = pipeparts.mkgeneric(pipeline, D_f2, "cabs")
	AT_f2_mag = pipeparts.mkgeneric(pipeline, AT_f2, "cabs")
	AP_f2_mag = pipeparts.mkgeneric(pipeline, AP_f2, "cabs")
	AU_f2_mag = pipeparts.mkgeneric(pipeline, AU_f2, "cabs")

	X2 = pipeparts.mktee(pipeline, X2)
	pcaly_line2_coh_clipped = mkinsertgap(pipeline, pcaly_line2_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	X2_unc = compute_calline_uncertainty(pipeline, pcaly_line2_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	X2_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, X2_unc, pipeparts.mkgeneric(pipeline, X2, "cabs")))

	complex_ktst = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatriximxer(pipeline, ktst, matrix = [[1.0, 0.0]]))
	A_tst = mkmultiplier(pipeline, list_srcs(pipeline, complex_ktst, AT_f2))
	if apply_complex_kappatst:
		i_omega_tau_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_tst, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_tst = pipeparts.mkgeneric(pipeline, i_omega_tau_tst, "cexp")
		A_tst = mkmultiplier(pipeline, list_srcs(pipeline, A_tst, exp_i_omega_tau_tst))
	A_tst_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, ktst, ktst_unc, AT_f2_mag))
	complex_kpum = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatriximxer(pipeline, kpum, matrix = [[1.0, 0.0]]))
	A_pum = mkmultiplier(pipeline, list_srcs(pipeline, complex_kpum, AP_f2))
	if apply_complex_kappapum:
		i_omega_tau_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_pum, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_pum = pipeparts.mkgeneric(pipeline, i_omega_tau_pum, "cexp")
		A_pum = mkmultiplier(pipeline, list_srcs(pipeline, A_pum, exp_i_omega_tau_pum))
	A_pum_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, kpum, kpum_unc, AP_f2_mag))
	complex_kuim = pipeparts.mktogglecomplex(pipeline, pipeparts.mkmatriximxer(pipeline, kuim, matrix = [[1.0, 0.0]]))
	A_uim = mkmultiplier(pipeline, list_srcs(pipeline, complex_kuim, AU_f2))
	if apply_complex_kappauim:
		i_omega_tau_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_uim, matrix = [[0, 2 * numpy.pi * opt_gain_fcc_line_freq]]))
		exp_i_omega_tau_uim = pipeparts.mkgeneric(pipeline, i_omega_tau_uim, "cexp")
		A_uim = mkmultiplier(pipeline, list_srcs(pipeline, A_uim, exp_i_omega_tau_uim))
	A_uim_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, kuim, kuim_unc, AU_f2_mag))
	A = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, A_tst, A_pum, A_uim)))
	A_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, A_tst_unc_abs, exponent = 2.0), mkpow(pipeline, A_pum_unc_abs, exponent = 2.0), mkpow(pipeline, A_uim_unc_abs, exponent = 2.0))), exponent = 0.5)
	minus_DA = complex_audioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, A, D_f2)), -1.0, 0.0)
	DA_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, A_unc_abs, D_f2_mag))
	X2_minus_DA = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, X2, minus_DA)))
	X2_minus_DA_mag = pipeparts.mkgeneric(pipeline, X2_minus_DA, "cabs")
	X2_minus_DA_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, X2_unc_abs, exponent = 2.0), mkpow(pipeline, DA_unc_abs, exponent = 2.0))), exponent = 0.5)
	S_c_unc = pipeparts.mktee(pipeline, complex_division(pipeline, X2_minus_DA_unc_abs, X2_minus_DA_mag))
	S_c = pipeparts.mktee(pipeline, complex_inverse(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, X2_minus_DA, C0_f2))))
	S_c_real = pipeparts.mkgeneric(pipeline, S_c, "creal")
	S_c_imag = pipeparts.mkgeneric(pipeline, S_c, "cimag")
	S_c_product = pipeparts.mkgeneric(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real, S_c_imag)), "cabs")
	S_c_square_modulus = pipeparts.mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "cabs"), exponent = 2.0)
	S_c_square_modulus_over_S_c_product = complex_division(pipeline, S_c_square_modulus, S_c_product)
	fcc_unc = mkmultiplier(pipeline, list_srcs(pipeline, S_c_square_modulus_over_S_c_product, S_c_unc))

	return S_c, S_c_unc, fcc_unc

def compute_SRC_uncertainty_from_filters_file(pipeline, C0_f1_real, C0_f1_imag, kc, kc_unc, fcc, fcc_unc, D_f1_real, D_f1_imag, act_pcal_line_freq, X1, pcaly_line1_coh, AT_f1_real, AT_f1_imag, ktst, tau_tst, apply_complex_kappatst, ktst_unc, AP_f1_real, AP_f1_imag, kpum, tau_pum, apply_complex_kappapum, kpum_unc, AU_f1_real, AU_f1_imag, kuim, tau_uim, apply_complex_kappauim, kuim_unc, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples, coherence_unc_threshold):

	#
	# S_s^{-1} = ((C0_f1 * kc) / (1 + i * f_src / f_cc)) * (pcalfpcal4 / derrfpcal4 - D_f1 * (ktst * AT_f1 + kpum * AP_f1 + kuim * AU_f1))
	#

	D_f1_mag = pow(D_f1_real * D_f1_real + D_f1_imag * D_f1_imag, 0.5)
	AT_f1_mag = pow(AT_f1_real * AT_f1_real + AT_f1_imag * AT_f1_imag, 0.5)
	AP_f1_mag = pow(AP_f1_real * AP_f1_real + AP_f1_imag * AP_f1_imag, 0.5)
	AU_f1_mag = pow(AU_f1_real * AU_f1_real + AU_f1_imag * AU_f1_imag, 0.5)

	X1 = pipeparts.mktee(pipeline, X1)
	fcc = pipeparts.mktee(pipeline, fcc)
	pcaly_line1_coh_clipped = mkinsertgap(pipeline, pcaly_line1_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	X1_unc = compute_calline_uncertainty(pipeline, pcaly_line1_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	X1_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, X1_unc, pipeparts.mkgeneric(pipeline, X1, "cabs")))

	A_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, ktst, matrix = [[AT_f1_real, AT_f1_imag]]))
	if apply_complex_kappatst:
		i_omega_tau_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_tst, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_tst = pipeparts.mkgeneric(pipeline, i_omega_tau_tst, "cexp")
		A_tst = mkmultiplier(pipeline, list_srcs(pipeline, A_tst, exp_i_omega_tau_tst))
	A_tst_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, ktst, ktst_unc)), AT_f1_mag)
	A_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kpum, matrix = [[AP_f1_real, AP_f1_imag]]))
	if apply_complex_kappapum:
		i_omega_tau_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_pum, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_pum = pipeparts.mkgeneric(pipeline, i_omega_tau_pum, "cexp")
		A_pum = mkmultiplier(pipeline, list_srcs(pipeline, A_pum, exp_i_omega_tau_pum))
	A_pum_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, kpum, kpum_unc)), AP_f1_mag)
	A_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kuim, matrix = [[AU_f1_real, AU_f1_imag]]))
	if apply_complex_kappauim:
		i_omega_tau_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_uim, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_uim = pipeparts.mkgeneric(pipeline, i_omega_tau_uim, "cexp")
		A_uim = mkmultiplier(pipeline, list_srcs(pipeline, A_uim, exp_i_omega_tau_uim))
	A_uim_unc_abs = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, kuim, kuim_unc)), AU_f1_mag)
	A = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, A_tst, A_pum, A_uim)))
	A_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, A_tst_unc_abs, exponent = 2.0), mkpow(pipeline, A_pum_unc_abs, exponent = 2.0), mkpow(pipeline, A_uim_unc_abs, exponent = 2.0))), exponent = 0.5)
	minus_DA = complex_audioamplify(pipeline, A, -D_f1_real, -D_f1_imag)
	DA_unc_abs = pipeparts.mkaudioamplify(pipeline, A_unc_abs, D_f1_mag)
	X1_minus_DA = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, X1, minus_DA)))
	X1_minus_DA_mag = pipeparts.mkgeneric(pipeline, X1_minus_DA, "cabs")
	X1_minus_DA_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, X1_unc_abs, exponent = 2.0), mkpow(pipeline, DA_unc_abs, exponent = 2.0))), exponent = 0.5)
	S_s_S_c_inverse_unc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, complex_division(pipeline, X1_minus_DA_unc_abs, X1_minus_DA_mag), exponent = 2.0))
	S_s_S_c_inverse = pipeparts.mktee(pipeline, complex_audioamplify(pipeline, X1_minus_DA, C0_f1_real, C0_f1_imag))
	S_s_S_c_inverse_real_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_s_S_c_inverse, "creal"), exponent = 2.0))
	S_s_S_c_inverse_imag_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_s_S_c_inverse, "cimag"), exponent = 2.0))

	complex_kc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kc, [[1.0, 0.0]]))
	f_over_fcc = complex_inverse(pipeline, pipeparts.mkaudioamplify(pipeline, fcc, 1.0 / act_pcal_line_freq))
	i_f_over_fcc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, f_over_fcc, [[0.0, 1.0]]))
	one_plus_i_f_over_fcc = pipeparts.mkgeneric(pipeline, i_f_over_fcc, "lal_add_constant", value = 1.0)
	S_c = pipeparts.mktee(pipeline, complex_division(pipeline, complex_kc, one_plus_i_f_over_fcc))
	S_c_real_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "creal"), exponent = 2.0))
	S_c_imag_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "cimag"), exponent = 2.0))

	S_s_inv = pipeparts.mktee(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_s_S_c_inverse, S_c)))
	pipeparts.mkfakesink(pipeline, S_s_inv)

	f_squared_over_fcc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkaudioamplify(pipeline, fcc, 1.0 / act_pcal_line_freq), exponent = -2.0))
	fcc_unc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, fcc_unc, exponent = 2.0))
	inv_denominator = mkpow(pipeline, pipeparts.mkgeneric(pipeline, f_squared_over_fcc_squared, "lal_add_constant", value = 1.0), exponent = -2.0)
	numerator = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, fcc_unc_squared, mkpow(pipeline, f_squared_over_fcc_squared, exponent = 2.0))), 2.0)
	S_c_unc_real_squared = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, kc_unc, exponent = 2.0), mkmultiplier(pipeline, list_srcs(pipeline, numerator, inv_denominator)))))
	S_c_unc_imag_squared = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, fcc_unc_squared)))
	S_s_inverse_real_unc_abs_squared = mkadder(pipeline, list_srcs(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real_squared, S_s_S_c_inverse_real_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, S_s_S_c_inverse_unc_squared)))), mkmultiplier(pipeline, list_srcs(pipeline, S_c_imag_squared, S_s_S_c_inverse_imag_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_imag_squared, S_s_S_c_inverse_unc_squared))))))
	S_s_inverse_imag_unc_abs_squared = mkadder(pipeline, list_srcs(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real_squared, S_s_S_c_inverse_imag_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, S_s_S_c_inverse_unc_squared)))), mkmultiplier(pipeline, list_srcs(pipeline, S_c_imag_squared, S_s_S_c_inverse_real_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_imag_squared, S_s_S_c_inverse_unc_squared))))))
	fs_squared_unc_abs = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, S_s_inverse_real_unc_abs_squared, exponent = 0.5), act_pcal_line_freq * act_pcal_line_freq)
	fs_over_Q_unc_abs = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, S_s_inverse_imag_unc_abs_squared, exponent = 0.5), act_pcal_line_freq)

	return S_s_inv, fs_squared_unc_abs, fs_over_Q_unc_abs

def compute_SRC_uncertainty(pipeline, C0_f1, kc, kc_unc, fcc, fcc_unc, D_f1, act_pcal_line_freq, X1, pcaly_line1_coh, AT_f1, ktst, tau_tst, apply_complex_kappatst, ktst_unc, AP_f1, kpum, tau_pum, apply_complex_kappapum, kpum_unc, AU_f1, kuim, tau_uim, apply_complex_kappauim, kuim_unc, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples, coherence_unc_threshold):

	#
	# S_s^{-1} = ((C0_f1 * kc) / (1 + i * f_src / f_cc)) * (pcalfpcal4 / derrfpcal4 - D_f1 * (ktst * AT_f1 + kpum * AP_f1 + kuim * AU_f1))
	#

	D_f1_mag = pipeparts.mkgeneric(pipeline, D_f1, "cabs")
	AT_f1_mag = pipeparts.mkgeneric(pipeline, AT_f1, "cabs")
	AP_f1_mag = pipeparts.mkgeneric(pipeline, AP_f1, "cabs")
	AU_f1_mag = pipeparts.mkgeneric(pipeline, AU_f1, "cabs")

	X1 = pipeparts.mktee(pipeline, X1)
	fcc = pipeparts.mktee(pipeline, fcc)
	pcaly_line1_coh_clipped = mkinsertgap(pipeline, pcaly_line1_coh, bad_data_intervals = [0, coherence_unc_threshold], replace_value = coherence_unc_threshold, insert_gap = False)
	X1_unc = compute_calline_uncertainty(pipeline, pcaly_line1_coh_clipped, coherence_samples, integration_samples, median_smoothing_samples, factors_average_samples)
	X1_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, X1_unc, pipeparts.mkgeneric(pipeline, X1, "cabs")))

	A_tst = mkmultiplier(pipeline, list_srcs(pipeline, pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, ktst, matrix = [[1.0, 0.0]])), AT_f1))
	if apply_complex_kappatst:
		i_omega_tau_tst = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_tst, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_tst = pipeparts.mkgeneric(pipeline, i_omega_tau_tst, "cexp")
		A_tst = mkmultiplier(pipeline, list_srcs(pipeline, A_tst, exp_i_omega_tau_tst))
	A_tst_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, ktst, ktst_unc, AT_f1_mag))
	A_pum = mkmultiplier(pipeline, list_srcs(pipeline, pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kpum, matrix = [[1.0, 0.0]])), AP_f1))
	if apply_complex_kappapum:
		i_omega_tau_pum = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_pum, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_pum = pipeparts.mkgeneric(pipeline, i_omega_tau_pum, "cexp")
		A_pum = mkmultiplier(pipeline, list_srcs(pipeline, A_pum, exp_i_omega_tau_pum))
	A_pum_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, kpum, kpum_unc, AP_f1_mag))
	A_uim = mkmultiplier(pipeline, list_srcs(pipeline, pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kuim, matrix = [[1.0, 0.0]])), AU_f1))
	if apply_complex_kappauim:
		i_omega_tau_uim = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, tau_uim, matrix = [[0, 2 * numpy.pi * act_pcal_line_freq]]))
		exp_i_omega_tau_uim = pipeparts.mkgeneric(pipeline, i_omega_tau_uim, "cexp")
		A_uim = mkmultiplier(pipeline, list_srcs(pipeline, A_uim, exp_i_omega_tau_uim))
	A_uim_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, kuim, kuim_unc, AU_f1_mag))
	A = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, A_tst, A_pum, A_uim)))
	A_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, A_tst_unc_abs, exponent = 2.0), mkpow(pipeline, A_pum_unc_abs, exponent = 2.0), mkpow(pipeline, A_uim_unc_abs, exponent = 2.0))), exponent = 0.5)
	minus_DA = complex_audioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, A, D_f1)), -1.0, 0.0)
	DA_unc_abs = mkmultiplier(pipeline, list_srcs(pipeline, A_unc_abs, D_f1_mag))
	X1_minus_DA = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, X1, minus_DA)))
	X1_minus_DA_mag = pipeparts.mkgeneric(pipeline, X1_minus_DA, "cabs")
	X1_minus_DA_unc_abs = mkpow(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, X1_unc_abs, exponent = 2.0), mkpow(pipeline, DA_unc_abs, exponent = 2.0))), exponent = 0.5)
	S_s_S_c_inverse_unc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, complex_division(pipeline, X1_minus_DA_unc_abs, X1_minus_DA_mag), exponent = 2.0))
	S_s_S_c_inverse = pipeparts.mktee(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, X1_minus_DA, C0_f1)))
	S_s_S_c_inverse_real_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_s_S_c_inverse, "creal"), exponent = 2.0))
	S_s_S_c_inverse_imag_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_s_S_c_inverse, "cimag"), exponent = 2.0))

	complex_kc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, kc, [[1.0, 0.0]]))
	f_over_fcc = complex_inverse(pipeline, pipeparts.mkaudioamplify(pipeline, fcc, 1.0 / act_pcal_line_freq))
	i_f_over_fcc = pipeparts.mktogglecomplex(pipeline, mkmatmix(pipeline, f_over_fcc, [[0.0, 1.0]]))
	one_plus_i_f_over_fcc = pipeparts.mkgeneric(pipeline, i_f_over_fcc, "lal_add_constant", value = 1.0)
	S_c = pipeparts.mktee(pipeline, complex_division(pipeline, complex_kc, one_plus_i_f_over_fcc))
	S_c_real_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "creal"), exponent = 2.0))
	S_c_imag_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkgeneric(pipeline, S_c, "cimag"), exponent = 2.0))

	S_s_inv = pipeparts.mktee(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_s_S_c_inverse, S_c)))
	pipeparts.mkfakesink(pipeline, S_s_inv)

	f_squared_over_fcc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, pipeparts.mkaudioamplify(pipeline, fcc, 1.0 / act_pcal_line_freq), exponent = -2.0))
	fcc_unc_squared = pipeparts.mktee(pipeline, mkpow(pipeline, fcc_unc, exponent = 2.0))
	inv_denominator = mkpow(pipeline, pipeparts.mkgeneric(pipeline, f_squared_over_fcc_squared, "lal_add_constant", value = 1.0), exponent = -2.0)
	numerator = pipeparts.mkaudioamplify(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, fcc_unc_squared, mkpow(pipeline, f_squared_over_fcc_squared, exponent = 2.0))), 2.0)
	S_c_unc_real_squared = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, mkpow(pipeline, kc_unc, exponent = 2.0), mkmultiplier(pipeline, list_srcs(pipeline, numerator, inv_denominator)))))
	S_c_unc_imag_squared = pipeparts.mktee(pipeline, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, fcc_unc_squared)))
	S_s_inverse_real_unc_abs_squared = mkadder(pipeline, list_srcs(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real_squared, S_s_S_c_inverse_real_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, S_s_S_c_inverse_unc_squared)))), mkmultiplier(pipeline, list_srcs(pipeline, S_c_imag_squared, S_s_S_c_inverse_imag_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_imag_squared, S_s_S_c_inverse_unc_squared))))))
	S_s_inverse_imag_unc_abs_squared = mkadder(pipeline, list_srcs(pipeline, mkmultiplier(pipeline, list_srcs(pipeline, S_c_real_squared, S_s_S_c_inverse_imag_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_real_squared, S_s_S_c_inverse_unc_squared)))), mkmultiplier(pipeline, list_srcs(pipeline, S_c_imag_squared, S_s_S_c_inverse_real_squared, mkadder(pipeline, list_srcs(pipeline, S_c_unc_imag_squared, S_s_S_c_inverse_unc_squared))))))
	fs_squared_unc_abs = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, S_s_inverse_real_unc_abs_squared, exponent = 0.5), act_pcal_line_freq * act_pcal_line_freq)
	fs_over_Q_unc_abs = pipeparts.mkaudioamplify(pipeline, mkpow(pipeline, S_s_inverse_imag_unc_abs_squared, exponent = 0.5), act_pcal_line_freq)

	return S_s_inv, fs_squared_unc_abs, fs_over_Q_unc_abs

def find_injection_ratios_from_model(filters, ktst = 1.0, tau_tst = 0.0, kpum = 1.0, tau_pum = 0.0, kuim = 1.0, tau_uim = 0.0, kc = 1.0, fcc = None, fs_squared = None, Qinv = None):
	#
	# Find the model-predicted X's of eqs. 5.2.22 and 5.2.23 in P1900052, given
	# a filters file and values for the TDCFs.  Useful mainly for testing.
	#

	f1 = float(filters['ka_pcal_line_freq'])
	f2 = float(filters['kc_pcal_line_freq'])
	fT = float(filters['ktst_esd_line_freq'])
	fP = float(filters['pum_act_line_freq'])
	fU = float(filters['uim_act_line_freq'])

	Cres1 = float(filters['EP11_real']) + 1j * float(filters['EP11_imag'])
	CresDAT1 = float(filters['EP25_real']) + 1j * float(filters['EP25_imag'])
	CresDAP1 = float(filters['EP26_real']) + 1j * float(filters['EP26_imag'])
	CresDAU1 = float(filters['EP27_real']) + 1j * float(filters['EP27_imag'])

	Cres2 = float(filters['EP6_real']) + 1j * float(filters['EP6_imag'])
	CresDAT2 = float(filters['EP28_real']) + 1j * float(filters['EP28_imag'])
	CresDAP2 = float(filters['EP29_real']) + 1j * float(filters['EP29_imag'])
	CresDAU2 = float(filters['EP30_real']) + 1j * float(filters['EP30_imag'])

	CresAT0T = float(filters['EP31_real']) + 1j * float(filters['EP31_imag'])
	CresDATT = float(filters['EP32_real']) + 1j * float(filters['EP32_imag'])
	CresDAPT = float(filters['EP33_real']) + 1j * float(filters['EP33_imag'])
	CresDAUT = float(filters['EP34_real']) + 1j * float(filters['EP34_imag'])

	CresAP0P = float(filters['EP35_real']) + 1j * float(filters['EP35_imag'])
	CresDATP = float(filters['EP36_real']) + 1j * float(filters['EP36_imag'])
	CresDAPP = float(filters['EP37_real']) + 1j * float(filters['EP37_imag'])
	CresDAUP = float(filters['EP38_real']) + 1j * float(filters['EP38_imag'])

	CresAU0U = float(filters['EP39_real']) + 1j * float(filters['EP39_imag'])
	CresDATU = float(filters['EP40_real']) + 1j * float(filters['EP40_imag'])
	CresDAPU = float(filters['EP41_real']) + 1j * float(filters['EP41_imag'])
	CresDAUU = float(filters['EP42_real']) + 1j * float(filters['EP42_imag'])

	fcc_model = float(filters['fcc'])
	if fcc is None:
		fcc = fcc_model
	fs_squared_model = float(filters['fs_squared'])
	if fs_squared is None:
		fs_squared = fs_squared_model
	Qinv_model = 1.0 / float(filters['srcQ'])
	if Qinv is None:
		Qinv = Qinv_model

	CresDAT1 *= ktst * numpy.exp(2.0 * numpy.pi * 1j * f1 * tau_tst)
	CresDAP1 *= kpum * numpy.exp(2.0 * numpy.pi * 1j * f1 * tau_pum)
	CresDAU1 *= kuim * numpy.exp(2.0 * numpy.pi * 1j * f1 * tau_uim)

	CresDAT2 *= ktst * numpy.exp(2.0 * numpy.pi * 1j * f2 * tau_tst)
	CresDAP2 *= kpum * numpy.exp(2.0 * numpy.pi * 1j * f2 * tau_pum)
	CresDAU2 *= kuim * numpy.exp(2.0 * numpy.pi * 1j * f2 * tau_uim)

	CresAT0T *= ktst * numpy.exp(2.0 * numpy.pi * 1j * fT * tau_tst)
	CresDATT *= ktst * numpy.exp(2.0 * numpy.pi * 1j * fT * tau_tst)
	CresDAPT *= kpum * numpy.exp(2.0 * numpy.pi * 1j * fT * tau_pum)
	CresDAUT *= kuim * numpy.exp(2.0 * numpy.pi * 1j * fT * tau_uim)

	CresAP0P *= kpum * numpy.exp(2.0 * numpy.pi * 1j * fP * tau_pum)
	CresDATP *= ktst * numpy.exp(2.0 * numpy.pi * 1j * fP * tau_tst)
	CresDAPP *= kpum * numpy.exp(2.0 * numpy.pi * 1j * fP * tau_pum)
	CresDAUP *= kuim * numpy.exp(2.0 * numpy.pi * 1j * fP * tau_uim)

	CresAU0U *= kuim * numpy.exp(2.0 * numpy.pi * 1j * fU * tau_uim)
	CresDATU *= ktst * numpy.exp(2.0 * numpy.pi * 1j * fU * tau_tst)
	CresDAPU *= kpum * numpy.exp(2.0 * numpy.pi * 1j * fU * tau_pum)
	CresDAUU *= kuim * numpy.exp(2.0 * numpy.pi * 1j * fU * tau_uim)

	CresX1 = CresDAT1 + CresDAP1 + CresDAU1 + ((1.0 + 1j * f1 / fcc) / kc) * ((f1 * f1 + fs_squared - 1j * f1 * numpy.sqrt(abs(fs_squared)) * Qinv) / (f1 * f1))
	CresX2 = CresDAT2 + CresDAP2 + CresDAU2 + ((1.0 + 1j * f2 / fcc) / kc) * ((f2 * f2 + fs_squared - 1j * f2 * numpy.sqrt(abs(fs_squared)) * Qinv) / (f2 * f2))
	CresAT0XT = CresDATT + CresDAPT + CresDAUT + ((1.0 + 1j * fT / fcc) / kc) * ((fT * fT + fs_squared - 1j * fT * numpy.sqrt(abs(fs_squared)) * Qinv) / (fT * fT))
	CresAP0XP = CresDATP + CresDAPP + CresDAUP + ((1.0 + 1j * fP / fcc) / kc) * ((fP * fP + fs_squared - 1j * fP * numpy.sqrt(abs(fs_squared)) * Qinv) / (fP * fP))
	CresAU0XU = CresDATU + CresDAPU + CresDAUU + ((1.0 + 1j * fU / fcc) / kc) * ((fU * fU + fs_squared - 1j * fU * numpy.sqrt(abs(fs_squared)) * Qinv) / (fU * fU))

	X1 = CresX1 / Cres1
	X2 = CresX2 / Cres2
	XT = CresAT0XT / CresAT0T
	XP = CresAP0XP / CresAP0P
	XU = CresAU0XU / CresAU0U

	return X1, X2, XT, XP, XU

def update_property_simple(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):
	prop = prop_maker.get_property(maker_prop_name)
	prop_taker.set_property(taker_prop_name, prefactor * prop)

def update_timestamped_property(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):

	binding = prop_taker.get_control_binding(taker_prop_name)
	if(binding is not None):
		cs = binding.get_property('control_source')
	else:
		cs = GstController.InterpolationControlSource.new()
		cs.set_property('mode', GstController.InterpolationMode.NONE) # no interpolation
		binding = GstController.DirectControlBinding.new_absolute(prop_taker, taker_prop_name, cs)
		prop_taker.add_control_binding(binding)

	prop = prop_maker.get_property(maker_prop_name)
	cs.set(int(prop[0] * Gst.SECOND), prefactor * prop[1])

def update_SS_demod_freqs(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):

	SS_freqs = [5.619, 6.0565, 6.4972, 6.7742, 7.0544, 7.475, 7.773, 8.002, 8.309, 8.791, 9.103, 10.334, 11.131, 11.709, 12.234, 15.6, 16.4, 17.1, 17.6, 19.1, 23.248, 25.751, 28.519, 32.758, 38.886, 46.16, 54.794, 65.043, 77.209, 88.939, 95.201, 108.795, 121.102, 139.447, 153.302, 182.997, 216.016, 305.066, 341.623, 401.433, 410.3, 428.909, 433.7, 610.055, 851.613, 1083.7]

	binding = prop_taker.get_control_binding(taker_prop_name)
	if(binding is not None):
		cs = binding.get_property('control_source')
	else:
		cs = GstController.InterpolationControlSource.new()
		cs.set_property('mode', GstController.InterpolationMode.NONE) # no interpolation
		binding = GstController.DirectControlBinding.new_absolute(prop_taker, taker_prop_name, cs)
		prop_taker.add_control_binding(binding)

	prop = prop_maker.get_property(maker_prop_name)
	freq = 0.0
	if float(prop[0]) > 1268420404 and float(prop[0]) < 1268421104:
		for f in SS_freqs:
			if abs(float(prop[1]) - f) < 0.1:
				freq = f

	cs.set(int(prop[0] * Gst.SECOND), prefactor * freq)

def update_SS_demod_freqs_T(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):

	SS_freqs = [6.4313,7.,7.6529,8.3666,10.,12.1153,14.678 ,15.6,16.4,17.1,17.6,19.1,21.5443,26.1016,31.6228,38.3119,46.4159,56.2341,68.1292,82.5404,100.,134.224 ,177.828 ,237.137 ,292.211 ,340.654 ,433.7,490.327 ,543.213 ,625.742 ,699.465 ,749.894 ,824.265 ,962.764 ,1221.57]

	binding = prop_taker.get_control_binding(taker_prop_name)
	if(binding is not None):
		cs = binding.get_property('control_source')
	else:
		cs = GstController.InterpolationControlSource.new()
		cs.set_property('mode', GstController.InterpolationMode.NONE) # no interpolation
		binding = GstController.DirectControlBinding.new_absolute(prop_taker, taker_prop_name, cs)
		prop_taker.add_control_binding(binding)

	prop = prop_maker.get_property(maker_prop_name)
	freq = 0.0
	if float(prop[0]) > 1268424147 and float(prop[0]) < 1268424867:
		for f in SS_freqs:
			if abs(float(prop[1]) - f) < 0.1:
				freq = f

	cs.set(int(prop[0] * Gst.SECOND), prefactor * freq)

def update_SS_demod_freqs_P(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):

	SS_freqs = [6.4313,7.,7.6529,8.3666,10.,12.1153,14.678 ,15.6,16.4,17.1,17.6,19.1,21.5443,26.1016,31.6228,38.3119,46.4159,56.2341,68.1292,82.5404,100.,134.224 ,177.828 ,237.137 ,292.211 ,340.654 ,433.7,490.327 ,543.213 ,625.742 ,699.465 ,749.894 ,824.265 ,962.764 ,1221.57]

	binding = prop_taker.get_control_binding(taker_prop_name)
	if(binding is not None):
		cs = binding.get_property('control_source')
	else:
		cs = GstController.InterpolationControlSource.new()
		cs.set_property('mode', GstController.InterpolationMode.NONE) # no interpolation
		binding = GstController.DirectControlBinding.new_absolute(prop_taker, taker_prop_name, cs)
		prop_taker.add_control_binding(binding)

	prop = prop_maker.get_property(maker_prop_name)
	freq = 0.0
	if float(prop[0]) > 1268423005 and float(prop[0]) < 1268423725:
		for f in SS_freqs:
			if abs(float(prop[1]) - f) < 0.1:
				freq = f

	cs.set(int(prop[0] * Gst.SECOND), prefactor * freq)

def update_SS_demod_freqs_U(prop_maker, arg, prop_taker, maker_prop_name, taker_prop_name, prefactor):

	SS_freqs = [6.4313,7.,7.6529,8.3666,10.,12.1153,14.678 ,15.6,16.4,17.1,17.6,19.1,21.5443,26.1016,31.6228,38.3119,46.4159,56.2341,68.1292,82.5404,100.,134.224 ,177.828 ,237.137 ,292.211 ,340.654 ,433.7,490.327 ,543.213 ,625.742 ,699.465 ,749.894 ,824.265 ,962.764 ,1221.57]

	binding = prop_taker.get_control_binding(taker_prop_name)
	if(binding is not None):
		cs = binding.get_property('control_source')
	else:
		cs = GstController.InterpolationControlSource.new()
		cs.set_property('mode', GstController.InterpolationMode.NONE) # no interpolation
		binding = GstController.DirectControlBinding.new_absolute(prop_taker, taker_prop_name, cs)
		prop_taker.add_control_binding(binding)

	prop = prop_maker.get_property(maker_prop_name)
	freq = 0.0
	if float(prop[0]) > 1268421989 and float(prop[0]) < 1268422469:
		for f in SS_freqs:
			if abs(float(prop[1]) - f) < 0.1:
				freq = f

	cs.set(int(prop[0] * Gst.SECOND), prefactor * freq)

def update_filter(filter_maker, arg, filter_taker, maker_prop_name, taker_prop_name):
	firfilter = filter_maker.get_property(maker_prop_name)[::-1]
	filter_taker.set_property(taker_prop_name, firfilter)

def update_filters(filter_maker, arg, filter_taker, maker_prop_name, taker_prop_name, filter_number):
	firfilter = filter_maker.get_property(maker_prop_name)[filter_number][::-1]
	filter_taker.set_property(taker_prop_name, firfilter)

def update_exact_kappas(prop_maker, arg, freq_channels, EPICS_channels, receivers, freqs0, EPICS0):

	this_call_time = update_exact_kappas.call_time = time.time()
	time.sleep(1)
	if this_call_time != update_exact_kappas.call_time:
		return
	# Get the current frequencies and EPICS values
	freqs = []
	for chan in freq_channels:
		freqs.append(float(chan.get_property("current_average")))
		if numpy.isinf(freqs[-1]) or numpy.isnan(freqs[-1]) or abs(freqs[-1]) > 1e300 or abs(freqs[-1]) < 1e-300:
			freqs[-1] = freqs0[len(freqs) - 1]
	EPICS = []
	for chan in EPICS_channels:
		EPICS.append(float(chan.get_property("current_average")))
		if numpy.isinf(EPICS[-1]) or numpy.isnan(EPICS[-1]) or abs(EPICS[-1]) > 1e300 or abs(EPICS[-1]) < 1e-300:
			EPICS[-1] = EPICS0[len(EPICS) - 1]

	# Find number of calibration lines and actuation stages
	num_lines = len(freq_channels)
	num_stages = num_lines - 2 # Stages of actuation (currently 3)

	# What is the timestamp of this property?
	#update_time = int(prop_maker.get_property("timestamped_average")[0] * Gst.SECOND)

	# Follow the same(ish) procedure as compute_exact_kappas()
	idx = 0
	for i in range(num_lines):
		matrix = Gst.ValueArray([Gst.ValueArray([EPICS[2 * (1 + num_stages) * i], EPICS[2 * (1 + num_stages) * i + 1]]), Gst.ValueArray([-EPICS[2 * (1 + num_stages) * i + 1], EPICS[2 * (1 + num_stages) * i]])])
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "matrix", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set_value_array(update_time, matrix)
		receiver.set_property("matrix", matrix)

	# Let's start by computing the V's of Eqs. 5.2.78 and 5.2.79
	for j in range(num_stages):
		factor1 = pow(freqs[0], -2) - pow(freqs[2 + j], -2)
		factor2 = pow(freqs[2 + j], -2) - pow(freqs[1], -2)
		factor3 = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2))
		factor4 = freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2))
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor1)
		receiver.set_property("amplification", factor1)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor2)
		receiver.set_property("amplification", factor2)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor3)
		receiver.set_property("amplification", factor3)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor4)
		receiver.set_property("amplification", factor4)

	# Now let's compute the elements of the matrix M, given by Eqs. 5.2.70 - 5.2.77
	# Many of the elements are constant, so make a stream of ones to multiply
	for j in range(num_stages):
		# Time-dependent matrix elements
		factor = pow(freqs[0], -2) - pow(freqs[1], -2)
		addend = (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[2 * ((1 + num_stages) + 1 + j)] + (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[2 * (1 + j)] - (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor)
		receiver.set_property("amplification", factor)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "value", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, addend)
		receiver.set_property("value", addend)

		factor = -2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2))
		addend = -2.0 * numpy.pi * freqs[1] * (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + j)] - 2.0 * numpy.pi * freqs[0] * (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * (1 + j)] + 2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor)
		receiver.set_property("amplification", factor)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "value", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, addend)
		receiver.set_property("value", addend)

		factor = -freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2))
		addend = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + j)] + freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[1 + 2 * (1 + j)] + freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor)
		receiver.set_property("amplification", factor)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "value", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, addend)
		receiver.set_property("value", addend)

		factor = -2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2))
		addend = 2.0 * numpy.pi * pow(freqs[1], 2) * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[2 * ((1 + num_stages) + 1 + j)] + 2.0 * numpy.pi * pow(freqs[0], 2) * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[2 * (1 + j)] + 2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + j)]
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, factor)
		receiver.set_property("amplification", factor)
		receiver = receivers[idx]
		idx += 1
		#cs = GstController.InterpolationControlSource.new()
		#binding = GstController.DirectControlBinding.new_absolute(receiver, "value", cs)
		#receiver.add_control_binding(binding)
		#cs.set_property('mode', GstController.InterpolationMode.NONE)
		#cs.set(update_time, addend)
		receiver.set_property("value", addend)

		# Constant matrix elements
		knotequalj = list(numpy.arange(num_stages))
		knotequalj.remove(j)
		for k in knotequalj:
			factor = (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[2 * ((1 + num_stages) + 1 + k)] + (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[2 * (1 + k)] - (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, factor)
			receiver.set_property("amplification", factor)
			factor = -2.0 * numpy.pi * freqs[1] * (pow(freqs[0], -2) - pow(freqs[2 + j], -2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + k)] - 2.0 * numpy.pi * freqs[0] * (pow(freqs[2 + j], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * (1 + k)] + 2.0 * numpy.pi * freqs[2 + j] * (pow(freqs[0], -2) - pow(freqs[1], -2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, factor)
			receiver.set_property("amplification", factor)
			factor = freqs[1] * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[1 + 2 * ((1 + num_stages) + 1 + k)] + freqs[0] * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[1 + 2 * (1 + k)] + freqs[2 + j] * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[1 + 2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, factor)
			receiver.set_property("amplification", factor)
			factor = 2.0 * numpy.pi * pow(freqs[1], 2) * (pow(freqs[0], 2) - pow(freqs[2 + j], 2)) * EPICS[2 * ((1 + num_stages) + 1 + k)] + 2.0 * numpy.pi * pow(freqs[0], 2) * (pow(freqs[2 + j], 2) - pow(freqs[1], 2)) * EPICS[2 * (1 + k)] + 2.0 * numpy.pi * pow(freqs[2 + j], 2) * (pow(freqs[1], 2) - pow(freqs[0], 2)) * EPICS[2 * ((2 + j) * (1 + num_stages) + 1 + k)]
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "amplification", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, factor)
			receiver.set_property("amplification", factor)

	# Now pass these to the matrix solver to find kappa_T, kappa_P, kappa_U, tau_T, tau_P, and tau_U.
	# (Not necessary in this function...)

	# Next, compute the sensing function time dependence.
	# Start by computing G_res at each frequency, defined in Eq. 5.2.30
	for n in range(2):
		for j in range(num_stages):
			matrix = Gst.ValueArray([Gst.ValueArray([-EPICS[2 * (n * (1 + num_stages) + 1 + j)], -EPICS[1 + 2 * (n * (1 + num_stages) + 1 + j)]])])
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "matrix", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, matrix)
			receiver.set_property("matrix", matrix)
			matrix = Gst.ValueArray([Gst.ValueArray([0, 2.0 * numpy.pi * freqs[n]])])
			receiver = receivers[idx]
			idx += 1
			#cs = GstController.InterpolationControlSource.new()
			#binding = GstController.DirectControlBinding.new_absolute(receiver, "matrix", cs)
			#receiver.add_control_binding(binding)
			#cs.set_property('mode', GstController.InterpolationMode.NONE)
			#cs.set(update_time, matrix)
			receiver.set_property("matrix", matrix)

	receiver = receivers[idx]
	idx += 1
	#cs = GstController.InterpolationControlSource.new()
	#binding = GstController.DirectControlBinding.new_absolute(receiver, "freq1", cs)
	#receiver.add_control_binding(binding)
	#cs.set_property('mode', GstController.InterpolationMode.NONE)
	#cs.set(update_time, freqs[0])
	receiver.set_property("freq2", freqs[1])
	#cs = GstController.InterpolationControlSource.new()
	#binding = GstController.DirectControlBinding.new_absolute(receiver, "freq2", cs)
	#receiver.add_control_binding(binding)
	#cs.set_property('mode', GstController.InterpolationMode.NONE)
	#cs.set(update_time, freqs[1])
	receiver.set_property("freq2", freqs[1])

update_exact_kappas.call_time = 0

def clean_data(pipeline, signal, signal_rate, witnesses, witness_rate, fft_length, fft_overlap, num_ffts, min_ffts, update_samples, fir_length, frequency_resolution, filter_taper_length, use_median = False, parallel_mode = False, notch_frequencies = [], high_pass = 15.0, noisesub_gate_bit = None, delay_time = 0.0, critical_lock_loss_time = 0, fft_window_type = 'dpss', fir_window_type = 'dpss', filename = None):

	#
	# Use witness channels that monitor the environment to remove environmental noise
	# from a signal of interest.  This function accounts for potential correlation
	# between witness channels.
	#

	signal_tee = pipeparts.mktee(pipeline, signal)
	witnesses = list(witnesses)
	witness_tees = []
	for i in range(len(witnesses)):
		witnesses[i] = mkresample(pipeline, witnesses[i], 4, False, witness_rate)
		witness_tees.append(pipeparts.mktee(pipeline, witnesses[i]))

	resampled_signal = mkresample(pipeline, signal_tee, 4, False, witness_rate)
	transfer_functions = mkinterleave(pipeline, numpy.insert(witness_tees, 0, resampled_signal, axis = 0))
	if noisesub_gate_bit is not None:
		transfer_functions = mkgate(pipeline, transfer_functions, noisesub_gate_bit, 1)
	transfer_functions = mktransferfunction(pipeline, transfer_functions, fft_length = fft_length, fft_overlap = fft_overlap, num_ffts = num_ffts, min_ffts = min_ffts, update_samples = update_samples, make_fir_filters = -1, fir_length = fir_length, frequency_resolution = frequency_resolution, high_pass = high_pass / 2.0, update_after_gap = True, use_median = use_median, parallel_mode = parallel_mode, notch_frequencies = notch_frequencies, use_first_after_gap = critical_lock_loss_time * witness_rate, update_delay_samples = int(delay_time * witness_rate), fir_timeshift = 0, fft_window_type = fft_window_type, fir_window_type = fir_window_type, filename = filename)
	signal_minus_noise = [signal_tee]
	for i in range(len(witnesses)):
		if parallel_mode:
			minus_noise = mktdepfirfilt(pipeline, mkqueue(pipeline, highpass(pipeline, witness_tees[i], witness_rate, fcut = high_pass, freq_res = high_pass / 3.0)), kernel = numpy.zeros(fir_length), latency = fir_length // 2, taper_length = filter_taper_length, kernel_endtime = 0)
			transfer_functions.connect("notify::fir-filters", update_filters, minus_noise, "fir_filters", "kernel", i)
			transfer_functions.connect("notify::fir-endtime", update_property_simple, minus_noise, "fir_endtime", "kernel_endtime", 1)
		else:
			minus_noise = mktdepfirfilt(pipeline, highpass(pipeline, witness_tees[i], witness_rate, fcut = high_pass, freq_res = high_pass / 3.0), kernel = numpy.zeros(fir_length), latency = fir_length // 2, taper_length = filter_taper_length)
			transfer_functions.connect("notify::fir-filters", update_filters, minus_noise, "fir_filters", "kernel", i)
		signal_minus_noise.append(mkresample(pipeline, minus_noise, 4, False, signal_rate))

	return mkadder(pipeline, tuple(signal_minus_noise))

def update_calibcorr(tf_src, arg, tf_dst, tf_src_name, tf_dst_name, freqs, corr):
	tf = tf_src.get_property(tf_src_name)[0]
	df = freqs[1] - freqs[0]
	f0 = freqs[0] % df
	sparse_tf = []
	complex_corr = []
	for i in range(len(freqs)):
		index = 2 * int(round((freqs[i] - f0) / df))
		sparse_tf.append(tf[index] + 1j * tf[index + 1])
		complex_corr.append(corr[2 * i] + 1j * corr[2 * i + 1])
	sparse_tf = numpy.array(complex_corr) * numpy.array(sparse_tf)
	tf = []
	for i in range(len(sparse_tf)):
		tf.append(float(numpy.real(sparse_tf[i])))
		tf.append(float(numpy.imag(sparse_tf[i])))
	tf_dst.set_property(tf_dst_name, tf)

def remove_systematic_error(pipeline, res, res_rate, tst, tst_rate, pum, pum_rate, uim, uim_rate, derr, pcal, pcal_rate, pcal_dq, tstexc, tstexc_rate, tstexc_dq, pumexc, pumexc_rate, pumexc_dq, uimexc, uimexc_rate, uimexc_dq, TDCF_rate, ktst, tau_tst, kpum, tau_pum, kuim, tau_uim, kc, fcc, fs_squared, fs_over_Q, filters, filter_time, filename = None, use_median = False):
	cres = filters['cres_calibcorr']
	D = filters['D_calibcorr']
	tst_model = filters['tst_calibcorr']
	Ftst = filters['Ftst_calibcorr']
	pum_model = filters['pum_calibcorr']
	Fpum = filters['Fpum_calibcorr']
	uim_model = filters['uim_calibcorr']
	Fuim = filters['Fuim_calibcorr']
	pcal_corr = filters['pcal_corr_calibcorr']
	daqdownsampling_tst = filters['daqdownsampling_tst_calibcorr']
	daqdownsampling_pum = filters['daqdownsampling_pum_calibcorr']
	daqdownsampling_uim = filters['daqdownsampling_uim_calibcorr']

	# Find fft lengths and parameters to set frequency resolutions.
	cinv_freqs = pcal_corr[0]
	tst_freqs = daqdownsampling_tst[0]
	pum_freqs = daqdownsampling_pum[0]
	uim_freqs = daqdownsampling_uim[0]
	all_freqs = cres[0]

	# First try an integer number of seconds
	pcal_fft_dur = smallest_len = 16
	pcal_frac = float(cinv_freqs[0]) % 1
	smallest_decimal = decimal = (pcal_frac * pcal_fft_dur) % 1
	while smallest_decimal > 1e-10 and pcal_fft_dur < 128:
		pcal_fft_dur += 1
		decimal = (pcal_frac * pcal_fft_dur) % 1
		if 1 - decimal < decimal:
			decimal = 1 - decimal
		if decimal < smallest_decimal:
			smallest_decimal = decimal
			smallest_len = pcal_fft_dur
	pcal_fft_samples = pcal_fft_dur * pcal_rate
	# If that didn't work, try fractional numbers of seconds
	if smallest_decimal > 1e-10:
		pcal_fft_dur = 16
		smallest_decimal = decimal = (pcal_frac * pcal_fft_dur) % 1
		pcal_fft_samples = smallest_samples = 16 * pcal_rate
		while smallest_decimal > 1e-10 and pcal_fft_dur < 128:
			pcal_fft_samples += 1
			pcal_fft_dur = float(pcal_fft_samples) / pcal_rate
			decimal = (pcal_frac * pcal_fft_dur) % 1
			if 1 - decimal < decimal:
				decimal = 1 - decimal
			if decimal < smallest_decimal:
				smallest_decimal = decimal
				smallest_samples = pcal_fft_samples
		pcal_fft_samples = smallest_samples
		pcal_fft_dur = float(pcal_fft_samples) / pcal_rate

	# First try an integer number of seconds
	tstexc_fft_dur = smallest_len = 16
	tstexc_frac = float(tst_freqs[0]) % 1
	smallest_decimal = decimal = (tstexc_frac * tstexc_fft_dur) % 1
	while smallest_decimal > 1e-10 and tstexc_fft_dur < 128:
		tstexc_fft_dur += 1
		decimal = (tstexc_frac * tstexc_fft_dur) % 1
		if 1 - decimal < decimal:
			decimal = 1 - decimal
		if decimal < smallest_decimal:
			smallest_decimal = decimal
			smallest_len = tstexc_fft_dur
	tstexc_fft_samples = tstexc_fft_dur * tstexc_rate
	# If that didn't work, try fractional numbers of seconds
	if smallest_decimal > 1e-10:
		tstexc_fft_dur = 16
		smallest_decimal = decimal = (tstexc_frac * tstexc_fft_dur) % 1
		tstexc_fft_samples = smallest_samples = 16 * tstexc_rate
		while smallest_decimal > 1e-10 and tstexc_fft_dur < 128:
			tstexc_fft_samples += 1
			tstexc_fft_dur = float(tstexc_fft_samples) / tstexc_rate
			decimal = (tstexc_frac * tstexc_fft_dur) % 1
			if 1 - decimal < decimal:
				decimal = 1 - decimal
			if decimal < smallest_decimal:
				smallest_decimal = decimal
				smallest_samples = tstexc_fft_samples
		tstexc_fft_samples = smallest_samples
		tstexc_fft_dur = float(tstexc_fft_samples) / tstexc_rate

	# First try an integer number of seconds
	pumexc_fft_dur = smallest_len = 16
	pumexc_frac = float(pum_freqs[0]) % 1
	smallest_decimal = decimal = (pumexc_frac * pumexc_fft_dur) % 1
	while smallest_decimal > 1e-10 and pumexc_fft_dur < 128:
		pumexc_fft_dur += 1
		decimal = (pumexc_frac * pumexc_fft_dur) % 1
		if 1 - decimal < decimal:
			decimal = 1 - decimal
		if decimal < smallest_decimal:
			smallest_decimal = decimal
			smallest_len = pumexc_fft_dur
	pumexc_fft_samples = pumexc_fft_dur * pumexc_rate
	# If that didn't work, try fractional numbers of seconds
	if smallest_decimal > 1e-10:
		pumexc_fft_dur = 16
		smallest_decimal = decimal = (pumexc_frac * pumexc_fft_dur) % 1
		pumexc_fft_samples = smallest_samples = 16 * pumexc_rate
		while smallest_decimal > 1e-10 and pumexc_fft_dur < 128:
			pumexc_fft_samples += 1
			pumexc_fft_dur = float(pumexc_fft_samples) / pumexc_rate
			decimal = (pumexc_frac * pumexc_fft_dur) % 1
			if 1 - decimal < decimal:
				decimal = 1 - decimal
			if decimal < smallest_decimal:
				smallest_decimal = decimal
				smallest_samples = pumexc_fft_samples
		pumexc_fft_samples = smallest_samples
		pumexc_fft_dur = float(pumexc_fft_samples) / pumexc_rate

	# First try an integer number of seconds
	uimexc_fft_dur = smallest_len = 16
	uimexc_frac = float(uim_freqs[0]) % 1
	smallest_decimal = decimal = (uimexc_frac * uimexc_fft_dur) % 1
	while smallest_decimal > 1e-10 and uimexc_fft_dur < 128:
		uimexc_fft_dur += 1
		decimal = (uimexc_frac * uimexc_fft_dur) % 1
		if 1 - decimal < decimal:
			decimal = 1 - decimal
		if decimal < smallest_decimal:
			smallest_decimal = decimal
			smallest_len = uimexc_fft_dur
	uimexc_fft_samples = uimexc_fft_dur * uimexc_rate
	# If that didn't work, try fractional numbers of seconds
	if smallest_decimal > 1e-10:
		uimexc_fft_dur = 16
		smallest_decimal = decimal = (uimexc_frac * uimexc_fft_dur) % 1
		uimexc_fft_samples = smallest_samples = 16 * uimexc_rate
		while smallest_decimal > 1e-10 and uimexc_fft_dur < 128:
			uimexc_fft_samples += 1
			uimexc_fft_dur = float(uimexc_fft_samples) / uimexc_rate
			decimal = (uimexc_frac * uimexc_fft_dur) % 1
			if decimal < smallest_decimal:
				smallest_decimal = decimal
				smallest_samples = uimexc_fft_samples
		uimexc_fft_samples = smallest_samples
		uimexc_fft_dur = float(uimexc_fft_samples) / uimexc_rate

	# Ensure that all fft lengths are within a factor of 1.5 of being the same.
	max_fft_dur = max(pcal_fft_dur, tstexc_fft_dur, pumexc_fft_dur, uimexc_fft_dur)
	if float(max_fft_dur) / pcal_fft_dur > 1.5:
		pcal_fft_samples *= 2
		pcal_fft_dur = float(pcal_fft_samples) / pcal_rate
	if float(max_fft_dur) / tstexc_fft_dur > 1.5:
		tstexc_fft_samples *= 2
		tstexc_fft_dur = float(tstexc_fft_samples) / tstexc_rate
	if float(max_fft_dur) / pumexc_fft_dur > 1.5:
		pumexc_fft_samples *= 2
		pumexc_fft_dur = float(pumexc_fft_samples) / pumexc_rate
	if float(max_fft_dur) / uimexc_fft_dur > 1.5:
		uimexc_fft_samples *= 2
		uimexc_fft_dur = float(uimexc_fft_samples) / uimexc_rate

	# Set the frequency resolution of all the ffts.
	frequency_resolution = 4.0 / min(pcal_fft_dur, tstexc_fft_dur, pumexc_fft_dur, uimexc_fft_dur)
	for i in range(len(all_freqs) - 1):
		if all_freqs[i + 1] - all_freqs[i] < 4 * frequency_resolution:
			frequency_resolution = (all_freqs[i + 1] - all_freqs[i]) / 4.0
	# Set a limit so that the ffts don't get much longer than 256 seconds.
	if frequency_resolution < 3.0 / 256:
		frequency_resolution = 3.01 / 256

	# Finalize fft lengths and overlaps
	while pcal_fft_dur * frequency_resolution < 3.0:
		pcal_fft_samples *= 2
		pcal_fft_dur = float(pcal_fft_samples) / pcal_rate
	if frequency_resolution * pcal_fft_dur > 11:
		pcal_fft_over = 7 * pcal_fft_samples // 8
	else:
		pcal_fft_over = 3 * pcal_fft_samples // 4
	while tstexc_fft_dur * frequency_resolution < 3.0:
		tstexc_fft_samples *= 2
		tstexc_fft_dur = float(tstexc_fft_samples) / tstexc_rate
	if frequency_resolution * tstexc_fft_dur > 11:
		tstexc_fft_over = 7 * tstexc_fft_samples // 8
	else:
		tstexc_fft_over = 3 * tstexc_fft_samples // 4
	while pumexc_fft_dur * frequency_resolution < 3.0:
		pumexc_fft_samples *= 2
		pumexc_fft_dur = float(pumexc_fft_samples) / pumexc_rate
	if frequency_resolution * pumexc_fft_dur > 11:
		pumexc_fft_over = 7 * pumexc_fft_samples // 8
	else:
		pumexc_fft_over = 3 * pumexc_fft_samples // 4
	while uimexc_fft_dur * frequency_resolution < 3.0:
		uimexc_fft_samples *= 2
		uimexc_fft_dur = float(uimexc_fft_samples) / uimexc_rate
	if frequency_resolution * uimexc_fft_dur > 11:
		uimexc_fft_over = 7 * uimexc_fft_samples // 8
	else:
		uimexc_fft_over = 3 * uimexc_fft_samples // 4

	cres = numpy.ndarray.flatten(numpy.transpose(cres[1:]))
	D = numpy.ndarray.flatten(numpy.transpose(D[1:]))
	tst_model = numpy.ndarray.flatten(numpy.transpose(tst_model[1:]))
	Ftst = numpy.ndarray.flatten(numpy.transpose(Ftst[1:]))
	pum_model = numpy.ndarray.flatten(numpy.transpose(pum_model[1:]))
	Fpum = numpy.ndarray.flatten(numpy.transpose(Fpum[1:]))
	uim_model = numpy.ndarray.flatten(numpy.transpose(uim_model[1:]))
	Fuim = numpy.ndarray.flatten(numpy.transpose(Fuim[1:]))
	pcal_corr = numpy.ndarray.flatten(numpy.transpose(pcal_corr[1:]))
	daqdownsampling_tst = numpy.ndarray.flatten(numpy.transpose(daqdownsampling_tst[1:]))
	daqdownsampling_pum = numpy.ndarray.flatten(numpy.transpose(daqdownsampling_pum[1:]))
	daqdownsampling_uim = numpy.ndarray.flatten(numpy.transpose(daqdownsampling_uim[1:]))

	derr = pipeparts.mktee(pipeline, derr)

	# If TDCFs are not provided, use default values from filters. Start by making ones.
	ones = mkpow(pipeline, derr, exponent = 0)
	ones = mkresample(pipeline, ones, 0, False, TDCF_rate)
	ones = pipeparts.mktee(pipeline, ones)
	# In case we don't use the ones
	pipeparts.mkfakesink(pipeline, ones)
	if ktst is None:
		ktst = ones
	if tau_tst is None:
		tau_tst = pipeparts.mkaudioamplify(pipeline, ones, 0.0)
	if kpum is None:
		kpum = ones
	if tau_pum is None:
		tau_pum = pipeparts.mkaudioamplify(pipeline, ones, 0.0)
	if kuim is None:
		kuim = ones
	if tau_uim is None:
		tau_uim = pipeparts.mkaudioamplify(pipeline, ones, 0.0)
	if kc is None:
		kc = ones
	if fcc is None:
		fcc = pipeparts.mkaudioamplify(pipeline, ones, float(filters['fcc']))
	if fs_squared is None:
		fs_squared = pipeparts.mkaudioamplify(pipeline, ones, float(filters['fs_squared']))
	if fs_over_Q is None:
		srcQ = float(filters['srcQ'])
		if float(filters['fs_squared']) < 0:
			srcQ = -1j * srcQ
		fs_over_Q = numpy.real(numpy.sqrt(complex(float(filters['fs_squared']))) / srcQ)
		fs_over_Q = pipeparts.mkaudioamplify(pipeline, ones, fs_over_Q)

	measured_response = mkinterleave(pipeline, [pcal, mkresample(pipeline, derr, 4, False, pcal_rate)])
	measured_response = mkgate(pipeline, measured_response, pcal_dq, 1)
	measured_response = mktransferfunction(pipeline, measured_response, fft_length = pcal_fft_samples, fft_overlap = pcal_fft_over, tf_on_nongap = True, use_median = use_median, df = cinv_freqs[1] - cinv_freqs[0], f0 = cinv_freqs[0], fft_window_type = 0, frequency_resolution = frequency_resolution)

	tst_tf = mkinterleave(pipeline, [mkresample(pipeline, derr, 4, False, tstexc_rate), tstexc])
	tst_tf = mkgate(pipeline, tst_tf, tstexc_dq, 1)
	tst_tf = mktransferfunction(pipeline, tst_tf, fft_length = tstexc_fft_samples, fft_overlap = tstexc_fft_over, tf_on_nongap = True, use_median = use_median, df = tst_freqs[1] - tst_freqs[0], f0 = tst_freqs[0], fft_window_type = 0, frequency_resolution = frequency_resolution)

	pum_tf = mkinterleave(pipeline, [mkresample(pipeline, derr, 4, False, pumexc_rate), pumexc])
	pum_tf = mkgate(pipeline, pum_tf, pumexc_dq, 1)
	pum_tf = mktransferfunction(pipeline, pum_tf, fft_length = pumexc_fft_samples, fft_overlap = pumexc_fft_over, tf_on_nongap = True, use_median = use_median, df = pum_freqs[1] - pum_freqs[0], f0 = pum_freqs[0], fft_window_type = 0, frequency_resolution = frequency_resolution)

	uim_tf = mkinterleave(pipeline, [mkresample(pipeline, derr, 4, False, uimexc_rate), uimexc])
	uim_tf = mkgate(pipeline, uim_tf, uimexc_dq, 1)
	uim_tf = mktransferfunction(pipeline, uim_tf, fft_length = uimexc_fft_samples, fft_overlap = uimexc_fft_over, tf_on_nongap = True, use_median = use_median, df = uim_freqs[1] - uim_freqs[0], f0 = uim_freqs[0], fft_window_type = 0, frequency_resolution = frequency_resolution)

	TDCFs = mkinterleave(pipeline, [ktst, tau_tst, kpum, tau_pum, kuim, tau_uim, kc, fcc, fs_squared, fs_over_Q])

	cinv_corr_length = int(round(res_rate * filter_time))
	tst_corr_length = int(round(tst_rate * filter_time))
	pum_corr_length = int(round(pum_rate * filter_time))
	uim_corr_length = int(round(uim_rate * filter_time))

	calibcorr = mkcalibcorr(pipeline, TDCFs, cinv_freqs = cinv_freqs, tst_freqs = tst_freqs, pum_freqs = pum_freqs, uim_freqs = uim_freqs, cinv_fir_length = cinv_corr_length, tst_fir_length = tst_corr_length, pum_fir_length = pum_corr_length, uim_fir_length = uim_corr_length, cres_model = cres, D = D, tst_model = tst_model, Ftst_model = Ftst, pum_model = pum_model, Fpum_model = Fpum, uim_model = uim_model, Fuim_model = Fuim, extrapolate_method = 1, filename = filename)

	measured_response.connect("notify::transfer-functions", update_calibcorr, calibcorr, "transfer_functions", "measured_response", cinv_freqs, pcal_corr)
	tst_tf.connect("notify::transfer-functions", update_calibcorr, calibcorr, "transfer_functions", "tst_tf", tst_freqs, daqdownsampling_tst)
	pum_tf.connect("notify::transfer-functions", update_calibcorr, calibcorr, "transfer_functions", "pum_tf", pum_freqs, daqdownsampling_pum)
	uim_tf.connect("notify::transfer-functions", update_calibcorr, calibcorr, "transfer_functions", "uim_tf", uim_freqs, daqdownsampling_uim)

	cinv_corr0 = numpy.zeros(cinv_corr_length)
	cinv_corr0[cinv_corr_length // 2] = 1.0
	res = mktdepfirfilt(pipeline, res, kernel = cinv_corr0[::-1], latency = cinv_corr_length // 2, taper_length = 3 * cinv_corr_length)
	calibcorr.connect("notify::cinvcorr-filt", update_filter, res, "cinvcorr_filt", "kernel")

	tst_corr0 = numpy.zeros(tst_corr_length)
	tst_corr0[tst_corr_length // 2] = 1.0
	tst = mktdepfirfilt(pipeline, tst, kernel = tst_corr0[::-1], latency = tst_corr_length // 2, taper_length = 3 * tst_corr_length)
	calibcorr.connect("notify::tstcorr-filt", update_filter, tst, "tstcorr_filt", "kernel")

	pum_corr0 = numpy.zeros(pum_corr_length)
	pum_corr0[pum_corr_length // 2] = 1.0
	pum = mktdepfirfilt(pipeline, pum, kernel = pum_corr0[::-1], latency = pum_corr_length // 2, taper_length = 3 * pum_corr_length)
	calibcorr.connect("notify::pumcorr-filt", update_filter, pum, "pumcorr_filt", "kernel")

	uim_corr0 = numpy.zeros(uim_corr_length)
	uim_corr0[uim_corr_length // 2] = 1.0
	uim = mktdepfirfilt(pipeline, uim, kernel = uim_corr0[::-1], latency = uim_corr_length // 2, taper_length = 3 * uim_corr_length)
	calibcorr.connect("notify::uimcorr-filt", update_filter, uim, "uimcorr_filt", "kernel")

	return res, tst, pum, uim

def detect_secular_change(pipeline, head, secular_dur, stochastic_dur = 600, coherence_time = 60, rate = 16):

	head = mkresample(pipeline, head, 0, False, rate)
	head = pipeparts.mktee(pipeline, head)
	num = pipeparts.mkgeneric(pipeline, head, "lal_stdev", array_size = int(secular_dur / coherence_time), coherence_length = int(coherence_time * rate))
	num = mkpow(pipeline, num, exponent = 2.0)
	den = pipeparts.mkgeneric(pipeline, head, "lal_stdev", array_size = int(stochastic_dur / coherence_time), coherence_length = int(coherence_time * rate))
	den = mkpow(pipeline, den, exponent = 2.0)
	den = pipeparts.mkgeneric(pipeline, den, "lal_smoothkappas", array_size = 1, avg_array_size = secular_dur * rate, default_kappa_re = 1e-15)
	ratio = complex_division(pipeline, num, den)

	return mkinsertgap(pipeline, ratio, insert_gap = False, replace_value = 1e35)

def find_linefreq(pipeline, signal, rate_in, fft_time = 64, update_time = 16384, median_samples = 256):

	signal = highpass(pipeline, signal, rate_in, length = 0.5, fcut = 8, filter_latency = 0.5, freq_res = 4.0, td = True)
	signal = pipeparts.mktee(pipeline, signal)
	# First, compute an ASD to whiten the signal.
	# The median applied to the ASD will ensure that sharp spectral features (lines) are left in the signal.
	asd = pipeparts.mkgeneric(pipeline, signal, "lal_asd", fft_samples = fft_time * rate_in, overlap_samples = int(fft_time * rate_in * 3 // 4), update_time = update_time, median_samples = median_samples)
	filt0 = numpy.zeros(int(fft_time * rate_in // median_samples))
	filt0[int(fft_time * rate_in // median_samples // 2)] = 1.0
	signal = mkqueue(pipeline, signal)
	whitened_signal = mktdepfirfilt(pipeline, signal, kernel = filt0[::-1], latency = int(fft_time * rate_in // median_samples // 2), taper_length = int(fft_time * rate_in // median_samples * 2), kernel_endtime = 0)
	asd.connect("notify::whiten-filter", update_filter, whitened_signal, "whiten_filter", "kernel")
	asd.connect("notify::asd-endtime", update_property_simple, whitened_signal, "asd_endtime", "kernel_endtime", 1)

	# Now, track the frequency
	return pipeparts.mkgeneric(pipeline, whitened_signal, "lal_trackfrequency")


