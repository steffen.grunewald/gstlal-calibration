/*
 * Copyright (C) 2023 Aaron Viets
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <gst/audio/audio.h>
#include <cmath_base.h>

#define TYPE_CMATH_CONJ \
	(cmath_conj_get_type())
#define CMATH_CONJ(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj),TYPE_CMATH_CONJ,CMathConj))
#define CMATH_CONJ_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass),TYPE_CMATH_CONJ,CMathConjClass))
#define IS_PLUGIN_TEMPLATE(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj),TYPE_CMATH_CONJ))
#define IS_PLUGIN_TEMPLATE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass),TYPE_CMATH_CONJ))

typedef struct _CMathConj CMathConj;
typedef struct _CMathConjClass CMathConjClass;

GType
cmath_conj_get_type(void);

struct _CMathConj
{
	CMathBase cmath_base;
};

struct _CMathConjClass 
{
	CMathBaseClass parent_class;
};


/*
 * ============================================================================
 *
 *		     GstBaseTransform vmethod Implementations
 *
 * ============================================================================
 */


/* A transform really does the same thing as the chain function */

static GstFlowReturn transform(GstBaseTransform *trans, GstBuffer *inbuf, GstBuffer *outbuf)
{
	CMathConj* element = CMATH_CONJ(trans);
	int bits = element -> cmath_base.bits;
	int is_complex = element -> cmath_base.is_complex;

	GstMapInfo inmap, outmap;
	gst_buffer_map(inbuf, &inmap, GST_MAP_READ);
	gst_buffer_map(outbuf, &outmap, GST_MAP_WRITE);

	gpointer indata = inmap.data;
	gpointer outdata = outmap.data;
	gpointer indata_end = indata + inmap.size;

	if(is_complex == 1) {

		if(bits == 128) {
			double complex *ptr, *end = indata_end;
			double complex *outptr = outdata;
			for(ptr = indata; ptr < end; ptr++, outptr++)
				*outptr = conj(*ptr);
		} else if(bits == 64) {
			float complex *ptr, *end = indata_end;
			float complex *outptr = outdata;
			for(ptr = indata; ptr < end; ptr++, outptr++)
				*outptr = conjf(*ptr);
		} else {
			g_assert_not_reached();
		}
	} else if(is_complex == 0) {

		if(bits == 64) {
			double *ptr, *end = indata_end;
			double *outptr = outdata;
			for(ptr = indata; ptr < end; ptr++, outptr++)
				*outptr = *ptr;
		} else if(bits == 32) {
			float *ptr, *end = indata_end;
			float *outptr = outdata;
			for(ptr = indata; ptr < end; ptr++, outptr++)
				*outptr = *ptr;
		} else {
			g_assert_not_reached();
		}
	} else {
		g_assert_not_reached();
	}

	gst_buffer_unmap(inbuf, &inmap);
	gst_buffer_unmap(outbuf, &outmap);

	return GST_FLOW_OK;
}


/*
 * ============================================================================
 *
 *				Type Support
 *
 * ============================================================================
 */

/* Initialize the plugin's class */
static void
cmath_conj_class_init(gpointer klass, gpointer klass_data)
{
	GstBaseTransformClass *basetransform_class = GST_BASE_TRANSFORM_CLASS(klass);

	gst_element_class_set_details_simple(GST_ELEMENT_CLASS(klass),
		"Conjugate",
		"Filter/Audio",
		"Calculate the complex conjugate of a complex number, y = x*", 
		"Aaron Viets <aaron.viets@ligo.org>");

	basetransform_class -> transform = GST_DEBUG_FUNCPTR(transform);
	basetransform_class -> set_caps = GST_DEBUG_FUNCPTR(set_caps);
}

GType
cmath_conj_get_type(void)
{
	static GType type = 0;

	if(!type) {
		static const GTypeInfo info = {
			.class_size = sizeof(CMathBaseClass),
			.class_init = cmath_conj_class_init,
			.instance_size = sizeof(CMathBase),
		};
		type = g_type_register_static(CMATH_BASE_TYPE, "CMathConj", &info, 0);
	}

	return type;
}
