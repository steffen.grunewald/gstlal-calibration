/*
 * Copyright (C) 2021  Aaron Viets <aaron.viets@ligo.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __GSTLAL_ASD_H__
#define __GSTLAL_ASD_H__


#include <complex.h>

#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>


G_BEGIN_DECLS


/*
 * gstlal_asd_window_type enum
 */


enum gstlal_asd_window_type {
	GSTLAL_ASD_DPSS = 0,
	GSTLAL_ASD_KAISER,
	GSTLAL_ASD_DOLPH_CHEBYSHEV,
	GSTLAL_ASD_BLACKMAN,
	GSTLAL_ASD_HANN,
	GSTLAL_ASD_NONE
};


#define GSTLAL_ASD_WINDOW_TYPE  \
	(gstlal_asd_window_get_type())


GType gstlal_asd_window_get_type(void);


/*
 * lal_asd element
 */


#define GSTLAL_ASD_TYPE \
	(gstlal_asd_get_type())
#define GSTLAL_ASD(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj), GSTLAL_ASD_TYPE, GSTLALASD))
#define GSTLAL_ASD_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GSTLAL_ASD_TYPE, GSTLALASDClass))
#define GST_IS_GSTLAL_ASD(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GSTLAL_ASD_TYPE))
#define GST_IS_GSTLAL_ASD_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GSTLAL_ASD_TYPE))


typedef struct _GSTLALASD GSTLALASD;
typedef struct _GSTLALASDClass GSTLALASDClass;


/**
 * GSTLALASD:
 */


struct _GSTLALASD {
	GstBaseSink basesink;

	/* stream info */
	gint rate;
	gint unit_size;
	enum gstlal_asd_data_type {
		GSTLAL_ASD_F32 = 0,
		GSTLAL_ASD_F64,
	} data_type;

	/* timestamp bookkeeping */
	GstClockTime t0;
	guint64 offset0;
	guint64 next_in_offset;

	/* properties */
	guint64 fft_samples;
	guint64 overlap_samples;
	guint64 update_time;
	guint64 update_samples;
	enum gstlal_asd_window_type window_type;
	double frequency_resolution;
	double *asd;
	guint64 asd_endtime;
	double *whiten_filter;
	guint64 filter_samples;
	guint64 median_samples;
	char *filename;

	/* element memory */
	guint64 input_index;
	guint64 num_ffts_in_avg;
	union {
		struct {
			double *window;
			double *fft_input;
		} doublep;  /* double-precision */
		struct {
			float *window;
			float *fft_input;
		} floatp;  /* single-precision */
	} workspace;
};


/**
 * GSTLALASDClass:
 * @parent_class:  the parent class
 */


struct _GSTLALASDClass {
	GstBaseSinkClass parent_class;
};


GType gstlal_asd_get_type(void);


G_END_DECLS


#endif	/* __GSTLAL_ASD_H__ */
